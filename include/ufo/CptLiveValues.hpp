#ifndef __CPT_LIVE_VALUES_HPP_
#define __CPT_LIVE_VALUES_HPP_

#include "llvm/Analysis/Dominators.h"
#include "llvm/Support/Debug.h"

#include "ufo/ufo.hpp"
#include "ufo/Cpg/Lbe.hpp"
#include "ufo/EdgeComp.hpp"

#include "boost/utility.hpp"




namespace ufo
{

  /**
   * Computes the set of live LLVM IR values at each CutPoint. Only
   * values that are tracked by VC are included.
   */
  class CptLiveValues : boost::noncopyable
  {
  private:
    typedef std::map<CutPointPtr, ExprVector > CptExprMap;

    CptExprMap m_live;

    
    ExprFactory &m_efac;
    const Function &m_main;
    CutPointPtr m_initL;
    CutPointPtr m_exitL;
    
    DominatorTree &m_dt;
    LBE &m_lbe;
    
    

  private:
    /** 
	Compute the set of all LLVM IR Values as Expr that are available at a
	cutpoint and are used.
	
	\tparam OutputIterator an output iterator for Expr

	\param[in] env the current environment 
	\param[in] cp the current cutpoint
	\param[out] out the output iterator
    */
    template <typename OutputIterator>
    void liveAtCpt (Environment &env, CutPointPtr cp, OutputIterator out)
    {
      for (Environment::iterator it = env.begin (), end = env.end (); 
	   it != end; ++it)
	{
	  Expr key = it->first;

	  DEBUG (dbgs () << "Processing varible: " << *key << "\n";);
	  
	  
	  if (isOpX<BB> (key)) 
	    {
	      DEBUG (dbgs() << "  BB" << "\n" << "  Discarded" << "\n";);
	      continue;
	    }
	  
	  if (isOpX<VARIANT> (key))
	    {
              DEBUG (dbgs() << "  Variant" << "\n");

	      Expr mv = variant::mainVariant (key);
	      assert (isOpX<VALUE>(mv));
	      
	      const Value* v = getTerm<const Value*> (mv);
	      assert(v != NULL);

	      const Instruction* inst = dynamic_cast<const Instruction*>(v);
	      assert(inst != NULL);
	      
	      assert (isa<PHINode> (inst));
	      // -- only care about PhiNodes that are defined in the node
	      if (inst->getParent () == cp->getBB ()) *(out++) = mv;
              else DEBUG (dbgs() << "  Discarded" << "\n");
	    }
	  
	  if (isOpX<VALUE> (key))
	    {
              DEBUG (dbgs() << "  Value" << "\n");

	      const Value* v = getTerm<const Value*> (key);
	      assert(v != NULL);
            
	      const Instruction* inst = dynamic_cast<const Instruction*>(v);
	      assert(inst != NULL);

	      // -- anything that is defined in the basic block is
	      // -- defined before used, so we don't need it from
	      // -- other environments.
	      if (inst->getParent () == cp->getBB ()) continue;

	      // -- does not dominate the basic block, must be out of scope
	      if (!m_dt.dominates (inst->getParent (), cp->getBB ())) continue;

	      // -- keep any value that is defined and used in different
	      // -- blocks except if the only different-block-use of the
	      // -- value is in a PHI assignment of nodeBB. 
	      
	      bool keep = false;
	      
	      for (Value::const_use_iterator ut = inst->use_begin(), 
		     ue = inst->use_end (); ut != ue; ++ut)
		{
		  const User* u = *ut;
		  const BasicBlock* ubb = cast<Instruction>(u)->getParent ();

		  
		  if (ubb != inst->getParent())
		    {
		      // -- found a use in a different block
		      // -- check if it is us
		      if (ubb == cp->getBB ())
			// -- use in the node BB. check if it is a phi use
			if (isa<PHINode> (u)) 
			  continue;
		      keep = true; 
		      break;
		    }
		}
	      if (keep) *(out++) = key;
              else DEBUG(dbgs() << "  Discarded" << "\n");
	    }
	}
    }
    
  public:

    CptLiveValues (ExprFactory &efac, const Function &f, 
		   CutPointPtr initLoc, CutPointPtr exitLoc, 
		   DominatorTree &dt, LBE &lbe) : 
      m_efac (efac), m_main(f), m_initL (initLoc), m_exitL (exitLoc), 
      m_dt (dt), m_lbe (lbe) {}

    /** Runs the analysis */
    void run ()
    {
      m_live.clear ();
      
      // -- copy of all cutpoints. almost in topological order
      CutPointVector cps (m_lbe.getFunctionCutPts (m_main));
      
      // second element of cps is the exit - move it to the end
      CutPointVector::iterator cp_it = cps.begin() + 1;
      CutPointPtr cp = *cp_it;
      cps.erase(cp_it);
      cps.push_back(cp);

      std::vector<Environment> envs (cps.size ());
      
      envs [0] = Environment (m_efac);
      forall (CutPointPtr cp, cps)
	{
	  // -- skip initial and last location (they depend on no variables)
	  if (cp == m_initL || cp == m_exitL) continue;
	  
	  for (edgeIterator it = cp->predBegin (), end = cp->predEnd ();
	       it != end; ++it)
	    {
	      SEdgePtr edg = *it;
	      CutPointPtr pred = edg->getSrc ();

	      // -- skip back-edges
	      if (pred->getID () >= cp->getID ()) continue;
	      
	      // -- symbolic execution
	      Environment env (envs [pred->getID ()]);
	      SEdgeCondComp::computeEdgeCond (m_efac, env, 
					      mk<TRUE> (m_efac), edg, m_lbe);

	      // -- store the environment for future use
	      envs [cp->getID ()] = env;

	      // -- compute variables in scope
	      liveAtCpt (env, cp, std::back_inserter (m_live [cp]));

	      break;
	    }
	}
    }
    
    
    /// Returns live values
    const ExprVector &live (CutPointPtr cp) { return m_live [cp]; }
    const ExprVector &operator[] (CutPointPtr cp)  { return live (cp); }
    
  };
}


#endif
