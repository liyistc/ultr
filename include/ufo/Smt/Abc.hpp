#ifndef __ABC__HPP__
#define __ABC__HPP__
/** Interface to ABC synthesis engine */

#include "ufo/Config/config.h"

#if defined ABC_FOUND

#define ABC_NAMESPACE abc
#include "string.h" // for strdup

#include "ufo/Expr.hpp"
#include <map>

#include "boost/range.hpp"
#include <boost/function_output_iterator.hpp>
#include <boost/foreach.hpp>
#ifndef forall
#define forall BOOST_FOREACH
#endif

namespace ABC_NAMESPACE
{
  typedef struct Abc_Ntk_t_ Abc_Ntk_t;
  typedef struct Abc_Obj_t_ Abc_Obj_t;
  typedef struct Abc_Frame_t_ Abc_Frame_t;
  typedef struct Abc_Aig_t_ Abc_Aig_t;

  extern Abc_Ntk_t* Abc_FrameReadNtk (Abc_Frame_t *p);
  extern void Abc_FrameDeleteAllNetworks (Abc_Frame_t *f);
  extern void Abc_FrameSetCurrentNetwork (Abc_Frame_t *f, Abc_Ntk_t *n);
  
  extern void Abc_ObjAddFanin( Abc_Obj_t * pObj, Abc_Obj_t * pFanin );

  void NtkSetName (Abc_Ntk_t* ntk, const char *name);
  int NtkPoNum (Abc_Ntk_t *ntk);
  int NtkPiNum (Abc_Ntk_t *ntk);
  Abc_Obj_t* NtkPi (Abc_Ntk_t*, int i);
  Abc_Obj_t* NtkPo (Abc_Ntk_t*, int i);
  Abc_Ntk_t *newAigNtk ();
  Abc_Obj_t *NtkCreatePi (Abc_Ntk_t* ntk);
  Abc_Obj_t *NtkCreatePo (Abc_Ntk_t* ntk);
}


namespace ufo
{
  using namespace expr;
  using namespace abc;
  Expr abc_exec (Expr e, const char *cmd);
  Expr abc_resyn (Expr e);
  Expr abc_resyn2 (Expr e);
  Expr abc_simplify (Expr e);
  
  namespace abc_detail
  {
    typedef std::map<Expr,Abc_Obj_t*> ExprObjMap;
    typedef std::map<Abc_Obj_t*, Expr> ObjExprMap;

    struct IsNonBool : public std::unary_function<Expr,bool>
    {
      IsNonBool () {}
      bool operator() (Expr e) { return ! (isOp<BoolOp> (e)); }
    };
      
    struct CreatePi
    {
      Abc_Ntk_t *m_ntk;
      ExprObjMap &m_PiExprs;
      ExprVector &m_PiNames;
      
      CreatePi (Abc_Ntk_t *ntk, ExprObjMap &piExprs, ExprVector &piNames)
	: m_ntk (ntk), m_PiExprs (piExprs), m_PiNames (piNames) {}
      
      void operator() (const Expr &e)
      {
	Abc_Obj_t *obj = NtkCreatePi (m_ntk);
	assert (obj != NULL);
	m_PiExprs [e] = obj;
	m_PiNames.push_back (e);
	assert (m_PiNames.size () == static_cast<size_t>(NtkPiNum (m_ntk)));
      }  
    };
    
      
  }
  
  
  class Abc
  {
  private:    

    ExprFactory &m_efac;
    ExprVector m_PiNames;
    
    Abc_Frame_t *m_frm;
  public:
    Abc (ExprFactory &efac);
    ~Abc ();

    template <typename Range> 
    void load (const Range &rng) { setNtk (createNtk (rng)); }
    
    template <typename OutputIterator>
    void store (OutputIterator out) 
    { 
      decodeNtk (getNtk (), out); 
      Abc_FrameDeleteAllNetworks (frm ());
    }

    void cmd (const char *s);
    
  private:
    
    Abc_Frame_t *frm () { return m_frm; }
    void setNtk (Abc_Ntk_t *ntk = NULL) 
    { Abc_FrameSetCurrentNetwork (frm (), ntk); }
    Abc_Ntk_t *getNtk () { return Abc_FrameReadNtk (frm ()); }

    

    template <typename Range>
    Abc_Ntk_t* createNtk (const Range &rng)
    {
      using namespace abc_detail;
      
      Abc_Ntk_t *ntk = newAigNtk ();
      NtkSetName (ntk, "ufo");

      abc_detail::ExprObjMap seen;

      // -- allocate all Pi names
      forall (Expr e, rng)
	expr::filter (e, 
		      IsNonBool(), 
		      boost::make_function_output_iterator (CreatePi 
							    (ntk, 
							     seen, 
							     m_PiNames)));

      forall (Expr e, rng)
	{
	  Abc_Obj_t *po = NtkCreatePo (ntk);
	  Abc_ObjAddFanin (po, toAbc (e, ntk, seen));
	}
      return ntk;
    }

    template <typename OutputIterator>
    void decodeNtk (Abc_Ntk_t *ntk, OutputIterator out)
    { 
      using namespace abc_detail;
      ObjExprMap seen;
      
      // -- load primary inputs
      for (int i = 0; i < NtkPiNum (ntk); i++)
	seen [NtkPi (ntk, i)] = m_PiNames [i];

      // -- process all primary outputs
      for (int i = 0; i < NtkPoNum (ntk); i++)
	*(out++) = toExpr (NtkPo (ntk, i), ntk, seen);
    }
   
    Abc_Obj_t* toAbc (Expr e, Abc_Ntk_t * ntk, abc_detail::ExprObjMap &seen);
    Expr toExpr (Abc_Obj_t* obj, Abc_Ntk_t* ntk, abc_detail::ObjExprMap &seen);
    
    
  };

  

  // inline Expr abc_fraig (Expr e)
  // {
  //   Abc abc (e->efac ());
  //   boost::array<Expr,1> in = {{boolop::norm (e)}};
  //   boost::array<Expr,1> out;
    
  //   Expr res = abc.fraig (in, out.begin ());
  //   res = boolop::gather (boolop::nnf (out [0]));
  //   dbgs () << "fraig: " << dagSize (e) << " -> " << dagSize (res) << "\n";
  //   return res;
  // }
}

#else
// not ABC_FOUND
namespace ufo
{
  #include "ufo/Util/UfoAssert.hpp"
  inline Expr abc_resyn (Expr e)
  { 
    ufo::assertion_failed ("ABC library is required for this action.", 
                           __FILE__, __LINE__); 
    return Expr(0);
  }
  inline Expr abc_simplify (Expr e)
  { 
    ufo::assertion_failed ("ABC library is required for this action.", 
                           __FILE__, __LINE__); 
    return Expr(0);
  }

}

#endif

#endif

