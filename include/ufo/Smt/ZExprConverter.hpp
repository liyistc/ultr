#ifndef Z3_EXPR_CONVERTER__HPP_
#define Z3_EXPR_CONVERTER__HPP_

/** Marshal and Unmarshal between Z3 ast and Expr*/

// --  used for CL options 
#include "ufo/ufo.hpp"
#include "Z3n.hpp"

namespace ufo
{

  struct FailMarshal
  {
    template <typename C>
    static z3::ast marshal (Expr e, z3::context &ctx, 
			    C &cache, expr_ast_map &seen)
    { 
      errs () << "Cannot marshal: " << *e << "\n";
      UFO_ASSERT (0); exit (1); 
    }
  };
  
  struct FailUnmarshal
  {
    template <typename C>
    static Expr unmarshal (const z3::ast &a, ExprFactory &efac, 
			  C &cache, ast_expr_map &seen)
    { 
      errs () << "Cannot unmarshal: " << lexical_cast<string> (a) << "\n";
      UFO_ASSERT (0); exit (1); 
    }
    
  };
    

  template <typename M>
  struct BasicExprMarshal
  {
    template <typename C>
    static z3::ast marshal (Expr e, z3::context &ctx, 
			    C &cache, expr_ast_map &seen)
    {
      UFO_ASSERT (e);
      if (isOpX<TRUE>(e)) return z3::ast (ctx, Z3_mk_true (ctx));
      if (isOpX<FALSE>(e)) return z3::ast (ctx, Z3_mk_false (ctx));
      
      /** check the cache */
      {
	typename C::const_iterator it = cache.find (e);
	if (it != cache.end ()) return it->second;
      }
      
      /** check computed table */
      {
	typename expr_ast_map::const_iterator it = seen.find (e);
	if (it != seen.end ()) return it->second;
      }
      
      Z3_ast res = NULL;

      
      if (bind::isBVar (e))
	{
	  z3::ast sort (marshal (bind::type (e), ctx, cache, seen));
	  res = Z3_mk_bound (ctx, bind::bvarId (e), 
			     reinterpret_cast<Z3_sort>
			     (static_cast<Z3_ast> (sort)));
	}      
      else if (isOpX<INT_TY> (e))
	res = reinterpret_cast<Z3_ast> (Z3_mk_int_sort (ctx));
      else if (isOpX<REAL_TY> (e))
	res = reinterpret_cast<Z3_ast> (Z3_mk_real_sort (ctx));
      else if (isOpX<BOOL_TY> (e))
	res = reinterpret_cast<Z3_ast> (Z3_mk_bool_sort (ctx));

      else if (isOpX<INT>(e))
	{
	  z3::sort sort (ctx, 
			 ufocl::USE_INTS ? Z3_mk_int_sort (ctx) : 
			 Z3_mk_real_sort (ctx));
	  std::string sname = boost::lexical_cast<std::string>(e.get());
	  res = Z3_mk_numeral(ctx, sname.c_str(), sort);
	}
      
      else if (isOpX<MPQ>(e))
	{
	  const MPQ& op = dynamic_cast<const MPQ&>(e->op ());

	  z3::sort sort (ctx, 
                         ufocl::USE_INTS ? 
                         Z3_mk_int_sort (ctx) : Z3_mk_real_sort (ctx));
	  std::string sname = boost::lexical_cast<std::string>(op.get());
	  res = Z3_mk_numeral (ctx, sname.c_str (), sort);
	}
      else if (isOpX<MPZ>(e))
	{
	  const MPZ& op = dynamic_cast<const MPZ&>(e->op ());
	  z3::sort sort (ctx, Z3_mk_int_sort (ctx));
	  std::string sname = boost::lexical_cast<std::string>(op.get());
	  res = Z3_mk_numeral (ctx, sname.c_str (), sort);
	}
      else if (bind::isBoolVar (e))
	{
	  // XXX The name 'edge' is misleading. Should be changed.
	  Expr edge = bind::name (e);    
	  string svar;
	  // -- for variables with string names, use the names
	  if (isOpX<STRING> (edge))
	    svar = getTerm<std::string> (edge);
	  else // -- for non-string named variables use address
	    svar = "E" + 
	      boost::lexical_cast<std::string,void*> (edge.get());

	  
	  res = Z3_mk_const (ctx, 
			     ctx.str_symbol (svar.c_str ()),
			     ctx.bool_sort ());
	}
      else if (bind::isIntVar (e))
	{
	  Expr name = bind::name (e);
	  string sname;
	  if (isOpX<STRING> (name)) 
	    sname = getTerm<std::string> (name);
	  else 
	    sname = "I" + lexical_cast<string,void*> (name.get ());

	  res = Z3_mk_const (ctx, 
			     ctx.str_symbol (sname.c_str ()),
			     ctx.int_sort ());	  
	}
      else if (bind::isRealVar (e))
	{
	  Expr name = bind::name (e);
	  string sname;
	  if (isOpX<STRING> (name))
	    sname = getTerm<string> (name);
	  else
	    sname = "R" + lexical_cast<string,void*> (name.get ());
	    
	  res = Z3_mk_const (ctx,
			     ctx.str_symbol (sname.c_str ()),
			     ctx.real_sort ());
	}

      /** function declaration */
      else if (bind::isFdecl (e))
	{
	  z3::ast_vector pinned (ctx);
	  pinned.resize (e->arity ());
	  std::vector<Z3_sort> domain (e->arity ());

	  for (size_t i = 0; i < bind::domainSz (e); ++i)
	    {
	      z3::ast a (marshal (bind::domainTy (e, i), ctx, cache, seen));
	      pinned.push_back (a);
	      domain [i] = reinterpret_cast<Z3_sort> (static_cast<Z3_ast>(a));
	    }

	  
	  z3::sort range (ctx, 
			  reinterpret_cast<Z3_sort> 
			  (static_cast<Z3_ast> 
			   (marshal (bind::rangeTy (e), ctx, cache, seen))));
	  

	  Expr fname = bind::fname (e);
	  string sname;
	  if (isOpX<STRING> (fname))
	    sname = getTerm<string> (fname);
	  else 
	    sname = "F" + lexical_cast<string,void*>(fname.get ());
	    
	  z3::symbol symname = ctx.str_symbol (sname.c_str ());

	  res = reinterpret_cast<Z3_ast> (Z3_mk_func_decl (ctx, 
							   symname, 
							   bind::domainSz (e),
							   &domain[0], range));
	}      
	
      /** function application */
      else if (bind::isFapp (e))
	{	 

	  z3::func_decl zfdecl (ctx, 
				reinterpret_cast<Z3_func_decl>
				(static_cast<Z3_ast> 
				 (marshal (bind::fname (e), ctx, cache, seen))));
	  

	  // -- marshall all arguments except for the first one
	  // -- (which is the fdecl)
	  std::vector<Z3_ast> args (e->arity ());
	  z3::ast_vector pinned_args (ctx);
	  pinned_args.resize (e->arity ());

	  unsigned pos = 0;
	  for (ENode::args_iterator it = ++ (e->args_begin ()), 
		 end = e->args_end (); it != end; ++it)
	    {
	      z3::ast a (marshal (*it, ctx, cache, seen));
	      pinned_args.push_back (a);
	      args [pos++] = a;
	    }
	  
	    
	  res = Z3_mk_app (ctx, zfdecl, e->arity () - 1, &args [0]);
	}
      
      // -- cache the result for unmarshaling
      if (res)
	{
	  z3::ast ast (ctx, res);
	  cache.insert (typename C::value_type (e, ast));
	  return ast;
	}
      
      int arity = e->arity ();
      /** other terminal expressions */
      if (arity == 0) return M::marshal (e, ctx, cache, seen);

      else if (arity == 1)
	{
	  // -- then it's a NEG or UN_MINUS
	  if (isOpX<UN_MINUS>(e))
	    {
	      z3::ast arg = marshal (e->left(), ctx, cache, seen);
	      return z3::ast (ctx, Z3_mk_unary_minus(ctx, arg));
	    }
	  
	  if (isOpX<NEG>(e))
	    {
	      z3::ast arg = marshal (e->left(), ctx, cache, seen);
	      return z3::ast (ctx, Z3_mk_not(ctx, arg));
	    }
	  
	  return M::marshal (e, ctx, cache, seen);
	}
      else if (arity == 2)
	{

	  z3::ast t1 = marshal(e->left(), ctx, cache, seen);
	  z3::ast t2 = marshal(e->right(), ctx, cache, seen);

	  Z3_ast args [2] = {t1, t2};
	  
	    
	  /** BoolOp */
	  if (isOpX<AND>(e)) 
	    res = Z3_mk_and(ctx, 2, args);
	  else if (isOpX<OR>(e))
	      res = Z3_mk_or(ctx, 2, args);
	  else if (isOpX<IMPL>(e))
	    res = Z3_mk_implies(ctx,t1, t2);
	  else if (isOpX<IFF>(e))
	    res = Z3_mk_iff(ctx, t1, t2);
	  else if (isOpX<XOR>(e))
	    res = Z3_mk_xor(ctx, t1, t2);

	  /** NumericOp */
	  else if (isOpX<PLUS>(e))
	    res = Z3_mk_add(ctx, 2, args);
	  else if (isOpX<MINUS>(e))
	      res = Z3_mk_sub(ctx, 2, args);
	  else if (isOpX<MULT>(e))
	      res = Z3_mk_mul(ctx, 2, args);
	  else if (isOpX<DIV>(e))
	    res = Z3_mk_div (ctx, t1, t2);

	  /** Comparisson Op */
	  else if (isOpX<EQ>(e))
	    res = Z3_mk_eq (ctx, t1, t2);
	  else if (isOpX<NEQ>(e))
	    res = Z3_mk_not (ctx, Z3_mk_eq (ctx, t1, t2));
	  else if (isOpX<LEQ>(e))
	    res =  Z3_mk_le(ctx, t1, t2);
	  else if (isOpX<GEQ>(e))
	    res = Z3_mk_ge(ctx, t1, t2);
	  else if (isOpX<LT>(e))
	    res = Z3_mk_lt(ctx, t1, t2);
	  else if (isOpX<GT>(e))
	    res = Z3_mk_gt(ctx, t1, t2);
	    
	  else
	    return M::marshal (e, ctx, cache, seen);
	}
	else if (isOpX<AND> (e) || isOpX<OR> (e) ||
		 isOpX<ITE> (e) || isOpX<XOR> (e) ||
		 isOpX<PLUS> (e) || isOpX<MINUS> (e) ||
		 isOpX<MULT> (e))
	  {
	    std::vector<z3::ast> pinned;
	    std::vector<Z3_ast> args;
      
	    for (ENode::args_iterator it = e->args_begin(), end = e->args_end();
		 it != end; ++it)
	      {
		z3::ast a = z3::ast (ctx, marshal (*it, ctx, cache, seen));
		args.push_back (a);
		pinned.push_back (a);
	      }
	    
          
	    if (isOp<ITE>(e))
	      {
		UFO_ASSERT (e->arity () == 3);
		res = Z3_mk_ite(ctx,args[0],args[1],args[2]);
	      }
	    else if (isOp<AND>(e))
	      res = Z3_mk_and (ctx, args.size (), &args[0]);
	    else if (isOp<OR>(e))
	      res = Z3_mk_or (ctx, args.size (), &args[0]);
	    else if (isOp<PLUS>(e))
	      res = Z3_mk_add (ctx, args.size (), &args[0]);
	    else if (isOp<MINUS>(e))
	      res = Z3_mk_sub (ctx, args.size (), &args[0]);
	    else if (isOp<MULT>(e))
	      res = Z3_mk_mul (ctx, args.size (), &args[0]);
	  }
	else
	  return M::marshal (e, ctx, cache, seen);
      
      UFO_ASSERT (res != NULL);
      z3::ast final (ctx, res);
      seen.insert (expr_ast_map::value_type (e, final));
      
      return final;
      
    }
  };
  
  template <typename U>
  struct BasicExprUnmarshal
  {
    template <typename C>
    static Expr unmarshal (const z3::ast &z, 
			   ExprFactory &efac, C &cache, 
			   ast_expr_map &seen)
    {
      z3::context &ctx = z.ctx ();

      Z3_lbool bVal = Z3_get_bool_value (ctx, z);
      if (bVal == Z3_L_TRUE) return mk<TRUE> (efac);
      if (bVal == Z3_L_FALSE) return mk<FALSE> (efac);


      Z3_ast_kind kind = z.kind ();
      
      
      if (kind == Z3_NUMERAL_AST)
	{
	  std::string snum = Z3_get_numeral_string (ctx, z);
	  const mpq_class mpq (snum);
	  return mkTerm (mpq, efac);
	}
      else if (kind == Z3_SORT_AST)
	{
	  Z3_sort sort = reinterpret_cast<Z3_sort> (static_cast<Z3_ast> (z));
	  switch (Z3_get_sort_kind (ctx, sort))
	    {
	    case Z3_BOOL_SORT:
	      return mk<BOOL_TY> (efac);
	    case Z3_INT_SORT:
	      return mk<INT_TY> (efac);
	    case Z3_REAL_SORT:
	      return mk<REAL_TY> (efac);
	    default:
	      UFO_ASSERT (0 && "Unsupported sort");
	    }
	}
      else if (kind == Z3_VAR_AST)
	{
	  unsigned idx = Z3_get_index_value (ctx, z);
	  // XXX Z3 gives no way to find the sort of the bound variable
	  return bind::unintBVar (idx, efac);
	}
      
      else if (kind == Z3_FUNC_DECL_AST)
	{
	  Z3_func_decl fdecl = Z3_to_func_decl (ctx, z);
	  
	  Z3_symbol symname = Z3_get_decl_name (ctx, fdecl);
	  UFO_ASSERT (Z3_get_symbol_kind (ctx, symname) == Z3_STRING_SYMBOL);
	  std::string sname = 
	    std::string (Z3_get_symbol_string (ctx, symname));
	  Expr name = mkTerm (sname, efac);

	  ExprVector type;
	  for (unsigned p = 0; p < Z3_get_domain_size (ctx, fdecl); ++p)
	    {
	      Z3_sort sort = Z3_get_domain  (ctx, fdecl, p);
	      type.push_back 
		(unmarshal (z3::ast (ctx, Z3_sort_to_ast (ctx, sort)), 
			    efac, cache, seen));
	    }	    
	    
	  type.push_back 
	    (unmarshal (z3::ast (ctx, 
				 Z3_sort_to_ast (ctx, 
						 Z3_get_range (ctx, fdecl))), 
				     efac, cache, seen));

	  return bind::fdecl (name, type);
	}
      

      UFO_ASSERT (kind == Z3_APP_AST);
      Z3_app app = Z3_to_app (ctx, z);
      Z3_func_decl fdecl = Z3_get_app_decl (ctx, app);
      Z3_decl_kind dkind = Z3_get_decl_kind (ctx, fdecl);
      
      if (dkind == Z3_OP_NOT)
	{
	  UFO_ASSERT (Z3_get_app_num_args (ctx, app) == 1);
	  return mk<NEG> (unmarshal 
			  (z3::ast (ctx, Z3_get_app_arg (ctx, app, 0)), 
				    efac, cache, seen));
    	}
      if (dkind == Z3_OP_UMINUS)
	return mk<UN_MINUS> (unmarshal 
			     (z3::ast (ctx, Z3_get_app_arg (ctx, app, 0)), 
			      efac, cache, seen));

      // XXX ignore to_real and to_int operators
      if (dkind == Z3_OP_TO_REAL || dkind == Z3_OP_TO_INT)
        return unmarshal (z3::ast (ctx, Z3_get_app_arg (ctx, app, 0)),
                          efac, cache, seen);

      {
	typename C::const_iterator it = cache.find (z);
	if (it != cache.end ()) return it->second;
      }
      {
	typename ast_expr_map::const_iterator it = seen.find (z);
	if (it != seen.end ()) return it->second;
      }
      
      Expr e;
      ExprVector args;
      for (size_t i = 0; i < (size_t)Z3_get_app_num_args (ctx, app); i++)
	args.push_back (unmarshal 
			(z3::ast(ctx, Z3_get_app_arg(ctx, app, i)), efac, cache, seen));
      
      /** newly introduced Z3 symbol */
      if (dkind == Z3_OP_UNINTERPRETED)
	{
	  Expr res = bind::fapp (unmarshal (z3::func_decl (ctx, fdecl), 
					    efac, cache, seen), args);
	  // -- XXX maybe use seen instead. not sure what is best.
	  cache.insert (typename C::value_type (z, res));
	  return res;
	}
      
      switch (dkind)
	{
	case Z3_OP_ITE:
	  e = mknary<ITE> (args.begin (), args.end ());
	  break;
	case Z3_OP_AND:
	  e = mknary<AND> (args.begin(), args.end());
	  break;
	case Z3_OP_OR:
	  e =  mknary<OR> (args.begin(), args.end());
	  break;
	case Z3_OP_XOR:
	  e = mknary<XOR> (args.begin(), args.end());
	  break;
	case Z3_OP_IFF:
	  e =  mknary<IFF> (args.begin(), args.end());
	  break;
	case Z3_OP_IMPLIES:
	  e =  mknary<IMPL> (args.begin(), args.end());
	  break;
	case Z3_OP_EQ:
	  e =  mknary<EQ> (args.begin(), args.end());
	  break;
	case Z3_OP_LT:
	  e =  mknary<LT> (args.begin(), args.end());
	  break;
	case Z3_OP_GT:
	  e =  mknary<GT> (args.begin(), args.end());
	  break;
	case Z3_OP_LE:
	  e =  mknary<LEQ> (args.begin(), args.end());
	  break;
	case Z3_OP_GE:
	  e =  mknary<GEQ> (args.begin(), args.end());
	  break;
	case Z3_OP_ADD:
	  e =  mknary<PLUS> (args.begin(), args.end());
	  break;
	case Z3_OP_SUB:
	  e =  mknary<MINUS> (args.begin(), args.end());
	  break;
	case Z3_OP_MUL:
	  e =  mknary<MULT> (args.begin(), args.end());
	  break;
	case Z3_OP_DIV:
	  e = mknary<DIV> (args.begin(), args.end());
	  break;
	case Z3_OP_MOD:
	  e = mknary<MOD> (args.begin(), args.end());
	  break;
	default:
	  return U::unmarshal (z, efac, cache, seen);
	}
      
      seen [z] = e;
      return e;
    }
    
  };
  
    
}


#endif
