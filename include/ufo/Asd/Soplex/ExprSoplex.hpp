#ifndef __EXPR_SOPLEX__H_
#define __EXPR_SOPLEX__H_

#include "soplex.h"

using namespace std;
using namespace expr;

namespace soplex
{
  typedef map<Expr, double> ExprDoubleMap;

  // Gather nested plus to n-ary form
  struct GatherPlus
  {
    ExprSet &newArgs;
    GatherPlus (ExprSet &_a) : newArgs(_a) {}

    VisitAction operator () (Expr e)
      {
        if (!isOpX<PLUS> (e))
        {
          newArgs.insert (e);
          return VisitAction::skipKids();
        }
        else
          return VisitAction::doKids();
      }
  };

  /**
   * Interpret the result of simplex solving
   *
   * max: is maximizing
   */
  inline mpq_class interpret_result (SoPlex &soplex, SPxSolver::Status stat)
  {
    mpq_class lb;
    /* get solution */
    if (stat == SPxSolver::OPTIMAL)
    {
      //errs () << "Optimal Solution Found.\n";
      lb = mpq_class (soplex.objValue());
      //errs () << soplex.objValue()*10000000000000000.0 - 3.0*10000000000000000.0 << "\n";
    }
    else if (stat == SPxSolver::INFEASIBLE)
    {
      //errs () << "Infeasible Problem.\n";
      lb = soplex.spxSense() == SPxLP::MAXIMIZE ? negInf() : posInf();
    }
    else if (stat == SPxSolver::UNBOUNDED)
    {
      //errs () << "Problem Unbounded!\n";
      lb = soplex.spxSense() == SPxLP::MAXIMIZE ? posInf() : negInf();
    }
    else assert (false && "Invalid Solution Status");

    return lb;
  }


  inline Expr gatherPlus (Expr pe)
  {
    if (!isOpX<PLUS>(pe)) return pe;

    ExprSet g;
    GatherPlus gp(g);
    dagVisit(gp, pe);

    return mknary<PLUS> (g.begin(), g.end());
  }

  /**
   * Convert a vector of linear constraints in Expr type to a glpk matrix.
   */
  template <typename var2dim_type>
  void expr2matrix (SoPlex &p, vector<Expr> &cons, var2dim_type &vMap)
  {
    LPRowSet rows (cons.size(), vMap.size()); // constraint matrix
    //int r = 0; // row number
    forall (Expr c, cons)
    {
      assert (isOp<ComparissonOp> (c));
    
      //errs () << "CONS: " << *c << "\n";
      
      Expr canoc = z3n_lite_simplify (
        c->efac().mkBin (c->op(),
                         mk<PLUS>(c->left(), mk<UN_MINUS> (c->right())),
                         mkTerm(mpq_class(0), c->efac())));

      //errs () << "CANO: " << *canoc << "\n";

      if (!isOp<ComparissonOp> (canoc))
        continue;

      Expr poly = canoc->left();
      Expr constant = canoc->right();

      //errs () << "poly: " << *poly << "\n";
      assert (isOpX<PLUS> (poly) || isOpX<MULT> (poly) || bind::isRealConst (poly));
      assert (isOpX<MPQ> (constant));

      DSVector row (vMap.size()); // create a row

      // Get coefficients
      if (isOpX<PLUS> (poly)) // e.g., 3*x + y
      {
        Expr gpoly = gatherPlus(poly);

        forall (Expr v, make_pair (gpoly->args_begin(), gpoly->args_end()))
        {
          if (isOpX<MULT> (v)) // e.g., 3*x
          {
            Expr tm = v;
            assert (isOpX<MPQ> (tm->left()));
            assert (bind::isRealConst (tm->right()));
            mpq_class coef = getTerm<mpq_class>(tm->left());
            //p.set_coefficient (r, vMap[tm->right()], coef.get_d());
            row.add (vMap[tm->right()], coef.get_d());
          }
          else if (bind::isRealConst (v)) // e.g., y
          {
            //p.set_coefficient (r, vMap[v], 1.0);
            row.add (vMap[v], 1.0);
          }
        }
      }
      else if (isOpX<MULT> (poly)) // e.g., 3*x
      {
        assert (isOpX<MPQ> (poly->left()));
        assert (bind::isRealConst (poly->right()));
        mpq_class coef = getTerm<mpq_class>(poly->left());
        row.add (vMap[poly->right()], coef.get_d());
      }
      else // e.g., y
      {
        // Single real var
        //p.set_coefficient (r, vMap[poly], 1.0);
        row.add (vMap[poly], 1.0);
      }

      // Get constant
      mpq_class ct = getTerm<mpq_class>(constant);
      double ct_d = ct.get_d();

      if (isOpX<GEQ> (canoc))
        //p.set_row_lower_bound (r++, ct_d);
        rows.add (ct_d, row, infinity);
      else if (isOpX<LEQ> (canoc))
        //p.set_row_upper_bound (r++, ct_d);
        rows.add (-infinity, row, ct_d);
      else if (isOpX<op::EQ> (canoc))
        rows.add (ct_d, row, ct_d);
      else assert (false && "Unreachable");
    }

    p.addRows (rows);

    unsigned i;
    for (i=0; i<vMap.size(); ++i)
      p.changeBounds (i, -infinity, infinity);
  }

  /**
   * Given a vector of linear constraints and a set of variables
   * Return upper and lower bounds for each variable using linear
   * programming library glpk.
   */
  inline map<Expr, Interval> getBounds (vector<Expr> &_c, set<Expr> &_v)
  {
    map<Expr, Interval> res;
    map<Expr, int> vMap;

    Stats::resume ("soplex.pp");
    int i = 0;
    forall (Expr c, _c)
    {
      ExprSet vars = Bound::collectVars (c);
      // map vars to dims
      forall (Expr v, vars)
      {
        if (vMap.count (v) == 0)
          vMap[v] = i++;
      }
    }

    SoPlex p_origin; // an original copy of the problem.
    expr2matrix (p_origin, _c, vMap);

    Stats::stop ("soplex.pp");
    // make objective function and solve
    forall (Expr v, _v)
    {
      // Make a copy of the problem
      SoPlex prob (p_origin);

      if (vMap.count (v) == 0)
      {
        if (bind::isRealConst(v)) // variable not appeared in the formula
        {
          //errs () << "not appear: " << *v << "\n";
          res[v] = Interval::top();
          continue;
        }

        // The objective function is a term: e.g., x+y, x-y, 3x
        assert (isOpX<PLUS>(v) || isOpX<MINUS>(v) || isOpX<MULT>(v));
     
        // clear objective function first
        unsigned j;
        for (j=0; j<vMap.size(); ++j)
          prob.changeObj (j, 0.0);

        Expr simp = z3n_lite_simplify (v);
        if (isOpX<MULT>(simp))
        {
          assert (vMap.count(simp->right()) > 0);
          mpq_class coeff = getTerm<mpq_class>(simp->left());
          //prob.set_objective_var (vMap[simp->right()], coeff.get_d());
          prob.changeObj (vMap[simp->right()], coeff.get_d());
        }
        else
        {
          assert (isOpX<PLUS> (simp));
          // Do not support objective function containing constants: x+3
          forall (Expr tm, make_pair(simp->args_begin(), simp->args_end()))
          {
            if (isOpX<MULT> (tm))
            {
              assert (vMap.count(tm->right()) > 0);
              mpq_class coeff = getTerm<mpq_class>(tm->left());
              //prob.set_objective_col (vMap[tm->right()], coeff.get_d());
              prob.changeObj (vMap[tm->right()], coeff.get_d());
            }
            else if (bind::isRealConst (tm))
            {
              assert (vMap.count(tm) > 0);
              //prob.set_objective_col (vMap[tm], 1.0);
              prob.changeObj (vMap[tm], 1.0);
            }
            else assert (0 && "Objective function containing constants not supported!");
          }
        }
      }
      else // the objective is a single variable
      {
        //prob.set_objective_var (vMap[v], 1.0);
        prob.changeObj (vMap[v], 1.0);
      }

      //prob.load_problem();
      //prob.set_obj_dir(false);
      prob.changeSense (SPxLP::MINIMIZE);
      mpq_class lb = interpret_result (prob, prob.solve());

      //prob.reset();
      //prob.load_problem();
      //prob.set_obj_dir (true);
      prob.changeSense (SPxLP::MAXIMIZE);
      mpq_class ub = interpret_result (prob, prob.solve());

      //prob.writeFile ("dump.lp", NULL, NULL, NULL);

      res[v] = Interval (lb,ub);
      //prob.print(cerr);
      //errs () << "RESULT: [" << res[v].first << "," << res[v].second << "]\n";
      // reset problem instance after simplex solving
      //prob.reset();
      //prob.clear();
    }

    return res;
  }



 
//   static void test ()
//   {
//     errs () << "Start Testing SoPlex\n";

//     SoPlex mysoplex;

//     /* set the objective sense */
//     mysoplex.changeSense(SPxLP::MINIMIZE);

//     /* we first add variables */
//     DSVector dummycol(0);
//     mysoplex.addCol(LPCol(-2.0, dummycol, infinity, 0.0));
//     mysoplex.addCol(LPCol(-3.0, dummycol, infinity, .0));
//     mysoplex.addCol(LPCol(-4.0, dummycol, infinity, .0));

//     /* then constraints one by one */
//     LPRowSet rows(2, 3);
//     DSVector row1(3);
//     row1.add(0, 3.0);
//     row1.add(1, 2.0);
//     row1.add(2, 1.0);
//     DSVector row2(3);
//     row2.add(0, 2.0);
//     row2.add(1, 5.0);
//     row2.add(2, 3.0);

//     rows.add (-infinity, row1, 10.0);
//     rows.add (-infinity, row2, 15.0);

//     mysoplex.addRows (rows);
//     // mysoplex.addRow(LPRow(-infinity, row1, 10.0));
//     // mysoplex.addRow(LPRow(-infinity, row2, 15.0));

//     /* NOTE: alternatively, we could have added the matrix nonzeros in dummycol already;
//        nonexisting rows are then
//        automatically created.
//     */

//     /* write LP in .lp format */
// //    mysoplex.writeFile("dump.lp", NULL, NULL, NULL);

//     /* solve LP */
//     // SPxSolver::Status stat;
//     // DVector prim(2);
//     // DVector dual(1);
//     // stat = mysoplex.solve();

//     // //result (stat, mysoplex);

//     // mysoplex.changeSense(SPxLP::MAXIMIZE);
//     // SPxSolver::Status stat2;
//     // stat2 = mysoplex.solve();

//     //result (stat2, mysoplex);
//   }

}


#endif
