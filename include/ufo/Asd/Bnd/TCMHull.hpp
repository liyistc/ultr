#ifndef _TCM_HULL_HPP
#define _TCM_HULL_HPP

#include "ufo/Asd/Interval.hpp"

/**
 * TCM (Template Constraint Matrix) Abstract State Domain.
 * Based on VMCAI05 paper: Scalable analysis of linear systems using mathematical
 * programming.
 */

using namespace std;

namespace ufo
{
  typedef map<Expr, Interval> TCMState;

  class TCMHull
  {
  private:
    TCMState val;
    ExprSet &bTemp;
    ExprSet &rTemp;
    
  public:
    TCMHull (ExprFactory &_e, ExprSet &_b, ExprSet &_r) : bTemp(_b), rTemp(_r)
    {
      forall (Expr b, _b)
        val[b] = Interval::bot();
      forall (Expr r, _r)
        val[r] = Interval::bot();
    }

    ~TCMHull () {}
    
    //template <typename R>
    
  };
}

#endif
