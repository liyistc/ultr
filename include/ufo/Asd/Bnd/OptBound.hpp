#ifndef __OPT_BOUND_HPP_
#define __OPT_BOUND_HPP_

#include "llvm/Support/Debug.h"

#include "ufo/Asd/Bnd/Bound.hpp"
#include "ufo/Smt/UfoZ3.hpp"
#include "ufo/Asd/Interval.hpp"
#include "ufo/Smt/MUS.hpp"


using namespace std;
using namespace boost;

namespace ufo
{
  /**
   * Bound algorithm based on glpk library.
   * Note: the formula is first closed (over-approximation in LR theory).
   */
  class OPTBound : public Bound
  {
  public:
    OPTBound (ExprFactory &_e, const ExprSet &_v, Expr p) :
      efac(_e),
      varbase(_v), phi (deepSimp (p)),
      realVars(filterVars(varbase,true)),
      boolVars(filterVars(varbase,false)),
      allVars(filterVars(collectVars(p), true)),
      literals(collectLiterals(phi))
    {
      forall (Expr v, varbase)
        bounds[v] = Interval::bot();
    }

    ~OPTBound () {}

    void boundAll (Expr _p)
    {
      UfoZ3 ctx (efac);
      ZSolver<UfoZ3> z3 (ctx);
      z3.push();
      z3.assertExpr (phi);

      UfoZ3 optiCtx (efac);
      ZSolver<UfoZ3> optiz3 (optiCtx);
      
      Stats::resume ("time.z3.solve");
      tribool sat = z3.solve();
      Stats::stop ("time.z3.solve");
      while (sat)
      {
        Stats::count ("opti.z3.call");
        Stats::resume ("time.simplification");
        ExprVector mE = ReduceCons ?
          getMinimizedE (z3, literals, phi) : getE (z3, literals, phi);
        //ExprVector sE = simplifyConstraints (mE);
        Stats::stop ("time.simplification");
        
        Stats::resume ("time.opti.z3");
        
        ExprVector g;
        optiz3.push();
        //errs () << "mE.size: " << mE.size() << "\n";
        Expr disj = mknary<AND>(mk<TRUE>(efac),mE.begin(),mE.end());
        //ExprSet disj_vars = collectVars (disj);
        //errs () << "disj: " << *disj << "\n";

        optiz3.assertExpr (disj);
        foreach (Expr _v, realVars)
        {
          //if (disj_vars.count (_v) == 0)
          //  errs () << "not found in the disjunct!\n";
          
          Interval b;
          tribool unbounded;
          tribool sat;

          if (!isNegInf(bounds[_v].first))
          {
            unbounded = false;
            sat = optiz3.solve_opti (unbounded, _v, false);
            assert ((bool)sat);
            if (unbounded)
            {
              Stats::count ("opti.z3.unbounded");
              b.first = negInf();
            }
            // else if (boost::indeterminate(unbounded))
            // {
            //   errs () << *_v << "not optimized!\n";
            //   Expr val = z3.getModel().eval(_v);
              
            //   if (isOpX<MPQ>(val))
            //     b.first = getTerm<mpq_class>(val);
            //   else b.first = negInf();
            // }
            else
            {
              Expr val = optiz3.getModel().eval(_v);
              errs () << "val: " << *val << "\n";
              
              if (isOpX<MPQ>(val))
                b.first = getTerm<mpq_class>(val);
              else b.first = negInf();
            }
          }

          if (!isPosInf(bounds[_v].second))
          {
            unbounded = false;
            sat = optiz3.solve_opti (unbounded, _v, true);
            assert ((bool)sat);
            if (unbounded)
            {
              Stats::count ("opti.z3.unbounded");
              b.second = posInf();
            }
            // else if (boost::indeterminate(unbounded))
            // {  
            //   errs () << *_v << "not optimized!\n";
            //   Expr val = z3.getModel().eval(_v);

            //   if (isOpX<MPQ>(val))
            //     b.second = getTerm<mpq_class>(val);
            //   else b.second = posInf();
            // }
            else
            {
              Expr val = optiz3.getModel().eval(_v);
              errs () << "val: " << *val << "\n";

              if (isOpX<MPQ>(val))
                b.second = getTerm<mpq_class>(val);
              else b.second = posInf();
            }
          }

          bounds[_v] = bounds[_v].join(b);
          g.push_back (bounds[_v].gamma(_v));
        }
        optiz3.pop();

        Stats::stop ("time.opti.z3");

        Expr _gamma = mknary<AND>(mk<TRUE>(efac), g.begin(), g.end());
        
        //errs () << "GAMMA: " << *_gamma << "\n";
        z3.assertExpr (mk<NEG> (_gamma));
        
        Stats::resume ("time.z3.solve");
        sat = z3.solve();
        Stats::stop ("time.z3.solve");
      }

      z3.pop();
    }

    inline Interval operator [] (Expr var)
    {
      assert (bounds.count (var) > 0);
      return bounds[var];
    }

  private:
    inline ExprVector getE (ZSolver<UfoZ3> &_z3, ExprSet &lits, Expr _phi)
    {
      Expr trueE = mk<TRUE> (efac);
      
      ExprVector E; // Set of literals satisfied by the current model
      forall (Expr _l, lits)
      {
        if (isOpX<TRUE> (_z3.getModel().eval (_l)))
          E.push_back (_l);
      }

      return filterLinearCons(E);
    }
    
    /**
     * Minimize E
     * param[in] ExprZ3 &_z3: Z3 object
     * param[in] ExprSet &lits: set of literals in the formula
     * param[in] Expr _phi: deep simplified phi
     * return ExprVector: a minimized set of linear constraints satisfied
     * by the current model
     */
    inline ExprVector getMinimizedE (ZSolver<UfoZ3> &_z3, ExprSet &lits, Expr _phi)
    {
      Expr trueE = mk<TRUE> (efac);
      
      ExprVector E; // Set of literals satisfied by the current model
      forall (Expr _l, lits)
      {
        //errs () << "lit: " << *_l << "\n";
        if (isOpX<TRUE> (_z3.getModel().eval (_l)))
        {
          //errs () << "pick\n";
          E.push_back (_l);
        }
      }

      //_z3.debugPrintModel();
      
      // Assert (/\E => phi)
      
      // assert ((bool)!z3n_is_sat (boolop::land
      //                      (mknary<AND> (mk<TRUE> (efac),
      //                                    E.begin(), E.end()),
      //                       boolop::lneg(_phi))));
      
      
      // Construct assumptions from E
      ExprVector assumps; // Assumptions: ASM<e> => e
      forall (Expr _e, E)
        assumps.push_back (mk<IMPL>(mk<ASM>(_e), _e));

      Expr lphi = boolop::land (boolop::lneg(_phi), 
                                mknary<AND> (trueE,
                                             assumps.begin(), assumps.end()));
      
      // Minimize E
      ExprSet usedAssumps; // Used assumptions        
      UfoZ3 ctx (efac);
      ZSolver<UfoZ3> mus_z3 (ctx);
      mus_basic (mus_z3, lphi, std::inserter (usedAssumps,
                                              usedAssumps.begin()),
                 3);

      // Extract linear constraints from assumptions
      ExprVector newCons = filterLinearCons (usedAssumps); // Used constraints

      return newCons;
    }

    /**
     * Extract linear constraints
     * param[in] R &collection: input is a collection of literals/ASM<literals>
     * return ExprVector: a vector of linear constraints
     */
    template <typename R>
    ExprVector filterLinearCons (R &collection)
    {
      ExprVector res;
      forall (Expr _c, collection)
      {
        Expr lit = _c;
        if (isOpX<ASM> (lit)) lit = _c->left();
        if (isOpX<NEG> (lit) && isOp<ComparissonOp> (lit->left()))
        {
          res.push_back (flip (lit->left()));
          continue;
        }
        else if (isOp<ComparissonOp> (lit))
          res.push_back (lit);
      }
      return res;
    }
    
    ExprFactory &efac;
    const ExprSet &varbase;
    Expr phi; // Closed formula
    ExprSet realVars;
    ExprSet boolVars;
    ExprSet allVars;
    ExprSet literals;
    VarBounds bounds;
  };
}

#endif
