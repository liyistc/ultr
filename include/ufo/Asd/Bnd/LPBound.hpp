#ifndef __LP_BOUND_HPP_
#define __LP_BOUND_HPP_

#include "llvm/Support/Debug.h"

#include "ufo/Asd/Bnd/Bound.hpp"
#include "ufo/Smt/UfoZ3.hpp"
//#include "ufo/Smt/ExprZ3.hpp"
#include "ufo/Asd/Interval.hpp"
#include "ufo/Smt/MUS.hpp"


#include "ufo/Asd/Glpk/ExprGlpk.hpp"
#include "ufo/Asd/Soplex/ExprSoplex.hpp"

using namespace std;
using namespace boost;

static cl::opt<bool>
ReduceCons ("dnf-reduce",
            cl::desc("Reduce linear constraints when building disjunct\n"),
            cl::init(true));

enum LPSolver {GLPK, SOPLEX};
static cl::opt<enum LPSolver>
LPSolver ("lp-solver",
          cl::desc ("Linear Programming Solver"),
          cl::values (
            clEnumVal (GLPK, "GLPK"),
            clEnumVal (SOPLEX, "SoPlex"),
            clEnumValEnd),
          cl::init (SOPLEX));

namespace ufo
{
  /**
   * Bound algorithm based on glpk library.
   * Note: the formula is first closed (over-approximation in LR theory).
   */
  class LPBound : public Bound
  {
  public:
    LPBound (ExprFactory &_e, const ExprSet &_v, Expr p) :
      efac(_e),
      varbase(_v), phi (closeFormula (p)),
      realVars(filterVars(varbase,true)),
      boolVars(filterVars(varbase,false)),
      allVars(filterVars(collectVars(p), true)),
      literals(collectLiterals(phi))
    {
      forall (Expr v, varbase)
        bounds[v] = Interval::bot();
    }

    ~LPBound () {}

    void boundAll (Expr _p)
    {
      // debug
//      soplex::test();
//      return;
      
      UfoZ3 ctx (efac);
      ZSolver<UfoZ3> z3 (ctx);
      z3.push();
      z3.assertExpr (phi);

      //Stats::resume ("while.loop");
      while (z3.solve())
      {
        Stats::count ("glpk.call");
        Stats::resume ("time.simplification");
        ExprVector mE = ReduceCons ?
          getMinimizedE (z3, literals, phi) : getE (z3, literals, phi);
        //ExprVector sE = simplifyConstraints (mE);
        Stats::stop ("time.simplification");
        
        Stats::resume ("time.glpk");
        VarBounds box;
        switch (LPSolver)
        {
        case GLPK: 
          box = glpk::getBounds (mE, realVars);
          break;
        case SOPLEX:
          box = soplex::getBounds (mE, realVars);
          break;
        }
        
        Stats::stop ("time.glpk");

        ExprVector g;
        foreach (Expr _v, realVars)
        {
          bounds[_v] = bounds[_v].join(box[_v]);
          g.push_back (bounds[_v].gamma(_v));
        }
        
        Expr _gamma = mknary<AND>(mk<TRUE>(efac), g.begin(), g.end());
        
        //errs () << "GAMMA: " << *_gamma << "\n";
        z3.assertExpr (mk<NEG> (_gamma));
      }
      //Stats::stop ("while.loop");

      z3.pop();
    }

    inline Interval operator [] (Expr var)
    {
      assert (bounds.count (var) > 0);
      return bounds[var];
    }

  private:
    inline ExprVector getE (ZSolver<UfoZ3> &_z3, ExprSet &lits, Expr _phi)
    {
      Expr trueE = mk<TRUE> (efac);
      
      ExprVector E; // Set of literals satisfied by the current model
      forall (Expr _l, lits)
      {
        if (isOpX<TRUE> (_z3.getModel().eval (_l)))
          E.push_back (_l);
      }

      return filterLinearCons(E);
    }
    
    /**
     * Minimize E
     * param[in] ExprZ3 &_z3: Z3 object
     * param[in] ExprSet &lits: set of literals in the formula
     * param[in] Expr _phi: deep simplified phi
     * return ExprVector: a minimized set of linear constraints satisfied
     * by the current model
     */
    inline ExprVector getMinimizedE (ZSolver<UfoZ3> &_z3, ExprSet &lits, Expr _phi)
    {
      Expr trueE = mk<TRUE> (efac);
      
      ExprVector E; // Set of literals satisfied by the current model
      forall (Expr _l, lits)
      {
        //errs () << "lit: " << *_l << "\n";
        if (isOpX<TRUE> (_z3.getModel().eval (_l)))
        {
          //errs () << "pick\n";
          E.push_back (_l);
        }
      }

      //_z3.debugPrintModel();
      
      // Assert (/\E => phi)
      
      // assert ((bool)!z3n_is_sat (boolop::land
      //                      (mknary<AND> (mk<TRUE> (efac),
      //                                    E.begin(), E.end()),
      //                       boolop::lneg(_phi))));
      
      
      /*
      // Debug simplify
      ExprVector Econs = filterLinearCons (E);
          
      Expr cjE = mknary<AND> (trueE, Econs.begin(), Econs.end());
      errs () << "E: " << *cjE << "\n";
      Expr simE = z3_simplify (cjE);
      errs () << "simp: " << *simE << "\n";
      */
      
      // Construct assumptions from E
      ExprVector assumps; // Assumptions: ASM<e> => e
      forall (Expr _e, E)
        assumps.push_back (mk<IMPL>(mk<ASM>(_e), _e));

      //assert ((bool)z3n_is_sat(_phi));
      
      Expr lphi = boolop::land (boolop::lneg(_phi), 
                                mknary<AND> (trueE,
                                             assumps.begin(), assumps.end()));
      //assert ((bool)z3n_is_sat(lphi));
      

      // Minimize E
      ExprSet usedAssumps; // Used assumptions        
      UfoZ3 ctx (efac);
      ZSolver<UfoZ3> mus_z3 (ctx);
      mus_basic (mus_z3, lphi, std::inserter (usedAssumps,
                                              usedAssumps.begin()),
                 3);

      //ctx.toSmtLibDecls (lphi);
      //ctx.toSmtLib (lphi);
      
      //errs () << "Used: " << usedAssumps.size() << "\n";
      // Extract linear constraints from assumptions
      ExprVector newCons = filterLinearCons (usedAssumps); // Used constraints
      //errs () << "Used Cons: " << newCons.size() << "\n";

      return newCons;
    }

    /**
     *
     * Buggy!!!!
     * 
     * Simplify Constraints
     * Discover equalities from pair of inequalities.
     * e.g. x>=k and x<=k  -->  x==k
     * Substitute all discovered equalities to constraints
     */
    /*
    inline ExprVector simplifyConstraints (const ExprVector &_cons)
    {
      // debug
      // return _cons;
      
      // x>=k /\ x<=k => x=k
      // Eliminate dimensions
      ExprVector resCons;      
      ExprMap geq, leq, eqk;
      forall (Expr c, _cons)
      {
        Expr left = c->left();
        Expr right = c->right();
        // x == k
        if (isOpX<EQ> (c) && isOpX<MPQ>(right) &&
            (isOpX<VARIANT>(left) || bind::isRealVar(left)))
          eqk[left] = right;
        // x >= k
        else if (isOpX<GEQ> (c) && isOpX<MPQ>(right) &&
                 (isOpX<VARIANT>(left) || bind::isRealVar(left)))
        {
          if (leq.count (left) > 0 && leq[left] == right)
          {
            eqk[left] = right;
            leq.erase (left);
          }
          else geq[left] = right;
        }
        // x <= k
        else if (isOpX<LEQ> (c) && isOpX<MPQ>(right) &&
                 (isOpX<VARIANT>(left) || bind::isRealVar(left)))
        {
          if (geq.count (left) > 0 && geq[left] == right)
          {
            eqk[left] = right;
            geq.erase (left);
          }
          else leq[left] = right;
        }
        else
          resCons.push_back (c);
      }

      // Substitute equalities
      for (vector<Expr>::iterator c = resCons.begin();
           c != resCons.end(); ++c)
        *c = replace (*c, eqk);        

      // Adding equalities into constraints
      forall (ExprPair e, eqk)
        resCons.push_back (mk<EQ>(e.first, e.second));
      forall (ExprPair e, leq)
        resCons.push_back (mk<LEQ>(e.first, e.second));
      forall (ExprPair e, geq)
        resCons.push_back (mk<GEQ>(e.first, e.second));
      
      return resCons;
    }
    */

    /**
     * Extract linear constraints
     * param[in] R &collection: input is a collection of literals/ASM<literals>
     * return ExprVector: a vector of linear constraints
     */
    template <typename R>
    ExprVector filterLinearCons (R &collection)
    {
      ExprVector res;
      forall (Expr _c, collection)
      {
        Expr lit = _c;
        if (isOpX<ASM> (lit)) lit = _c->left();
        if (isOpX<NEG> (lit) && isOp<ComparissonOp> (lit->left()))
        {
          res.push_back (flip (lit->left()));
          continue;
        }
        else if (isOp<ComparissonOp> (lit))
          res.push_back (lit);
      }
      return res;
    }
    
    
    
    ExprFactory &efac;
    const ExprSet &varbase;
    Expr phi; // Closed formula
    ExprSet realVars;
    ExprSet boolVars;
    ExprSet allVars;
    ExprSet literals;
    VarBounds bounds;
  };
}

#endif
