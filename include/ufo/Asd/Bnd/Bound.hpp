#ifndef __BOUND__HPP_
#define __BOUND__HPP_

#include "ufo/ufo.hpp"

namespace ufo
{
  typedef std::map<Expr, Interval> VarBounds;

  /**
   * Parent Class for VarBound, BoxBound and DNFBound
   */
  class Bound
  {
  public:
    virtual ~Bound () {};
    virtual void boundAll (Expr phi) = 0;
    virtual Interval operator [] (Expr var) = 0;

    /**  Test if a var is bool type. */
    static bool isBool (Expr var)
    {
      if (bind::isBoolConst(var)) return true;
      if (isOp<NumericOp>(var)) return false;
      Expr u = var;
      if (isOpX<VARIANT>(u)) u = variant::mainVariant(u);
      if (isOpX<VARIANT>(u)) u = variant::mainVariant(u);
      
      if (!isOpX<VALUE>(u)) return false; //Real

      if (isBoolType(getTerm<const Value*>(u)->getType()))
        //if (bind::isBoolVar (u))
        return true;
      
      return false;
    }

    /**
     * filterVars.
     * Filter Boolean or real variables.
     * param[in] const ExprSet &vars. variables to be filtered.
     * param[in] bool isReal. Filter real valued variables.
     * return ExprSet.
     */
    template <typename R>
    ExprSet filterVars (const R &vars, bool isReal)
    {
      ExprSet res;
      foreach (Expr v, vars)
      {
        if ((isBool (v) && !isReal)
            || (!isBool(v) && isReal))
          res.insert(v);        
        else continue;
      }
      return res;
    }
    
    /**
     * Filter collecting linear constraints
     */
    struct ConsFilter
    {
      bool operator() (Expr v) const { return isOp<ComparissonOp> (v); }
    };

    /**
     * Filter collecting evaluated variables (VARIANT)
     */
    struct VarsFilter
    {
      bool operator() (Expr v) const { return 
          isOpX<VARIANT> (v)
          || isOpX<VALUE> (v)
          || bind::isBoolConst(v)
          || bind::isRealConst(v)
          || bind::isIntConst(v)
          || bind::isRealVar(v)
          || bind::isIntVar(v)
          || bind::isBoolVar(v); }
    };

    static bool isNumericTerm (Expr e)
    {
      if (isOp<NumericOp> (e)) return true;
      if (isOpX<VALUE> (e))
      {
        const Value *v = getTerm<const Value*> (e);
        return v->getType ()->isIntegerTy () && 
          !v->getType ()->isIntegerTy (1);
      }

      return false;
    }

    /**
     * Filter collecting bind variables
     */
    // struct BindFilter
    // {
    //   bool operator() (Expr v) const { return isOpX<BIND> (v); }
    // };

    /**
     * Filter collecting literals:
     * Including variables, terms and bind variables.
     */
    struct LiteralFilter
    {
      bool operator() (Expr v) const
      {
        if (isOpX<NEG> (v)) return true;
        
        return (isOpX<BIND> (v)
                || isOp<ComparissonOp> (v)
                || bind::isRealConst (v)
                || bind::isIntConst (v)
                || bind::isBoolConst (v)
                || isOpX<VARIANT> (v)
                || isOpX<VALUE> (v));
      }
    };

    /** This visitor close a formula */
    struct CloseFormulaVisit
    {
      CloseFormulaVisit () {}

      VisitAction operator() (Expr exp)
      {
        if (isOpX<NEG>(exp))
        {
          Expr e = exp->left();
          //   // Flip operators
          Expr res;
          if (isOpX<LEQ>(e))
            res = mk<NEG>(mk<LT>(e->left(), e->right()));
          if (isOpX<GEQ>(e))
            res = mk<NEG>(mk<GT>(e->left(), e->right()));
          if (isOpX<GT>(e))
            res = exp;
          if (isOpX<LT>(e))
            res = exp;
          if (isOpX<EQ>(e))
            res = mk<TRUE>(e->efac());
          if (isOpX<NEQ>(e))
            res = mk<EQ>(e->left(), e->right());

          if (res)
            return VisitAction::changeTo(res);
        }
        if (isOpX<GT>(exp)) 
          return VisitAction::changeTo(mk<GEQ>(exp->left(),exp->right()));
        if (isOpX<LT>(exp)) 
          return VisitAction::changeTo(mk<LEQ>(exp->left(),exp->right()));
        if (isOpX<NEQ>(exp))
          return VisitAction::changeTo(mk<TRUE>(exp->efac()));

        return VisitAction::doKids();
      }
    };

    /**
     * Close formula visit under the assumption of Integer theory.
     */
    struct CloseFormulaVisitOverInt
    {
      CloseFormulaVisitOverInt () {}
      
      VisitAction operator () (Expr exp)
      {
	if (isOpX<NEG> (exp) && isOp<ComparissonOp> (exp->left()))
        {
          exp = flip (exp->left());
        }
	
	if(isOp<ComparissonOp> (exp))
        {  
          if (isOpX<GT> (exp) || isOpX<LT> (exp) || isOpX<NEQ> (exp))
	  {
            Expr left = exp->left();
            Expr right = exp->right();

            //errs () << "left: " << *left << "\n";
            //errs () << "right: " << *right << "\n";
            //assert (VarsFilter () (left));

            Expr one = mkTerm (mpq_class(1), exp->efac());
            Expr kp = mk<PLUS> (right, one);
            Expr km = mk<MINUS> (right, one);
   
            if (isOpX<GT>(exp))
              return VisitAction::changeTo (mk<GEQ>(left, kp));
            if (isOpX<LT>(exp))
              return VisitAction::changeTo (mk<LEQ>(left, km));
            if (isOpX<NEQ>(exp))
              return VisitAction::changeTo (boolop::lor 
                                            (mk<GEQ> (left, kp),
                                             mk<LEQ> (left, km)));
	  }
          else return VisitAction::changeTo (exp);
        }
	return VisitAction::doKids();
      }
    };
    
    /**
     * This simplifier flatten the formula.
     * Change IFF, IMPL, XOR, ITE to AND, OR
     */
    struct DeepSimplifier
    {
      ExprFactory &efac;
      DeepSimplifier (ExprFactory &_e) : efac(_e) {}

      VisitAction operator () (Expr exp)
      {
        // (x:Bool) = a op b 
        // (a && b) || (!a && !b)
        if (isOpX<IFF> (exp) || (isOpX<EQ> (exp) && bind::isBoolConst (exp->left())))
          return VisitAction::changeDoKids (
            boolop::lor (mk<AND> (exp->left(), exp->right()),
                         boolop::land
                         (boolop::lneg(exp->left()), boolop::lneg(exp->right()))));

        // !a || b
        if (isOpX<IMPL> (exp))
          return VisitAction::changeDoKids (
            boolop::lor (boolop::lneg (exp->left()), exp->right()));

        // a&&!b || !a&&b
        if (isOpX<XOR> (exp))
          return VisitAction::changeDoKids (
            boolop::lor (boolop::land (exp->left(), boolop::lneg(exp->right())),
                         boolop::land (boolop::lneg(exp->left()), exp->right())));
              
        if (isOp<ComparissonOp> (exp))
        {
          // x = ITE(b, y, z)
          // b/\(x=y) || !b/\(x=z)
          if (isOpX<EQ>(exp) && isOpX<ITE> (exp->right()))
          {
            Expr x = exp->left();
            Expr b = exp->right()->left();
            Expr y = exp->right()->right();
            Expr z = *((exp->right()->args_begin())+2);
              
            return VisitAction::changeTo (
              boolop::lor(
                boolop::land (b, mk<EQ>(x,y)),
                boolop::land (boolop::lneg(b), mk<EQ>(x,z))
                )
              );
          }
        }

        return VisitAction::doKids();
      }
    };

    /** Deep Simplify formula */
    static Expr deepSimp (Expr e)
    {
      DeepSimplifier ds (e->efac());
      e = dagVisit (ds, e);
      e = boolop::nnf (e);
      return e;
    }

    /** Flip operator */
    static Expr flip (Expr c)
    {
      assert (isOp<ComparissonOp> (c));
      Expr left = c->left();
      Expr right = c->right();
      if (isOpX<EQ> (c))
        return mk<NEQ> (left, right);
      else if (isOpX<NEQ> (c))
        return mk<EQ> (left, right);
      else if (isOpX<GT> (c))
        return mk<LEQ> (left, right);
      else if (isOpX<LT> (c))
        return mk<GEQ> (left, right);
      else if (isOpX<GEQ> (c))
        return mk<LT> (left, right);
      else if (isOpX<LEQ> (c))
        return mk<GT> (left, right);
      else
        assert (0 && "Unreachable");
    }
    
    /** Close the formula */
    // static Expr closeFormula(Expr e)
    // {
    //   // Convert to Negative Normal Form
    //   Expr res = deepSimp (e); // In NNF without ITE, IMPL, etc.
    //   //assert (!z3_is_sat (boolop::land (e, mk<NEG>(res))));
    //   //assert (!z3_is_sat (boolop::land (res, mk<NEG>(e))));
    //   // Close operators
    //   CloseFormulaVisit cv;
    //   res = dagVisit (cv,res);      
      
    //   return res;
    // }

    /**
     * Close the formula under Real theory
     */
    static Expr closeFormulaReal (Expr e)
    {
      Expr res = deepSimp (e);
      CloseFormulaVisit cv;
      res = dagVisit (cv, res);

      //assert (!z3_is_sat (boolop::land (e, mk<NEG>(res))));
      assert ((bool)!z3n_is_sat (boolop::land (e, mk<NEG>(res))));
      return res;
    }
    
    /**
     * Close the formula over Integer theory.
     */
    static Expr closeFormula (Expr e)
    {
      Expr res = deepSimp (e);
      CloseFormulaVisitOverInt cv;
      res = dagVisit (cv, res);

      return res;
    }

    /** Collect literals from formula */
    ExprSet collectLiterals (Expr formula)
    {
      ExprSet literals;
      filter (formula, LiteralFilter(), inserter (literals, literals.begin()));
      return literals;
    }
    
    /** Collect variables from formula */
    static ExprSet collectVars (Expr formula)
    {
      ExprSet vars;
      filter (formula, VarsFilter (), std::inserter(vars, vars.begin()));
      return vars;
    }

    /**
     * boundBool. Bound Boolean variables.
     * param[in] ExprZ3 &z3.
     * param[in] Expr phi.
     * param[in] ExprSet &vars. variables.
     * param[out] VarBounds &res. a map to store the results.
     */
    void boundBool (Expr phi, ExprSet &vars, VarBounds &res)
    {
      if (vars.size () == 0)
        return;
      UfoZ3 ctx (phi->efac());
      ZSolver<UfoZ3> z3(ctx);
      z3.assertExpr (phi);
      foreach (Expr _v, vars)
      {
        Interval r = Interval::bot();
        z3.push();
        z3.assertExpr(_v);
        if (z3.solve()) r.second = posInf();
        z3.pop();
        z3.push();
        z3.assertExpr(mk<NEG>(_v));
        if (z3.solve()) r.first = negInf();
        z3.pop();

        //assert (res.count(_v) > 0);
        res[_v] = r;
      }
    }

  
    void print(Interval i)
    {
      cout << "[" << i.first << "," << i.second << "]" << endl;
    }

  
  };
}

#endif
