#ifndef __DNF_BOUND_HPP
#define __DNF_BOUND_HPP

#if defined APRON_FOUND

#include "llvm/Support/Debug.h"

#include "ufo/Asd/Bnd/Bound.hpp"
#include "ufo/Asd/Apron/ExprApron.hpp"
#include "ufo/Smt/UfoZ3.hpp"
//#include "ufo/Smt/ExprZ3.hpp"
#include "ufo/Asd/Interval.hpp"
#include "ufo/Smt/MUS.hpp"

using namespace std;
using namespace boost;

namespace ufo
{
  /**
   * DNFBound.
   * Bound variables in a first order logic formula with linear
   * constraints by constructing an over-approximated
   * Disjunctive Normal Form formula incrementally.
   */
  template <typename AP>
  class DNFBound : public Bound
  {
  public:
    DNFBound (ExprFactory &_e, const ExprSet &_v, Expr _p) :
      efac(_e), varbase(_v),
      phi (closeFormula (_p)),
      realVars(filterVars(varbase,true)),
      boolVars(filterVars(varbase,false)),
      allVars(filterVars(collectVars(_p),true)),
      literals(collectLiterals(phi)),
      ap(efac, allVars)
    {
      forall (Expr v, varbase)
        bounds[v] = Interval::bot();
    }

    ~DNFBound () {}

    /**
     * boundAll.
     * Bound all variables.
     */
    void boundAll (Expr _p)
    { 
      //phi = closeFormula (phi);
      //Bound Boolean variables
      //boundBool (phi, boolVars, bounds);
      
      UfoZ3 ctx (efac);
      ZSolver<UfoZ3> z3 (ctx);
      z3.push();
      z3.assertExpr (phi);
      
      while (z3.solve())
      {           
        // Create an apron state from constraints satisfied by the model
        ExprVector mE = getMinimizedE (z3, literals, phi);
        // Simplify constraints in mE
        //ExprVector sE = simplifyConstraints (mE);
        
        DEBUG (errs () << "Before cons2state\n");
        Stats::resume ("dnf.cons2state");
        ap_ptr s = ap.cons2state (mE);
        Stats::stop ("dnf.cons2state");
        DEBUG (errs () << "After cons2state\n");

        ExprVector g;
        foreach (Expr _v, realVars)
        { 
          Interval _b;
          // Apron does not support terms like x+y (only support variables).
          if (allVars.count (_v) <= 0)
            _b = Interval::top();
          else
            // Get Box from apron
            _b = ap.getBound (_v, s);

          // Join with the current Box
          bounds[_v] = bounds[_v].join (_b);
          g.push_back (bounds[_v].gamma(_v));
        }

        Expr _gamma = mknary<AND>(mk<TRUE>(efac), g.begin(), g.end());
        DEBUG (errs() << *_gamma << "\n");
        // block the region inside the Box
        z3.assertExpr (mk<NEG> (_gamma));
      }
      z3.pop();
    }

    /**
     * Minimize E
     * param[in] ExprZ3 &_z3: Z3 object
     * param[in] ExprSet &lits: set of literals in the formula
     * param[in] Expr _phi: deep simplified phi
     * return ExprVector: a minimized set of linear constraints satisfied
     * by the current model
     */
    inline ExprVector getMinimizedE (ZSolver<UfoZ3> &_z3, ExprSet &lits, Expr _phi)
    {
      Expr trueE = mk<TRUE> (efac);
      
      ExprVector E; // Set of literals satisfied by the current model
      forall (Expr _l, lits)
      {
        if (isOpX<TRUE> (_z3.getModel().eval (_l)))
          E.push_back (_l);
      }

      // Assert (/\E => phi)
      // assert ((bool)!z3n_is_sat (boolop::land
      //                     (mknary<AND> (mk<TRUE> (efac),
      //                                   E.begin(), E.end()),
      //                      boolop::lneg(_phi))));

      /*
      // Debug simplify
      ExprVector Econs = filterLinearCons (E);
          
      Expr cjE = mknary<AND> (trueE, Econs.begin(), Econs.end());
      errs () << "E: " << *cjE << "\n";
      Expr simE = z3_simplify (cjE);
      errs () << "simp: " << *simE << "\n";
      */
      
      // Construct assumptions from E
      ExprVector assumps; // Assumptions: ASM<e> => e
      forall (Expr _e, E)
        assumps.push_back (mk<IMPL>(mk<ASM>(_e), _e));

      Expr lphi = boolop::land (boolop::lneg(_phi), 
                                mknary<AND> (trueE,
                                             assumps.begin(), assumps.end()));

      // Minimize E
      ExprSet usedAssumps; // Used assumptions        
      UfoZ3 ctx (efac);
      ZSolver<UfoZ3> mus_z3 (ctx);
      mus_basic (mus_z3, lphi, std::inserter (usedAssumps,
                                              usedAssumps.begin()),
                 3);

      // Extract linear constraints from assumptions
      ExprVector newCons = filterLinearCons (usedAssumps); // Used constraints

      return newCons;
    }

    /**
     * Simplify Constraints
     * Discover equalities from pair of inequalities.
     * e.g. x>=k and x<=k  -->  x==k
     * Substitute all discovered equalities to constraints
     */
    inline ExprVector simplifyConstraints (const ExprVector &_cons)
    {
      // x>=k /\ x<=k => x=k
      // Eliminate dimensions
      ExprVector resCons;      
      ExprMap geq, leq, eqk;
      forall (Expr c, _cons)
      {
        Expr left = c->left();
        Expr right = c->right();
        // x == k
        if (isOpX<EQ> (c) && isOpX<MPQ>(right) && isOpX<VARIANT>(left))
          eqk[left] = right;
        // x >= k
        else if (isOpX<GEQ> (c) && isOpX<MPQ>(right) && isOpX<VARIANT>(left))
        {
          if (leq.count (left) > 0 && leq[left] == right)
            eqk[left] = right;
          else geq[left] = right;
        }
        // x <= k
        else if (isOpX<LEQ> (c) && isOpX<MPQ>(right) && isOpX<VARIANT>(left))
        {
          if (geq.count (left) > 0 && geq[left] == right)
            eqk[left] = right;
          else leq[left] = right;
        }
        else
          resCons.push_back (c);
      }

      // Substitute equalities
      for (vector<Expr>::iterator c = resCons.begin();
           c != resCons.end(); ++c)
        *c = replace (*c, eqk);        

      // Adding equalities into constraints
      forall (ExprPair e, eqk)
        resCons.push_back (mk<EQ>(e.first, e.second));
      forall (ExprPair e, leq)
        resCons.push_back (mk<LEQ>(e.first, e.second));
      forall (ExprPair e, geq)
        resCons.push_back (mk<GEQ>(e.first, e.second));
      
      return resCons;
    }

    /**
     * Extract linear constraints
     * param[in] R &collection: input is a collection of literals/ASM<literals>
     * return ExprVector: a vector of linear constraints
     */
    template <typename R>
    ExprVector filterLinearCons (R &collection)
    {
      ExprVector res;
      forall (Expr _c, collection)
      {
        Expr lit = _c;
        if (isOpX<ASM> (lit)) lit = _c->left();
        if (isOpX<NEG> (lit) && isOp<ComparissonOp> (lit->left()))
        {
          res.push_back (flip (lit->left()));
          continue;
        }
        else if (isOp<ComparissonOp> (lit))
          res.push_back (lit);
      }
      return res;
    }
    
    /**
     * Override [] operator
     */
    inline Interval operator [] (Expr var)
    {
      assert (bounds.count (var) > 0);
      return bounds[var];
    }
    
  private:
    
    //Expr sphi; // simplified phi
    ExprFactory &efac;
    const ExprSet &varbase;
    Expr phi; // original phi
    ExprSet realVars; // real variables need to be bounded
    ExprSet boolVars; // Boolean variables need to be bounded
    ExprSet allVars; // All real-valued variables appeared in the formula
    ExprSet literals; // All literals in the formula
    VarBounds bounds; // temporary bounds for each variable
    AP ap;
  };
}

#endif

#endif
