#ifndef __NEW_BOUND_HPP_
#define __NEW_BOUND_HPP_

#include <boost/algorithm/string/predicate.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/optional.hpp>
#include <iostream>
#include "ufo/Asd/Interval.hpp"
//#include "ufo/Smt/ExprZ3.hpp"
#include "ufo/Smt/UfoZ3.hpp"
#include "ufo/Asd/TCMAbstractStateDomain.hpp"
#include "ufo/Asd/Bnd/Bound.hpp"
#include "Equivalence.hpp"
#include <queue>
#include <boost/dynamic_bitset.hpp>

using namespace expr;
using namespace expr::op;
using namespace boost;
using namespace llvm;

namespace ufo
{
  /**
   * NewBound.
   * Bound an array of variables together. It's an alternative to VarBound.
   */
  class NewBound : public Bound
  {
    typedef boost::dynamic_bitset<> BitSet;
    typedef std::set<BitSet> EQC;

    struct Context
    {
      AbstractState point; // Model
      BitSet bit; // The equivalence class which the model is in
      unsigned degree; // No. of times of unboudedness checks left
      bool unb; // The model is produced by unbound check
      
      Context (AbstractState _p, BitSet _b, unsigned _d = 2, bool _u = false) :
        point(_p), bit(_b), degree(_d), unb(_u) {}

      ~Context () {}
    };
    typedef std::queue<Context> ContextQueue; 

  private:  
    ExprFactory &efac; // Expression Factory
    const ExprSet &varbase; // Variables to be bounded. Set of template
    UfoZ3 ctx;
    ZSolver<UfoZ3> z3; // Z3 object
    ExprSet realVars; // real-valued variables
    ExprSet boolVars; // Boolean-valued variables
    BoolTCMProduct btcm; // Boolean TCM product domain
    AbstractState U; // TCM convex hull: Under approximation of phi.
    ExprVector F; // Faces: {t=k|t<=k is an atomic proposition in phi}
    EQC EQbase; // Visited Equivalence classes
    Equivalence<Expr> relate; // Relation sets
   
  public:
    NewBound (ExprFactory &_e, const ExprSet &_v) :
      efac(_e), varbase(_v), ctx (_e), z3 (ctx), 
      realVars(filterVars(varbase, true)),
      boolVars(filterVars(varbase, false)),
      btcm(efac, boolVars, realVars),
      U(btcm.bot(CutPointPtr())),
      relate (varbase) {} 

    ~NewBound ();


    /**
     * scanFace
     * Relate vars appear in the same constraint
     * param[in] Expr phi
     */
    void scanFace (Expr phi);
    
    /**
     * scanTemplate
     * Relate vars appear in the same template
     */
    void scanTemplate ();
    
    /**
     * boundAll. Compute bounds for templates. Store results in U.
     * param[in] formula phi.
     */
    void boundAll (Expr phi);
    
    /**
     * assertStrongerEQ. Make an assertion that asks for a model in
     * a stronger equivalence class.
     * 
     * param[in] ExprVector& rf: Available faces
     * param[in] ExprVector& pc: Faces in the current equivalence class
     */
    inline void assertStrongerEQ (ExprVector& rF, ExprVector& pC);
    
    /**
     * relatedF. Find the related faces for term t.
     * param[in] Expr t
     * return ExprVector: related faces
     */
    inline ExprVector relatedF (Expr t);
    
    /**
     * unboundCheck
     * param[in] Context &ctx: the context of point p1.
     * param[in] AbstractState &Uold: old copy of the under-approximation (TCM hull).
     * param[in] ExprVector &delta: list of dimensions that are updated by p.
     * param[out] ContextQueue &queue: queue for storing contexts need to be
     * further checked.
     * param[in] bool type2: true if this is a type 2 check.
     * effect: check unboundedness of the 
     */
    void unboundCheck (Context &ctx,
                       AbstractState &Uold, ExprVector &delta,
                       ContextQueue &queue, bool type2);

    /**
     * type2Check.
     * Try find p2 in [p1] such that t(p2)>t(p1).
     * param[in] bool pos: direction (true if +, false if -)
     * param[in] Expr t: template
     * param[in] ExprVector &pClass: set of equalities p1 satisfies
     * param[in] Expr tp_expr: t(p1)
     * param[in] Interval itv: [t(p1), t(p1)]
     * return Expr: p2 if found; otherwise NULL
     */
    Expr type2Check (bool pos, Expr t,
                     ExprVector &pClass, Expr tp_expr, Interval itv);

    /**
     * Get Equivalence Class (represented as bit vectors) of the current model.
     * Return a Bit Vector representing the equivalence class that the
     * current model is in.
     * return BitVector&: a bit vector representing the set of satisfied equalities
     * by the current model.
     */
    inline BitSet getEQ ();

    /**
     * Get equalities from bit vectors
     * param[in] BitVector& b: a bit vector representing the satisfied equalities
     * return ExprVector: a set of satisfied equalities
     */
    inline ExprVector bitvec2eq (BitSet& b);

    /**
     * Return true if there is stronger EQ stored in EQbase
     */
    inline bool existStrongerEQ (BitSet &_p);

  
    /** findModel.
     * Compute the alpha of the model in the current context.
     * param[in] bool joinWithU: join the model found with U if true.
     * return AbstractState: alpha_TCM (model)
     */
    inline AbstractState findModel (bool joinWithU);    

    /**
     * directionalPush.
     * Given p2, try find a p3 such that t(p3)>=t(p2) AND [p3]>[p2].
     * param[in] bool pos: direction (true if +, false if -)
     * param[in] Expr t: template
     * param[in] ExprVector &pClass: set of equalities p2 satisfies
     * param[in] Expr tp: t(p2)
     * param[in] unsigned dgr: >1 do not join p3 with U
     * return optional<Context>: p3 if found; otherwise empty
     */
    inline optional<Context> directionalPush
    (bool pos, Expr t, ExprVector &pClass, Expr tp, unsigned dgr);
    
    /**
     * Override [] operator.
     * Provide access to variable bounds.
     * param[in] variable var
     */
    Interval operator [] (Expr var)
    {
      return btcm.getBound (var, U);
    }

    /**
     * gamma function.
     * return Expr: representation of the current box.
     */
    Expr gamma ()
    {
      return btcm.gamma (CutPointPtr(), U);
    }

    /**
     * print bit vector
     */
    void printBit (BitSet& b);
    
    /**
     * Add the equivalence class of the current model to list
     */
    void addEQ (BitSet bit)
    {   
      EQbase.insert (bit);
    }
  };
}

#endif
