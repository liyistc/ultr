#ifndef __SYMBA_BOUND_HPP_
#define __SYMBA_BOUND_HPP_


#include "llvm/Support/Debug.h"

#include "ufo/Asd/Interval.hpp"
#include "ufo/Smt/UfoZ3.hpp"
#include "ufo/Smt/MUS.hpp"
#include "ufo/Asd/Bnd/Bound.hpp"
#include "ufo/Asd/Bnd/Equivalence.hpp"

#include <queue>

#include <boost/dynamic_bitset.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/optional.hpp>


using namespace expr;
using namespace expr::op;
using namespace boost;
using namespace llvm;

static cl::opt<bool>
AddTermPushPoints ("symba-add-term-push-points",
		   cl::desc ("Use intermediate push points"),
		   cl::init (false));
static cl::opt<bool>
PreferIntSolution ("symba-pref-int",
		   cl::desc ("Round towards integers"),
		   cl::init (true));

static cl::opt<unsigned>
GlobalPushStep ("global-push-step",
		cl::desc ("Number of termpushs between global push"),
		cl::init (100000));

static cl::opt<bool>
UseOptiZ3 ("opti-z3",
           cl::desc ("Use Opt-z3 in Symba"),
           cl::init (false));

static cl::opt<double>
TermPushTime ("tpush-time",
              cl::desc ("Term push time limit (0.0,1.0] (balance parameter described in the paper)"),
              cl::init (1.0));

namespace ufo
{
  typedef boost::dynamic_bitset<> EClass;
  typedef list<EClass> EClassList;

  /**
   * PushList
   * A direction is either + or - of a template t. It contains a push-list
   * which is a list of Bit Set that represents the equivalence classes
   * visited in the direction.
   */
  class PushList
  {
  private:
    // Predicate: return true if c is weaker than e
    struct isWeakerThan
    {
      const EClass &e;
      isWeakerThan (const EClass &_e) : e(_e) {}

      bool operator () (const EClass &_c) { return _c.is_subset_of (e); }
    };

    struct isStrongerThan
    {
      const EClass &e;
      isStrongerThan (const EClass &_e) : e(_e) {}

      bool operator () (const EClass &_c) { return e.is_subset_of (_c); }
    };

    /** The list */
    EClassList pl;
    /** The term to which this list belongs */
    Expr t;
    /** The direction of this push-list (positive or negative) */
    int dir;

  public:
    bool extreme;

  public:
    PushList (Expr _t, int _dir) : t(_t), dir(_dir), extreme(false) {}
    PushList (const PushList &other) : 
      pl(other.pl), t(other.t), dir(other.dir), extreme(false) {}

    Expr getTerm () { return t; }

    int getDir () { return dir; }

    bool isExtreme () {return extreme; }
    
    PushList &operator= (const PushList &rhs)
    {
      PushList tmp (rhs);
      std::swap (pl, tmp.pl);
      std::swap (t, tmp.t);
      //std::swap (th, tmp.th);
      dir = tmp.dir;
      return *this;
    }
    
    // remove all equal or weaker classes first
    // then push into the list
    void push (const EClass &e)
    {
      // Remove weaker classes in the list first
      pl.remove_if (isWeakerThan (e));
      // Push e
      pl.push_back (e);
    }

    // pop the oldest class
    EClass pop ()
    {
      assert (pl.size() > 0);
      // pop the oldest class
      EClass res = pl.front();
      pl.pop_front ();
      return res;
    }

    unsigned size () { return pl.size(); }

    void reachExtreme () { extreme = true; }

    void removeStronger (const EClass &e)
    {
      pl.remove_if (isStrongerThan (e));
    }
    
    void clear () { pl.clear (); extreme = true; }
    

    /* print equivalence class in the direction */
    void print ()
    {
      errs () << "PushList:\n";
      forall (EClass e, pl)
      {
        errs () << "EClass: ";
        unsigned i;
        for (i = 0; i < e.size(); ++i) errs () << e[i];
        errs () << "\n";
      } 
    }
  };

  /**
   * Template
   * A template is a term to be bounded. It contains two Directions: +/-,
   * two flags: posExtreme, negExtreme which indicate whether bounds for
   * +/- directions are already the real extremes.
   */
  class Template 
  {
  public:
    Expr t; // term of the template
    PushList pos; // positive direction
    PushList neg; // negative direction
    Interval bd; // [lower, upper] bound
    bool posExtreme; // positive direction reaches extreme
    bool negExtreme; // negative direction reaches extreme

    ExprVector related;

    //Template () {}

    Template (Expr _t) :
      t (_t),
      pos(t, 1), neg(t, -1), bd (Interval::bot()),
      posExtreme(false), negExtreme(false) {}

    Template () : t(Expr(0)), pos(t, 1), neg (t, -1), 
		  bd (Interval::bot()), 
		  posExtreme (false), negExtreme (false) {}
    
    Template (const Template &other) : t(other.t), pos(other.pos), 
				       neg (other.neg), bd (other.bd),
				       posExtreme (other.posExtreme),
				       negExtreme (other.negExtreme) {}    

    const Interval& getBound () const { return bd; }

    Expr gamma () { return bd.gamma (t); }

    void posDone () { errs () << "+"; posExtreme = true; pos.clear(); }
    void negDone () { errs () << "-"; negExtreme = true; neg.clear(); }

    void posIsInf () 
    { errs () << "["; bd.second = posInf(); posDone(); errs () << "]"; }
    void negIsInf () 
    { errs () << "["; bd.first = negInf(); negDone(); errs () << "]"; }
  };
  
  typedef vector<PushList*> PushListPtrVector;
  typedef list<PushList*> PushListPtrList;
  
  /**
   * TCM
   * TCM Hull with join function.
   */
  struct TCM
  {
    ExprFactory &efac;
    const ExprSet &T; // terms
    map<Expr, Template> hull; // templates

    TCM (ExprFactory &_e, const ExprSet &_T) : efac (_e), T(_T)
    { forall (Expr t, T) hull[t] = Template (t); }

    Template& operator [] (const Expr _t) 
    {
      assert (hull.count (_t) > 0);
      return hull [_t];
    }

    /**
     * Find a term that is not already bounded. (open direction)
     */
    bool findOpenDir (Expr & t, bool & dir)
      {
        forall (Expr _t, T)
        {
          if (!hull[t].posExtreme)
          { t = _t; dir = true; return true; }
          if (!hull[t].negExtreme)
          { t = _t; dir = false; return true; }
        }
        return false;
      }

    Expr getMax (Expr t)
    {
      assert (hull.count (t) > 0);
      return mkTerm (hull[t].bd.second, efac);
    }

    Expr getMin (Expr t)
    {
      assert (hull.count (t) > 0);
      return mkTerm (hull[t].bd.first, efac);
    }
    
    Expr gamma ()
    {
      ExprVector g;
      forall (Expr t, T)
      {
        assert (hull.count(t) > 0);
        g.push_back (hull[t].gamma());
      }

      return mknary<AND> (mk<TRUE>(efac), g.begin(), g.end());
    }

    void print ()
    {
      errs () << "TCM HULL: |T|=" << T.size() << "\n";
      forall (Expr t, T)
      {
        errs () << *t << " : [" << hull[t].bd.first
                << "," << hull[t].bd.second << "]\n";
      }
    }

    /**
     * join. Join p with U.
     * param[in] ExprMap &rv: a map from terms to expr 
     * representation of the model value p
     * param[in] EClass &ec: a bit set that represents 
     * the equivalence class p is in
     * return DirVector: a vector of updated directions
     * side effect: push-list of the affected directions are updated
     */
    template <typename OutputIterator>
    void join (ExprMap &rv, EClass &ec, OutputIterator out)
    {
      typedef pair<Expr, Expr> ExprPair;
      
      forall (ExprPair p, rv)
      {
        //Template& t = hull[p.first];

        if (isOpX<MPQ> (p.second))
        {
          // get t(p)
          mpq_class v = getTerm<mpq_class>(p.second);
          // get U.t.min
          mpq_class umin = hull[p.first].bd.first;
          // get U.t.max
          mpq_class umax = hull[p.first].bd.second;

          // Add directions changed to delta
          if (!leq (v, umax)) // v > U.t.max
          {
            hull[p.first].pos.push (ec); // update push-list
            *(out++) = &(hull[p.first].pos);
          }
          if (!leq (umin, v)) // v < U.t.min
          {
            hull[p.first].neg.push (ec); // update push-list
            *(out++) = &(hull[p.first].neg);
          }
          // Join with current TCM hull
          hull[p.first].bd = hull[p.first].bd.join(Interval (v, v));
        }
        else
        {
          // nondet value: map to top
          hull[p.first].posIsInf ();
          hull[p.first].negIsInf ();
        }
      }
      
      DEBUG (print ());
    }
    
  };

  
  class SymbaBound : public Bound
  {
    typedef pair<ExprMap, EClass> Point; // (model, class)
    typedef vector<Point> PointVector;
    
  private:
    ExprFactory &efac;
    const ExprSet &varbase;
    ExprSet T; // Set of templates
    ExprSet boolVars; // Boolean variables
    VarBounds boolBound; // Bounds for bool vars
    UfoZ3 ctx;
    ZSolver<UfoZ3> z3; // Z3 object
    TCM U; // Under-approximation
    ExprVector E; // Equalities: {t=k|t<=k is an atomic proposition in phi}
    Equivalence<Expr> relation; // C.O.I. relation sets
    PushListPtrList queue;
    Expr phi;
    bool preProcessed;

  public:
    SymbaBound (ExprFactory &_e, const ExprSet &_v, Expr _phi) :
      efac(_e), varbase(_v),
      T(filterVars (varbase, true)),
      boolVars (filterVars (varbase, false)),
      ctx(_e), z3(ctx),
      U (efac, T),
      relation (_v),
      phi (closeFormula(_phi)), preProcessed(false)
    {
      // set z3 elim-and
      ZParams<UfoZ3> params (ctx);
      params.set (":elim-and", true);
      // Initialize bool bounds.
      forall (Expr b, boolVars)
        boolBound[b] = Interval::bot();

      preProcessing (phi); // do preprocessing if phi is given
    }

    SymbaBound (ExprFactory &_e, const ExprSet &_v) :
      efac(_e), varbase(_v),
      T(filterVars (varbase, true)),
      boolVars (filterVars (varbase, false)),
      ctx(_e), z3(ctx),
      U (efac, T),
      relation (_v),
      preProcessed(false)
      {
        // set z3 elim-and
        ZParams<UfoZ3> params (ctx);
        params.set (":elim-and", true);
        // Initialize bool bounds.
        forall (Expr b, boolVars)
          boolBound[b] = Interval::bot();
      }
    
    void preProcessing (Expr _p)
      {
        Stats::resume ("symb.pp.total");
        phi = closeFormula (_p);
        // Scan E
        scanTemplate ();
        scanFace (phi);

        assert (T.size() > 0);
        forall (Expr t, T) 
        { 
          U[t].related = relatedE (t);
          DEBUG (errs () << "Size of related of " << *t << " is " 
                 << U[t].related.size () << "\n");

          DEBUG(forall (Expr r, U[t].related)
                errs () << "\t" << *r << "\n";);
        }
        preProcessed = true;
        Stats::stop ("symb.pp.total");
      }

    /*
     * Bound One Template a time.
     */ 
    void boundOne (Expr t, Expr p)
      {
        if (!preProcessed) preProcessing (p);

        z3.push();
        assert (preProcessed);
        // Assert Phi
        z3.assertExpr (phi);
        bool cdir = false; // current direction for term ct, default: minimize

        while (optional<Point> p = globalPush (t, cdir))
        {
          U.join (p->first, p->second, back_inserter (queue));
          z3.assertExpr (mk<NEG>(U.gamma()));
        
          // exhaust queue
          unsigned step = 0;
          while (step++ < GlobalPushStep && queue.size() > 0)
          { 
            PushList* plist = queue.front();
            queue.pop_front ();

            // apply termPush on plist, join points produced
            // to U.
            while (plist->size() > 0 && !plist->isExtreme())
            {
              DEBUG(plist->print());
              
              Stats::resume ("tpush.total");
              PointVector newPts = termPush (*plist);
              Stats::stop ("tpush.total");
              if (!newPts.empty ())
              {
                forall (Point p, newPts)
                  U.join (p.first, p.second, back_inserter (queue));
                
                z3.assertExpr (mk<NEG>(U.gamma()));
              }
            }
          }

          // update current direction and term for opti-z3
          if (U[t].negExtreme) 
            cdir = true;
        }

        z3.pop();
      }

    /**
     * boundAll
     * stub. Demo the usage of globalPush and U.
     */
    void boundAll (Expr _p)
    {
      if (!preProcessed) preProcessing (_p);
      boundBool (phi, boolVars, boolBound);

      // Assert Phi
      z3.assertExpr (phi);
      
      Expr ct = (true) ? 
        *(T.begin()) : NULL; // current term: initialized to the first template
      bool cdir = false; // current direction for term ct, default: minimize

      while (optional<Point> p = globalPush (ct, cdir))
      {
        U.join (p->first, p->second, back_inserter (queue));
        z3.assertExpr (mk<NEG>(U.gamma()));
        
        // exhaust queue
        unsigned step = 0;
        while (step++ < GlobalPushStep && queue.size() > 0)
        { 
          PushList* plist = queue.front();
          queue.pop_front ();

          // apply termPush on plist, join points produced
          // to U.
          while (plist->size() > 0 && !plist->isExtreme())
          {
            DEBUG(plist->print());

            // do termpush if it uses less than 'balance' portion of time so far
            if ((double)Stats::sw["tpush.total"].getTimeElapsed() 
                / (double)Stats::sw["symb.total"].getTimeElapsed() < TermPushTime)
            {  
              //errs () << "tpush\n";
              Stats::resume ("tpush.total");
              PointVector newPts = termPush (*plist);
              Stats::stop ("tpush.total");
              if (!newPts.empty ())
              {
                forall (Point p, newPts)
                  U.join (p.first, p.second, back_inserter (queue));
	      
                z3.assertExpr (mk<NEG>(U.gamma()));
              }
            }
            else break;
          }
        }

        // update current direction and term for opti-z3
        if (!U.findOpenDir(ct, cdir))
          ct = NULL; // don't know what to optimize next
        
        //U.print();
      }
    }

    /**
     * globalPush
     * Produce a new point out of U.gamma and within phi.
     * side effect: assert not U.gamma()
     */
    optional<Point> globalPush ()
    {
      Stats::count ("symb.gpush");
      Stats::resume ("gpush.total");
      DEBUG (errs () << "global\n");

      optional<Point> res = optional<Point> ();

      tribool sat = z3.solve ();
      Stats::count ("smtcall");
      assert (!boost::indeterminate (sat));
      if (sat) 
      {
        Point p;
        findPoint (p);
        res = optional<Point>(p);
      }
      // else
      // no point can be produced algorithm should terminate

      Stats::stop ("gpush.total");
      return res;
    }

    /**
     * globalPush
     * Produce a new point out of U.gamma, within phi and optimize v.
     */
    optional<Point> globalPush (Expr v, bool max)
    {
      optional<Point> res = optional<Point> ();
      // Use default z3.
      if (!UseOptiZ3 || v==NULL) return globalPush ();
      //if (v == NULL) return res;
      //errs () << "OPTI: " << *v << ", " << max << "\n";

      // Use opti-z3.
      Stats::count ("symb.opt.gpush");
      Stats::resume ("gpush.total");
      

      tribool unbound = false;
      tribool sat = z3.solve_opti (unbound, v, max);

      Stats::count ("smtcall");
      assert (!boost::indeterminate (sat));
      
      if (unbound) // unboundedness detected
      {
        Stats::count ("opti.z3.unbounded");
        if (max) U.hull[v].posIsInf();
        else U.hull[v].negIsInf();
      }

      if (sat)
      {
        Point p;
        findPoint (p);
        res = optional<Point>(p);

        if (boost::indeterminate(unbound))
          errs () << *v << " not optimized.\n";
      }

      Stats::stop ("gpush.total");
      return res;
    }

    Expr floor (Expr v)
    {
      if (!isOpX<MPQ> (v)) return v;
      mpq_class mpq = getTerm<mpq_class> (v);
      
      mpz_class num = mpq.get_num ();
      mpz_class den = mpq.get_den ();
      if (den == 1 || den == 0) return v;
      
      mpq_class res;
      mpz_fdiv_q (res.get_num ().get_mpz_t (),
		  num.get_mpz_t (), den.get_mpz_t ());
      return mkTerm (res, v->efac ());
    }
    Expr ceil (Expr v)
    {
      if (!isOpX<MPQ> (v)) return v;
      mpq_class mpq = getTerm<mpq_class> (v);
      
      mpz_class num = mpq.get_num ();
      mpz_class den = mpq.get_den ();
      if (den == 1 || den == 0) return v;
      
      mpq_class res;
      mpz_cdiv_q (res.get_num ().get_mpz_t (),
		  num.get_mpz_t (), den.get_mpz_t ());
      return mkTerm (res, v->efac ());
    }
    
    /**
     * termPush
     */
    PointVector termPush (PushList &anchor)
    {
      Stats::count ("symb.tpush");
      DEBUG (errs () << "termPush.entered\n");
      PointVector res;
      
      EClass ec = anchor.pop();
      Expr t = anchor.getTerm();
      Template& tmp = U[t];

      bool max_dir = anchor.getDir()>0; // direction is to maximize

      Expr bnd = max_dir ? U.getMax (t) : U.getMin (t);

      Expr nbnd = max_dir ? ceil (bnd) : floor (bnd);
     
      tribool unbounded = false; // unbounded flag

      tribool sat = false;
      if (PreferIntSolution && nbnd != bnd)
      {
        DEBUG (errs () << "FRACTIONAL: from: " 
               << *bnd << " to: " << *nbnd 
               << ": ");
	  
        Stats::resume ("tpush.z3.push");
        z3.push();
        Stats::stop ("tpush.z3.push");
        z3.assertExpr (max_dir ? 
                       mk<GEQ> (t, nbnd) : mk<LEQ> (t, nbnd));
        sat = UseOptiZ3 ? 
          z3.solve_opti(unbounded, t, max_dir) : 
          z3.solve ();

        Stats::count ("smtcall");
        assert (!boost::indeterminate (sat));
        DEBUG (errs () << (sat ? "YES" : "NO") << "\n");
        if (!sat) 
        {
          Stats::resume ("tpush.z3.pop");
          z3.pop ();
          Stats::stop ("tpush.z3.pop");
        }
        
        if (unbounded)
        {
          Stats::count ("opti.z3.unbounded");
          //errs () << "z3.unbounded!\n";
          if (max_dir) U.hull[t].posIsInf();
          else U.hull[t].negIsInf();
        
          z3.pop();
          return res;
        }
        
      }
      
      if (!sat)
      {
        Stats::resume ("tpush.z3.push");
        z3.push();
        Stats::stop ("tpush.z3.push");

        z3.assertExpr (max_dir ? 
                       mk<GT> (t, bnd) : mk<LT> (t, bnd));

        // 1st check
        DEBUG (errs () << "1st\n");
      
        Stats::count ("tpush.fst.cnt");
        Stats::resume ("tpush.fst.time");
        tribool sat = UseOptiZ3 ?
          z3.solve_opti(unbounded, t, max_dir) : z3.solve ();
        Stats::count ("smtcall");
        assert (!boost::indeterminate (sat));
        Stats::stop ("tpush.fst.time");

        if (!sat)
        {
          if (max_dir) tmp.posDone ();
          else tmp.negDone ();

          Stats::resume ("tpush.z3.pop");
          z3.pop();
          Stats::stop ("tpush.z3.pop");
          return res;
        }      

        if (AddTermPushPoints) 
        {
          Point pout; // 1st point
          findPoint (pout);
          res.insert (res.begin (), pout);
        }
        
        if (unbounded)
        {
          //errs () << "z3.unbounded!\n";
          Stats::count ("opti.z3.unbounded");
          if (max_dir) U.hull[t].posIsInf();
          else U.hull[t].negIsInf();
        
          z3.pop();
          return res;
        }
        
      }
      
      // 2nd check
      {
        DEBUG (errs () << "2nd\n");
        Expr be = bit2expr(ec);
        z3.assertExpr (be);
      }
      
      //DEBUG(errs() << "bit2expr: " << *(bit2expr(ec)) << "\n");

      Stats::count ("tpush.snd.cnt");

      Stats::resume ("tpush.snd.time");
      sat = UseOptiZ3 ? z3.solve_opti(unbounded, t, max_dir) : z3.solve ();
      Stats::count ("smtcall");
      assert (!boost::indeterminate (sat));
      Stats::stop ("tpush.snd.time");
      if (!sat)
      {
        anchor.removeStronger (ec);
        Stats::resume ("tpush.z3.pop");
        z3.pop();
        Stats::stop ("tpush.z3.pop");
        return res;
      }
      
      if (unbounded)
      {
        //errs () << "z3.unbounded!\n";
        Stats::count ("opti.z3.unbounded");
        if (max_dir) U.hull[t].posIsInf();
        else U.hull[t].negIsInf();
        
        z3.pop();
        return res;
      }

      if (AddTermPushPoints)
      {
        Point pmid; // 2nd point
        findPoint (pmid);
        res.insert (res.begin (), pmid);
      }
      

      // 3rd check
      DEBUG (errs () << "3rd\n");
      ExprVector &rE = U[t].related;
      {
        Expr seq = strongerEQ (rE, ec);
        z3.assertExpr (seq);
      }
      

      DEBUG (errs () << "strongerEQ: " << *(strongerEQ (rE, ec)) << "\n");

      Stats::count ("tpush.trd.cnt");
      Stats::resume ("tpush.trd.time");
      sat = UseOptiZ3 ? z3.solve_opti(unbounded, t, max_dir) : z3.solve ();
      Stats::count ("smtcall");
      assert (!boost::indeterminate (sat));
      Stats::stop ("tpush.trd.time");
      if (!sat || unbounded)
      {
        if (unbounded) Stats::count ("opti.z3.unbounded");
        if (max_dir)
        {
          U.hull[t].posIsInf();
          assert (U.hull[t].pos.extreme);
        }
        else
        {
          U.hull[t].negIsInf();
          assert (U.hull[t].neg.extreme);
        }
        
        assert (anchor.extreme);
        // anchor.reachExtreme();

        Stats::resume ("tpush.z3.pop");
        z3.pop();
        Stats::stop ("tpush.z3.pop");
        return res;
      }
      
      Point pinf; // 3rd point
      findPoint (pinf);
      res.insert (res.begin (), pinf);
      
      Stats::resume ("tpush.z3.pop");
      z3.pop();
      Stats::stop ("tpush.z3.pop");
      
      return res;
    }

    void findPoint (Point &p)
    {
      Stats::resume ("symb.pt.time");
      findModel (p.first);
      p.second = findClass ();
      Stats::stop ("symb.pt.time");
    }
    
    /* return a expr representation of the current model */
    void findModel (ExprMap &p)
    {
      Stats::count ("symb.model");
      forall (Expr t, T) p[t] = z3.getModel().eval (t);
    }

    /* return the bit set that represents the equivalence
     * class that the current model is in */ 
    EClass findClass ()
    {
      EClass res (E.size());
      // generate bit vector
      unsigned i = 0;
      forall (Expr e, E)
      {
        Expr m = z3.getModel().eval (e);
        if (isOpX<TRUE>(m))
          res[i] = true;
        else if (isOpX<FALSE>(m))
          res[i] = false;
        else // partial model
        {
          // XXX can split this case to mark more eclass as visited.
          res[i] = false;
        }
        ++i; 
      }
      // return the inserted element
      //printBit (res);
      //checkClass (res);
      return res;
    }
    
    void checkClass (EClass res)
    {
      ExprVector used;
      ExprVector ec;
      ExprVector terms;
      // Filter inequalities
      filter(phi, ConsFilter(), back_inserter (terms));
      
      ExprVector vv;
      forall (Expr t, terms)
      {
        Expr val = z3.getModel().eval (t);
        if (isOpX<TRUE> (val))
        {
          ec.push_back (mk<IMPL> (mk<ASM> (t), t));
          vv.push_back (t);
        }
        else if (isOpX<FALSE> (val))
        {
          Expr nt = mk<NEG> (t);
          ec.push_back (mk<IMPL> (mk<ASM> (nt), nt));
          vv.push_back (nt);
        }
      }
      
      assert 
	((bool)!z3n_is_sat (mk<AND> (mknary<AND> (mk<TRUE> (efac), 
						 vv.begin (),
						 vv.end ()),
				    mk<NEG> (phi))));

      UfoZ3 ctx (efac);
      ZSolver<UfoZ3> _z3 (ctx);
      _z3.assertExpr (mk<NEG> (phi));
      tribool r = 
	mus_basic (_z3, 
		   mknary<AND> (mk<TRUE> (efac), ec.begin (), ec.end ()), 
		   back_inserter (used));      
      errs () << "CC:"
	      << " res: " << r
	      << " orig: " << ec.size () 
	      << " usd: " << used.size ()
	      << "\n";
    }
    

    /**
     * relatedE. Find the related edges for term t.
     * param[in] Expr t
     * return ExprVector: related faces
     */
    ExprVector relatedE (Expr t)
    {
      ExprVector res;
      forall (Expr e, E)
        if (relation.isRelated (e, t))
          res.push_back (e);
      
      return res;
    }
    
    /**
     * Override [] operator.
     * Provide access to variable bounds.
     * param[in] variable var
     */
    Interval operator [] (Expr var)
    {
      if (boolVars.count (var) > 0)
        return boolBound[var];
      
      return U[var].getBound();
    }

    /**
     * Get equalities from bit vectors
     * param[in] BitVector& b: a bit vector representing the satisfied equalities
     * return ExprVector: a set of satisfied equalities
     */
    Expr bit2expr (EClass& b)
    {
      ExprVector g = bit2eq (b);
      return mknary<AND> (mk<TRUE>(efac), g.begin(), g.end());
    }

    ExprVector bit2eq (EClass& b)
    {
      ExprVector g;
      unsigned i = 0;
      forall (Expr e, E)
      {
        if (b[i])
          g.push_back (e);
        ++i;
      }
      return g;
    }

    /**
     * assertStrongerEQ. Make an assertion that asks for a model in
     * a stronger equivalence class.
     * param[in] ExprVector& rf: Available faces
     * param[in] ExprVector& pc: Faces in the current equivalence class
     */
    Expr strongerEQ (ExprVector& rF, EClass& b)
    {
      // rF / ||p|| (set minus)
      ExprVector Fmp;
      ExprVector pC = bit2eq (b);
    
      set_difference (rF.begin(), rF.end(), pC.begin(), pC.end(),
                      back_inserter(Fmp));

      //return boolop::land (
      //  mknary<AND> (mk<TRUE>(efac), pC.begin(), pC.end()),
      //  mknary<OR> (mk<FALSE>(efac), Fmp.begin(), Fmp.end()));
      return mknary<OR> (mk<FALSE>(efac), Fmp.begin(), Fmp.end());
    }

    /**
     * scanFace
     * Relate vars appear in the same constraint
     * param[in] Expr phi
     */
    void scanFace (Expr phi)
    {
      ExprSet tmpF;
      // Filter inequalities
      filter(phi, ConsFilter(), std::inserter(tmpF, tmpF.begin()));

      Stats::uset ("phi.term.sz", tmpF.size ());

      ExprSet tmpFeq; //Remove duplicates
      forall (Expr f, tmpF)
        tmpFeq.insert (mk<EQ> (f->left(), f->right()));

      forall (Expr f, tmpFeq)
      {
        ExprSet vars = collectVars (f);
        forall (Expr var, vars)
        {
          // Relate vars in the same f with f itself
          relation.relate (var, f);
        }
      }
      
      // Copy back to F
      copy (tmpFeq.begin(), tmpFeq.end(), back_inserter (E));
    }

    /**
     * scanTemplate
     * Relate vars appear in the same template
     */
    void scanTemplate ()
    {
      forall (Expr t, T)
      {
        ExprSet vars = collectVars (t);
        forall (Expr var, vars)
        {
          // Relate variables within the same template
          relation.relate (var, t);
        }
      }
    }

    /**
     * print bit vector
     */
    void printBit (const EClass& b)
    {
      errs () << "Bit: ";
      unsigned i;
      for (i=0; i<b.size(); ++i)
        errs () << b[i];
      errs () << "\n";
    }

    void validate ()
    {
      UfoZ3 ctx (efac);
      ZSolver<UfoZ3> _z3 (ctx);
      
      _z3.assertExpr (phi);
      ExprVector res;
      forall (Expr t, T) res.push_back (U[t].getBound ().gamma (t));
      _z3.assertExpr (mk<NEG> (mknary<AND> (mk<TRUE> (efac), 
					    res.begin (),
					    res.end ())));
      if (_z3.solve ())
      {
        errs () << "PHI NOT COVERED BY RES: BAD MODEL: \n";
        forall (Expr t, T)
          errs () << *t << ": " << *_z3.getModel().eval (t) << "\n";
      }
      else
        errs () << "VALIDATE SUCCESS\n";
    }
    
  };
}


#endif
