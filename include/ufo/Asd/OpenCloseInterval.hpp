/** Interval that can be open or closed */
#ifndef __OPEN_CLOSE_INTERVAL__HPP_
#define __OPEN_CLOSE_INTERVAL__HPP_

#include "ufo/Asd/Interval.hpp"

namespace ufo
{
  class OpenCloseInterval : public Interval
  {
  public:
    bool lclose, rclose; // Left close and Right close

    OpenCloseInterval () : Interval (), 
                           lclose(false), 
                           rclose(false) {}
    
    OpenCloseInterval (const mpq_class &min, const mpq_class &max, 
                       bool _lclose, bool _rclose) : 
      Interval (min, max),
      lclose (_lclose),
      rclose (_rclose) 
      {
        // max > min or (max == min and both side are closed)
        assert (!leq (max, min) ||
                (eq (max, min) && lclose && rclose));
      }
    
    bool operator == (const OpenCloseInterval &rhs) const
      {
        return lclose == rhs.lclose && rclose == rhs.rclose
          && eq (first, rhs.first) && eq (second, rhs.second);
      }

    bool operator != (const OpenCloseInterval &rhs) const
      {
        return !(*this == rhs);
      }

    bool operator <= (const OpenCloseInterval &rhs) const
      {
        assert (false && "Unimplemented!");
      }

    /**
     * Test if two intervals are adjacent (not overlap)
     */
    bool isAdjacent (const OpenCloseInterval &rhs) const
      {
        if (eq(first, rhs.second))
          return (lclose && !rhs.rclose ||
                  !lclose && rhs.rclose);
            
        if (eq(second, rhs.first))
          return (rclose && !rhs.lclose ||
                  !rclose && rhs.lclose);
        
        return false;
      }

    OpenCloseInterval meet (const OpenCloseInterval &rhs) const
      {
        assert (false && "Unimplemented!");
      }

    /**
     * Compute the join of two open-close-intervals
     * update the size of the increment on both direction +/-
     */
    OpenCloseInterval join (const OpenCloseInterval &rhs) const
      {
        bool l = lclose, r = rclose;
        if (eq (first, rhs.first))
          l = lclose || rhs.lclose;
        if (eq (second, rhs.second))
          r = rclose || rhs.rclose;
        if (!leq (first, rhs.first))
          l = rhs.lclose;
        if (!leq (rhs.second, second))
          r = rhs.rclose;

        return OpenCloseInterval (min (first, rhs.first), 
                                  max (second, rhs.second),
                                  l, r);
      }

    Expr gamma (Expr x) const
      {
        if (isBot ()) return mk<FALSE> (x->efac());
        if (isTop ()) return mk<TRUE> (x->efac());
        
        Expr res = mk<TRUE> (x->efac());
        if (!isNegInf (first))
          res = boolop::land (res,
                              lclose ? mk<GEQ> (x, mkTerm (first, x->efac())) :
                              mk<GT> (x, mkTerm (first, x->efac())));
        
        if (!isPosInf (second))
          res = boolop::land (res,
                              rclose ? mk<LEQ> (x, mkTerm (second, x->efac())) :
                              mk<LT> (x, mkTerm (second, x->efac())));

        return res;
      }

    std::string toString () const
      {
        std::string res = "";
        
        res += lclose ? "[" : "(";
        res += first.get_str();
        res += ",";
        res += second.get_str();
        res += rclose ? "]" : ")";

        return res;
      }
  };
}


#endif
