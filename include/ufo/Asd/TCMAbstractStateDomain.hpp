#ifndef __TCM_ABSTRACT_STATE_DOMAIN_HPP
#define __TCM_ABSTRACT_STATE_DOMAIN_HPP

#include "ufo/Cpg/Lbe.hpp"
#include "AbstractStateDomain.hpp"
#include "BoolAbstractStateDomain.hpp"
#include "Interval.hpp"

/**
 * TCM (Template Constraint Matrix) Abstract State Domain.
 * Based on VMCAI05 paper: Scalable analysis of linear systems using mathematical
 * programming.
 */

namespace ufo
{
  typedef map<Expr, Interval> TCMState;

  /**
   * TCM Abstract State Value
   */
  class TCMAbstractStateValue : public AbstractStateValue
  {
  private:
    TCMState val;
  public:
    TCMAbstractStateValue (TCMState _s) : val(_s) {}

    TCMState& getVal() { return val; }
  };

  /**
   * TCM Abstract State Domain
   */
  class TCMAbstractStateDomain : public AbstractStateDomain
  {
  private:
    ExprFactory &efac;
    const ExprSet &vars; // tracked variables
    
  public:
    TCMAbstractStateDomain (const TCMAbstractStateDomain&);
    TCMAbstractStateDomain (ExprFactory &_efac, const ExprSet &_v) :
      efac(_efac), vars(_v) {}
    ~TCMAbstractStateDomain () {}

    std::string name () { return "TCM Abstract Domain"; }

    /**
     * abs: create an abstract state from TCMState map
     */
    AbstractState abs (TCMState _s)
    {
      return AbstractState (new TCMAbstractStateValue (_s));
    }

    /**
     * getVars: return tracked variables in the domain
     */
    const ExprSet& getVars () { return vars; }

    /**
     * getVal: return a reference to the TCMState map
     */
    TCMState& getVal (AbstractState _a)
    {
      assert (_a && "Abstract State is NULL");
      return dynamic_cast<TCMAbstractStateValue*>(&*_a)->getVal();
    }

    /**
     * point2state.
     * param[in] Range (map) from dimensions to values
     * return an abstract state
     */
    template <typename R>
    AbstractState point2state (R &rm)
    {
      TCMState s;
      typedef pair<Expr, Expr> ExprPair;
      
      foreach (ExprPair p, rm)
      {
        if (isOpX<MPQ> (p.second))
        {
          mpq_class v = getTerm<mpq_class>(p.second);
          s[p.first] = Interval (v, v);
        }
        else
          // nondet value: map to top
          s[p.first] = Interval::top();
      }
      return abs (s);
    }

    AbstractState alpha (CutPointPtr loc, Expr val)
    {
      assert (0 && "Unimplemented.");
      TCMState s;
      return abs (s);
    }

    Expr gamma (CutPointPtr loc, AbstractState v)
    {
      TCMState& s = getVal (v);
      ExprVector g;
      foreach (Expr _v, vars)
      {
        assert (s.count(_v) > 0);
        g.push_back (s[_v].gamma(_v));
      }
      return mknary<AND>(mk<TRUE>(efac), g.begin(), g.end());
    }

    AbstractState top (CutPointPtr loc)
    {
      TCMState s;
      forall (Expr _v, vars)
        s[_v] = Interval::top();
      return abs (s);
    }

    AbstractState bot (CutPointPtr loc)
    {
      TCMState s;
      forall (Expr _v, vars)
        s[_v] = Interval::bot();
      return abs (s);
    }

    bool isTop (CutPointPtr loc, AbstractState v)
    {
      TCMState& s = getVal (v);
      forall (Expr _v, vars)
      {
        assert (s.count (_v) > 0);
        if (!s[_v].isTop()) return false;
      }
      return true;
    }

    bool isBot (CutPointPtr loc, AbstractState v)
    {
      TCMState& s = getVal (v);
      forall (Expr _v, vars)
      {
        assert (s.count (_v) > 0);
        if (s[_v].isBot()) return true;
      }
      return false;
    }

    bool isLeq (CutPointPtr loc,
                AbstractState v1, AbstractState v2)
    {
      assert (0 && "Unimplemented");
      return false;
    }

    bool isLeq (CutPointPtr loc, AbstractState v1,
                AbstractStateVector const &u)
    {
      assert (0 && "Unimplemented");
      return false;
    }

    AbstractState copy (AbstractState _x)
    {
      TCMState& b = getVal (_x);
      TCMState c = TCMState (b);

      return abs (c);
    }

    virtual bool isEq (CutPointPtr lco,
                       AbstractState v1, AbstractState v2)
    {
      assert (0 && "Unimplemented");
      return false;
    }

    virtual AbstractState join (CutPointPtr loc,
                                AbstractState v1, AbstractState v2)
    {
      TCMState s;
      TCMState& s1 = getVal (v1);
      TCMState& s2 = getVal (v2);

      forall (Expr _v, vars)
      {
        assert (s1.count(_v) > 0 && s2.count(_v) > 0);
        assert (s1.size() == s2.size());
        s[_v] = s1[_v].join(s2[_v]);
      }
      return abs (s);
    }

    /**
     * Join that is aware of the templates in v1 being updated
     * param[out] ExprVector &x: updated templates in v1
     */
    virtual AbstractState joinAware (CutPointPtr loc,
        AbstractState v1, AbstractState v2, ExprVector &x)
    {
      TCMState s;
      TCMState& s1 = getVal (v1);
      TCMState& s2 = getVal (v2);

      forall (Expr _v, vars)
      {
        assert (s1.count(_v) > 0 && s2.count(_v) > 0);
        assert (s1.size() == s2.size());
        // !(s2 <= s1) and s1 is not bot
        if (!(s2[_v] <= s1[_v]))
        {
          //errs () << "S1:[" << s1[_v].first << "," << s1[_v].second << "]\n";
          //errs () << "S2:[" << s2[_v].first << "," << s2[_v].second << "]\n";
          x.push_back (_v);
        }

        s[_v] = s1[_v].join(s2[_v]);
      }
      return abs (s);
    }

    virtual AbstractState meet (CutPointPtr loc,
                                AbstractState v1, AbstractState v2)
    {
      TCMState s;
      TCMState& s1 = getVal (v1);
      TCMState& s2 = getVal (v2);

      forall (Expr _v, vars)
      {
        assert (s1.count(_v) > 0 && s2.count(_v) > 0);
        assert (s1.size() == s2.size());
        s[_v] = s1[_v].meet(s2[_v]);
      }
      return abs (s);
    }

    virtual AbstractState widen (CutPointPtr loc,
                                 AbstractState v1, AbstractState v2)
    {
      TCMState s;
      TCMState& s1 = getVal (v1);
      TCMState& s2 = getVal (v2);

      forall (Expr _v, vars)
      {
        assert (s1.count(_v) > 0 && s2.count(_v) > 0);
        assert (s1.size() == s2.size());
        s[_v] = s1[_v].widen(s2[_v]);
      }
      return abs (s);
    }

    AbstractState post (AbstractState pre, 
                        CutPointPtr src, CutPointPtr dst)
    {
      assert (0 && "Unimplemented");
      TCMState s;
      return abs (s);
    }

    /** Called to hint the abstract domain that it is useful to be
	able to preciiselly represent hint expression at a given
	location*/
    void refinementHint (CutPointPtr loc, Expr hint)
    {
      assert (0 && "Unimplemented");
    }
    
    bool isFinite ()
    {
      return false;
    }

    AbstractState widenWith (Loc loc, AbstractStateVector const &oldV, 
                             AbstractState newV)
    {
      assert (0 && "Unimplemented");
      TCMState s;
      return abs (s);
    }
	
    AbstractState post (const LocLabelStatePairVector &pre, 
                        Loc dst,
                        BoolVector &deadLocs)
    {
      assert (0 && "Unimplemented");
      TCMState s;
      return abs (s);
    }

    /**
     * getBound.
     * param[in] Expr _name: variable name
     * param[in] AbstractState _s: state
     * return value of _name in state
     */
    Interval getBound (Expr _name, AbstractState _s)
    {
      TCMState& s = getVal (_s);
      assert (s.count (_name) > 0);
      return s[_name];
    }

    /**
     * Dimensional join. Join on one dimension.
     */
    void dimJoin (Expr _name, AbstractState _s, Interval _b)
    {
      TCMState& s = getVal (_s);
      assert (s.count (_name) > 0);
      s[_name] = s[_name].join(_b);
    }

    void print (AbstractState _s)
    {
      errs () << "TCM State:\n";
      TCMState& s = getVal (_s);
      foreach (Expr _v, vars)
      {
        assert (s.count(_v) > 0);
        errs () << *_v << " : [" << s[_v].first << "," << s[_v].second 
                << "]\n";
      }
    }
  };

  /**
   * Product of Bool and TCM
   */
  class BoolTCMProduct : public ProductAbstractStateDomain
  {
  private:
    BoolAbstractStateDomain babs;
    TCMAbstractStateDomain tabs;
  public:
    BoolTCMProduct (ExprFactory &_e, ExprSet &_b, ExprSet &_r) :
      ProductAbstractStateDomain (babs, tabs),
      babs (BoolAbstractStateDomain (_e, _b)),
      tabs (TCMAbstractStateDomain (_e, _r)) {}
    ~BoolTCMProduct () {}

    template <typename R>
    AbstractState point2state (R &bm, R &rm)
    {
      return absState (babs.point2state (bm), tabs.point2state (rm));
    }

    AbstractState joinAware (
                             AbstractState v1, AbstractState v2,
                             ExprVector &x)
    {
      AbsStatePair &p1 = getAbsVal (v1);
      AbsStatePair &p2 = getAbsVal (v2);
      
      return absState (babs.join (CutPointPtr(), p1.first, p2.first),
                       tabs.joinAware (CutPointPtr(), p1.second, p2.second, x));
    }
    
    Interval getBound (Expr _x, AbstractState _s)
    {
      AbsStatePair &pair = getAbsVal (_s);
      if (babs.getVars().count (_x) > 0)
        return babs.getBound (_x, pair.first);
      else if (tabs.getVars().count(_x) > 0)
        return tabs.getBound (_x, pair.second);
      else assert (0 && "Unreachable");
    }

    /**
     * joinBound. (currently only for TCM domain)
     */
    void joinBound (Expr _x, AbstractState _s, Interval _b)
    {
      AbsStatePair &pair = getAbsVal (_s);
      assert (tabs.getVars().count (_x) > 0);
      tabs.dimJoin (_x, pair.second, _b);
    }

    AbstractState copy (AbstractState _x)
    {
      AbsStatePair &pair = getAbsVal (_x);
      return absState (babs.copy(pair.first),
                       tabs.copy(pair.second));
    }

    void print (AbstractState _s)
    {
      AbsStatePair &pair =  getAbsVal (_s);
      babs.print (pair.first);
      tabs.print (pair.second);
    }
  };
}

#endif
