#ifndef __EXPR_GLPK__H_
#define __EXPR_GLPK__H_

#include "ufoglpk.hpp"

using namespace ufoglpk;
using namespace expr;
using namespace std;

namespace ufo
{
  namespace glpk
  {
    typedef map<Expr, double> ExprDoubleMap;

    // Gather nested plus to n-ary form
    struct GatherPlus
    {
      ExprSet &newArgs;
      GatherPlus (ExprSet &_a) :newArgs(_a) {}

      VisitAction operator () (Expr e)
      {
        if (!isOpX<PLUS> (e))
        {
          newArgs.insert (e);
          return VisitAction::skipKids();
        }
        else
          return VisitAction::doKids();
      }
    };

    /**
     * Interpret the result of simplex solving
     *
     * max: is maximizing
     */
    inline mpq_class interpret_result (LProb &lp, bool max)
    {
      int status = lp.get_problem_status();
      mpq_class lb;

      if (status == GLP_OPT)
        lb = mpq_class (lp.get_optimal_solution());
      else if (status == GLP_NOFEAS)
        lb = max ? negInf() : posInf();
      else if (status == GLP_UNBND)
        lb = max ? posInf() : negInf();
      else assert (false && "Invalid solution status");

      return lb;
    }


    inline Expr gatherPlus (Expr pe)
    {
      if (!isOpX<PLUS>(pe)) return pe;

      ExprSet g;
      GatherPlus gp(g);
      dagVisit(gp, pe);

      return mknary<PLUS> (g.begin(), g.end());
    }

    /**
     * Convert a vector of linear constraints in Expr type to a glpk matrix.
     */
    template <typename var2dim_type>
    void expr2matrix (LProb &p, vector<Expr> &cons, var2dim_type &vMap)
    {
      int r = 0; // row number
      forall (Expr c, cons)
      {
        assert (isOp<ComparissonOp> (c));
        //errs () << "CONS: " << *c << "\n";
        // Canonize linear constraint into the
        // form: a_ix_i {=,>=,<=} k
        Expr canoc = z3n_lite_simplify (
          c->efac().mkBin (c->op(),
                           mk<PLUS>(c->left(), mk<UN_MINUS> (c->right())),
                           mkTerm(mpq_class(0), c->efac())));
        //errs () << "CANO: " << *canoc << "\n";
        if (!isOp<ComparissonOp> (canoc))
          continue;

        Expr poly = canoc->left();
        Expr constant = canoc->right();

        //errs () << "error: " << *poly << "\n";
        assert (isOpX<PLUS> (poly) || isOpX<MULT> (poly) || bind::isRealConst (poly));
        assert (isOpX<MPQ> (constant));

        // Get coefficients
        if (isOpX<PLUS> (poly))
        {
          Expr gpoly = gatherPlus(poly);

          forall (Expr v, make_pair (gpoly->args_begin(), gpoly->args_end()))
          {
            if (isOpX<MULT> (v))
            {
              Expr tm = v;
              assert (isOpX<MPQ> (tm->left()));
              assert (bind::isRealConst (tm->right()));
              mpq_class coef = getTerm<mpq_class>(tm->left());
              p.set_coefficient (r, vMap[tm->right()], coef.get_d());
            }
            else if (bind::isRealConst (v))
            {
              p.set_coefficient (r, vMap[v], 1.0);
            }
          }
        }
        else if (isOpX<MULT> (poly))
        {
          assert (isOpX<MPQ> (poly->left()));
          assert (bind::isRealConst (poly->right()));
          mpq_class coef = getTerm<mpq_class>(poly->left());
          p.set_coefficient (r, vMap[poly->right()], coef.get_d());
        }
        else
        {
          // Single real var
          p.set_coefficient (r, vMap[poly], 1.0);
        }

        // Get constant
        mpq_class ct = getTerm<mpq_class>(constant);
        double ct_d = ct.get_d();

        if (isOpX<GEQ> (canoc))
          p.set_row_lower_bound (r++, ct_d);
        else if (isOpX<LEQ> (canoc))
          p.set_row_upper_bound (r++, ct_d);
        else if (isOpX<EQ> (canoc))
        {
          p.set_row_lower_bound (r, ct_d);
          p.set_row_upper_bound (r++, ct_d);
        }
        else assert (false && "Unreachable");

      }
    }

    /**
     * Given a vector of linear constraints and a set of variables
     * Return upper and lower bounds for each variable using linear
     * programming library glpk.
     */
    inline map<Expr, Interval> getBounds (vector<Expr> &_c, set<Expr> &_v)
    {
      map<Expr, Interval> res;
      map<Expr, int> vMap;

      int i = 0;
      forall (Expr c, _c)
      {
        ExprSet vars = Bound::collectVars (c);
        // map vars to dims
        forall (Expr v, vars)
        {
          if (vMap.count (v) == 0)
            vMap[v] = i++;
        }
      }

      LProb prob (_c.size(), i);
      expr2matrix (prob, _c, vMap);

      // make objective function and solve
      forall (Expr v, _v)
      {
        if (vMap.count (v) == 0)
        {
          if (bind::isRealConst(v)) // variable not appeared in the formula
          {
            res[v] = Interval::top();
            continue;
          }

          // The objective function is a term: e.g., x+y, x-y, 3x
          assert (isOpX<PLUS>(v) || isOpX<MINUS>(v) || isOpX<MULT>(v));

          Expr simp = z3n_lite_simplify (v);
          if (isOpX<MULT>(simp))
          {
            assert (vMap.count(simp->right()) > 0);
            mpq_class coeff = getTerm<mpq_class>(simp->left());
            prob.set_objective_var (vMap[simp->right()], coeff.get_d());
          }
          else
          {
            assert (isOpX<PLUS> (simp));
            // Do not support objective function containing constants: x+3
            forall (Expr tm, make_pair(simp->args_begin(), simp->args_end()))
            {
              if (isOpX<MULT> (tm))
              {
                assert (vMap.count(tm->right()) > 0);
                mpq_class coeff = getTerm<mpq_class>(tm->left());
                prob.set_objective_col (vMap[tm->right()], coeff.get_d());
              }
              else if (bind::isRealConst (tm))
              {
                assert (vMap.count(tm) > 0);
                prob.set_objective_col (vMap[tm], 1.0);
              }
              else assert (0 && "Objective function containing constants not supported!");
            }
          }
        }
        else // the objective is a single variable
        {
          prob.set_objective_var (vMap[v], 1.0);
        }

        prob.load_problem();
        prob.set_obj_dir(false);
        prob.solve ();
        mpq_class lb = interpret_result (prob, false);

        //prob.reset();
        //prob.load_problem();
        prob.set_obj_dir (true);
        prob.solve();

        mpq_class ub = interpret_result (prob, true);

        res[v] = Interval (lb,ub);

        //prob.print(cerr);
        //errs () << "RESULT: [" << res[v].first << "," << res[v].second << "]\n";
        // reset problem instance after simplex solving
        prob.reset();


      }

      return res;
    }


    /**
     * Test on an example:
     * Minimize: Z=-2x-3y-4z
     * Subject to
     * 3x+2y+z<=10
     * 2x+5y+3z<=15
     * x,y,z>=0
     * Result should be min(Z)=-20
     */
    /*
      static void test ()
      {
      errs () << "Start Testing GLPK\n";

      LProb prob (4,4);

      prob.set_coefficient (0,0,1.0);
      prob.set_coefficient (1,1,1.0);
      prob.set_coefficient (2,2,1.0);
      prob.set_coefficient (3,3,1.0);
      //prob.set_coefficient (1,1,5.0);
      //prob.set_coefficient (1,2,3.0);

      prob.set_row_upper_bound (0, 0.0);
      prob.set_row_upper_bound (1, 0.0);
      prob.set_row_lower_bound (1, 0.0);
      prob.set_row_lower_bound (2, 0.0);
      prob.set_row_lower_bound (3, 0.0);
      //prob.set_col_lower_bound (0, 0.0);
      //prob.set_col_lower_bound (1, 0.0);
      //prob.set_col_lower_bound (2, 0.0);

      prob.set_objective_col (2, 1.0);
      //prob.set_objective_col (1, -3.0);
      //prob.set_objective_col (2, -4.0);

      prob.load_problem ();
      prob.print(cerr);
      prob.solve ();

      //pair<long,long> res =  prob.get_optimal_value_fractional ();
      //errs () << "RESULT: " << res.first << "/" << res.second << "\n";
      if (prob.get_problem_status() == GLP_OPT)
      errs () << "RESULT: " << prob.get_optimal_solution() << "\n";

      prob.set_obj_dir (true);

      prob.solve ();
      if (prob.get_problem_status() == GLP_OPT)
      errs () << "RESULT: " << prob.get_optimal_solution() << "\n";


      //prob.set_objective_col (3, 0.0);
      prob.set_objective_var (0, 1.0);
      prob.rebuild_objective_coefficients();
      prob.set_obj_dir (false);
      prob.print(cerr);

      prob.solve ();
      if (prob.get_problem_status() == GLP_OPT)
      errs () << "RESULT: " << prob.get_optimal_solution() << "\n";

      prob.set_obj_dir (true);
      prob.solve();
      if (prob.get_problem_status() == GLP_OPT)
      errs () << "RESULT: " << prob.get_optimal_solution() << "\n";

//res = prob.get_optimal_value_fractional ();
//errs () << "RESULT: " << res.first << "/" << res.second << "\n";

}
    */
  }
}

#endif
