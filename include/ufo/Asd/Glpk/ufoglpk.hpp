/**
 * Interface to GNU Linear Programming Kit (GLPK).
 * Code adapted from Sriram Sankanarayanan's project
 * LpInv: Prototype Program Analyzer for Linear Systems
 * using Linear Programming.
 *
 * Link to the project host page:
 * http://www.cs.colorado.edu/~srirams/Software/lpinv.html
 */

#ifndef __UFO_GLPK_HPP_
#define __UFO_GLPK_HPP_

#include "glpk.h"

using namespace std;
using namespace boost;

namespace ufoglpk
{
  typedef glp_prob * prob_ptr;
  typedef pair<int, int> Coordinate;
  typedef map<Coordinate, double> CoeffMap;
  typedef pair<int, double> IntDoublePair;
  typedef map<int, double> IntDoubleMap;

  class LProb
  {
#define _ASSERT_VALID_ROW(row) (  assert ((row >= 0 && row <_m) && "Row index out of range") )
#define _ASSERT_VALID_COL(col) (  assert ((col >= 0 && col <_n) && "Col index out of range") )
#define TOLERANCE_LIMIT ((double) 0.00001)
#define WITHIN_LIMITS(a,b) ( ((double) a >= (double) b - TOLERANCE_LIMIT) && ((double) a <= (double) b + TOLERANCE_LIMIT) )

  private:
    int _m, _n; // _m and _n are the number of row and column variables
    bool _maximize; // _maximize is true if the objective is to maximize

    CoeffMap _cm; // _cm maps coordinates to non-zero double value
    IntDoubleMap _colobjs; // set the column coefficients of the objective function

    // upper and lower bounds for row and column variables
    IntDoubleMap _upper_bounds_rows;
    IntDoubleMap _lower_bounds_rows;
    IntDoubleMap _upper_bounds_cols;
    IntDoubleMap _lower_bounds_cols;

    prob_ptr _lp; // the problem object pointer

  public:
    LProb (int m, int n): _m(m), _n(n), _maximize(false)
    {
      _lp = glp_create_prob(); // create problem object
      glp_set_obj_dir(_lp, (_maximize) ? GLP_MAX : GLP_MIN); // set optimize direction

      assert (_m!=0 && _n!=0);
      // add all the rows and columns
      glp_add_rows(_lp, _m);
      glp_add_cols(_lp, _n);
    }

    // copy constructor
    LProb (LProb const & c): _m(c._m),
                             _n(c._n),
                             _cm(c._cm),
                             _colobjs(c._colobjs),
                             _upper_bounds_rows(c._upper_bounds_rows),
                             _lower_bounds_rows(c._lower_bounds_rows),
                             _upper_bounds_cols(c._upper_bounds_cols),
                             _lower_bounds_cols(c._lower_bounds_cols)
    {
      // now build everything
      _lp=c._lp;

    }

    ~LProb()
    {
      glp_delete_prob(_lp);
    }

    /**
     * reset the problem object to before-solve-state
     */
    void reset()
    {
      glp_delete_prob(_lp);

      _lp = glp_create_prob();

      glp_add_rows( _lp, _m);
      glp_add_cols( _lp, _n);
    }

  private:
    ostream & operator << (ostream & out)
    {
      print(out);
      return out;
    }

    void print_row_bounds (ostream & out,
                           const IntDoubleMap &tpr,
                           string dirn) const
    {
      map<int,double>::const_iterator vi;

      for (vi= tpr.begin(); vi != tpr.end(); ++vi)
      {
        int i = vi->first;
        double v = vi -> second;

        out << "r_" << i << " ";
        out<< dirn << v << "\n";
      }
    }

  public:
    void set_row_lower_bound (int row, double what)
    {
      _ASSERT_VALID_ROW(row);
      _lower_bounds_rows[row]= what;
    }

    void set_row_upper_bound(int row, double what)
    {
      _ASSERT_VALID_ROW(row);
      _upper_bounds_rows[row]= what;
    }

    void set_col_lower_bound(int col, double what)
    {
      _ASSERT_VALID_COL(col);
      _lower_bounds_cols[col]= what;
    }

    void set_col_upper_bound(int col, double what)
    {
      _ASSERT_VALID_COL(col);
      _upper_bounds_cols[col]= what;
    }

    void set_coefficient(int row, int col, double what)
    {
      _ASSERT_VALID_ROW(row);
      _ASSERT_VALID_COL(col);

      if (what == (double)0)
        return;

      Coordinate c(row,col);

      if (get_coefficient(row,col) == (double) 0) {
        _cm.insert(pair<Coordinate,double>(c, (double) what));
      }
      else
      {
        CoeffMap::iterator vi = _cm.find(c);
        vi->second = what;
      }

      return;
    }

    double get_coefficient(int row, int col)
    {
      _ASSERT_VALID_ROW(row);
      _ASSERT_VALID_COL(col);

      Coordinate c (row, col);

      CoeffMap::iterator vi = _cm.find(c);

      if (vi == _cm.end())
        return (double) 0;

      return vi->second;
    }

    /**
     * Add (what) to coefficient at (row,col)
     */
    void add_to_coefficient(int row, int col, double what)
    {
      _ASSERT_VALID_ROW(row);
      _ASSERT_VALID_COL(col);

      if (what == (double)0)
        return;

      Coordinate c(row,col);
      CoeffMap::iterator vi = _cm.find(c);
      if (vi == _cm.end())
      {
        _cm.insert( pair<Coordinate,double>(c, what));
      }
      else
      {
        vi->second += what;
      }

      return;
    }

    /**
     * Single Column Objective Function (a var)
     */
    void set_objective_var (int col, double what)
    {
      _ASSERT_VALID_COL (col);
      // rest previous obj
      _colobjs.clear();
      set_objective_col (col, what);
    }

    void set_objective_col (int col, double what)
    {
      _ASSERT_VALID_COL (col);

      _colobjs[col] = what;
    }

    /**
     * Load coefficient to the glpk library
     */
    void add_coefficients_to_library()
    {
      int sz = _cm.size();

      vector<int> rn (1+sz);
      vector<int> cl (1+sz);
      vector<double> a(1+sz);

      CoeffMap::iterator vi;
      int i;
      for (i = 1, vi = _cm.begin(); vi != _cm.end(); ++ vi, ++i )
      {
        Coordinate const & c = vi->first;

        rn[i] = c.first + 1;
        cl[i] = c.second + 1;
        a[i]= vi->second;
      }

      glp_load_matrix(_lp, sz, &rn[0], &cl[0], &a[0]);

      return;
    }

    void add_objective_coefficients( int i, map<int, double> const & m,
                                     void (* setfunction) ( prob_ptr, int, double) )
    {
      map<int,double>::const_iterator vi = m.find(i);
      if (vi == m.end())
        return;

      setfunction(_lp, 1+i, vi->second);
      return;
    }

    void set_obj_dir (bool max)
    {
      glp_set_obj_dir (_lp, (max)? GLP_MAX : GLP_MIN);
    }

    void load_problem ()
    {
      if (_m == 0 || _n == 0)
        return;

      int i;

      rebuild_bounds();
      add_coefficients_to_library();

      if (_maximize)
        glp_set_obj_dir (_lp, GLP_MAX);
      else
        glp_set_obj_dir (_lp, GLP_MIN);

      for (i=0; i<_n; ++i)
        add_objective_coefficients (i, _colobjs, glp_set_obj_coef);
    }

    void rebuild_objective_coefficients ()
    {
      int i;
      for (i=0; i<_n; ++i)
        add_objective_coefficients (i, _colobjs, glp_set_obj_coef);
    }

    void add_bounds (int i,
                     IntDoubleMap const & lower,
                     IntDoubleMap const & upper ,
                     void (* handlerfun) (prob_ptr , int, int, double, double)
      )
    {
      map<int,double>::const_iterator vu =  upper.find(i);
      map<int,double>::const_iterator vl =  lower.find(i);

      double ub = (vu == upper.end())? (double) 0 : vu->second;
      double lb = (vl == lower.end())? (double) 0 : vl->second;

      int typx = GLP_FR;

      if (vu != upper.end()) {

        if (vl != lower.end()){

          if (lb == ub)
            typx = GLP_FX;
          else
            typx = GLP_DB;

        } else { typx= GLP_UP;}

      } else {
        if (vl != lower.end())
          typx = GLP_LO;
        else
          typx = GLP_FR;
      }

      //
      // my index i is known to the library
      // as i+1
      //

      handlerfun (_lp, i+1, typx, lb, ub);
      return;
    }

    void rebuild_bounds_cols ()
    {
      int i;

      for (i=0; i < _n ; ++i )
      {
        add_bounds(i, _lower_bounds_cols, _upper_bounds_cols, glp_set_col_bnds);
      }
    }

    void rebuild_bounds_rows()
    {
      int i;
      for (i=0; i < _m ; ++i )
      {
        add_bounds(i, _lower_bounds_rows, _upper_bounds_rows, glp_set_row_bnds);
      }
    }

    void rebuild_bounds()
    {
      rebuild_bounds_rows();
      rebuild_bounds_cols();
    }

    int get_problem_status()
    {
      return glp_get_status(_lp);
    }

    bool optimal_exists ()
    {
      return glp_get_status (_lp) == GLP_OPT;
    }

    // Taken from Siram's Code
    double get_optimal_solution ()
    {
      assert ( optimal_exists() );
      return glp_get_obj_val (_lp);
    }


    bool solve(){
      // simplex
      glp_smcp parm;
      glp_init_smcp(&parm);
      parm.meth = GLP_PRIMAL;
      //parm.meth = GLP_DUALP;

      int s1 = glp_simplex(_lp, &parm);

      if (s1 != 0)
      {
        errs() <<"Simplex Solving terminated unsuccessfully."<< "\n";
        return false;
      }
      return true;
    }

    bool solve_exact ()
      {
        glp_smcp parm;
        glp_init_smcp (&parm);
        parm.meth = GLP_PRIMAL;

        int s1 = glp_exact(_lp, &parm);

        if (s1 != 0)
        {
          errs() <<"Simplex Solving terminated unsuccessfully."<< "\n";
          return false;
        }
        return true;
      }


    /*
       * The algorithm is as follows:
       *       1. split the optimal value into the integer and fractional parts
       *       2. check if the fractional part is of a specific form within some
       *          tolerance limits (arbitrarily set to (+,-) 10^-5
       *       3. If not, truncate the scaled optimal.
       *       4. Reduce results to the lowest terms
    pair<long,long> get_optimal_value_fractional(){


      long num=0,den=1;

      double Z = get_optimal_solution();

      // split the optimal value into integer and mantissa
      double intpart;
      double mantissa;

      mantissa=modf(Z,&intpart);

      // now check if the mantissa is close to zero

      if (WITHIN_LIMITS(mantissa, 0)){
        // the integer part is the result
        num= (long) intpart;
        den= 1;
        return pair<long,long>(num,den);
      }

      // make mantissa positive
      if (mantissa < 0) {
        mantissa = mantissa +1;
        intpart = intpart -1;
      }

      assert ( mantissa > 0 && mantissa < 1);


      if (WITHIN_LIMITS(mantissa, 1)){
        // the integer part is the result
        num= (long) intpart+1;
        den= 1;
      } else if (WITHIN_LIMITS(mantissa, 0)){
        // the integer part is the result
        num= (long) intpart;
        den= 1;
      } else if (WITHIN_LIMITS(mantissa, 0.33333333333)){
        num =  3 * (long) intpart + 1;
        den = 3;
      } else if (WITHIN_LIMITS(mantissa, 0.5)){
        num =  2 * (long) intpart + 1;
        den = 2;
      } else if (WITHIN_LIMITS(mantissa, 0.6666666666)){
        num =  3 * (long) intpart + 2;
        den = 3;
      } else if (WITHIN_LIMITS(mantissa, 0.25)){
        num =  4 * (long) intpart + 1;
        den = 4;
      } else if (WITHIN_LIMITS(mantissa, 0.75)){
        num =  4 * (long) intpart + 3;
        den = 4;
      } else if (WITHIN_LIMITS(mantissa, 0.1)){
        num =  10 * (long) intpart + 1;
        den = 10;
      } else if (WITHIN_LIMITS(mantissa, 0.2)){
        num =  5 * (long) intpart + 1;
        den = 5;
      } else if (WITHIN_LIMITS(mantissa, 0.4)){
        num =  5 * (long) intpart + 2;
        den = 5;
      } else if (WITHIN_LIMITS(mantissa, 0.6)){
        num =  5 * (long) intpart + 3;
        den = 5;
      }else if (WITHIN_LIMITS(mantissa, 0.8)){
        num =  5 * (long) intpart + 4;
        den = 5;
      } else if (WITHIN_LIMITS(mantissa, 0.83333333333)){
        num =  6* (long) intpart + 5;
        den = 6;
      }else {


        // or else we just treat it differently for maximization and minimizations
        errs() << " Message --- Encountered unrecognized mantissa : " << mantissa << "\n";
        if (_maximize){
          // we need to lower-bound the solution
          num= (long) intpart;
          den=1;
        } else{

          // or else we upperbound the solution
          num= (long) intpart + 1;
          den=1;
        }
      }
      assert ( den > 0 && " negative denominator can mess the whole process up ");

      return pair<long,long>(num,den);
    }
    */

    void print_objective(ostream & out) const
    {
      map<int,double>::const_iterator vi;

      for (vi=_colobjs.begin(); vi!=_colobjs.end(); ++vi)
      {
        int index = vi->first;
        double val = vi->second;

        out << val <<" * " ;
        out << "r_" << index;
        out <<" + ";
      }

      out << " 0 ";
    }

    void print_problem(ostream & out ) const
    {
      //
      // get the expression for each row
      //
      int * cols = new int[_n+1];
      double * vals = new double[_n+1];

      for (int i=0; i<_m;++i)
      {
        out << "r_" << i;
        out <<" = ";
        int len = glp_get_mat_row ( _lp, 1+i, cols, vals);

        for (int j=1;j<= len ; ++j){

          int c = cols[j]-1;
          double v = vals[j];

          out << v << " * ";
          out << "c_" << c;
          out << " + ";
        }
        out <<" 0 "<< "\n";
      }
      delete(vals);
      delete(cols);
    }

    void print( ostream & out) const
    {
      /*
       * Print the problem -- first print what needs to be maximized
       *
       */
      if (_maximize)
        out <<"Max ";
      else
        out <<"Min ";

      print_objective(out);

      out << "\n";

      /*
       * Print the main problem
       */
      print_problem(out);

      /*
       * print bounds
       */
      print_row_bounds(out, _upper_bounds_rows, string("<="));
      print_row_bounds(out, _lower_bounds_rows, string(">="));

      // done
    }
  };

}


#endif
