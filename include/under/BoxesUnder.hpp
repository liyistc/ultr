#ifndef __BOXES__UNDER__HPP_
#define __BOXES__UNDER__HPP_

#include "ufo/Asd/OpenCloseInterval.hpp"
#include <boost/optional.hpp>
#include "ufo/Smt/UfoZ3.hpp"

static cl::opt<double>
DecayRate ("decay-rate",
           cl::desc("The rate of decay"),
           cl::init(2.0));

static cl::opt<unsigned>
DecayInterval ("decay-intv",
               cl::desc("How many iterations to wait before decaying"),
               cl::init(5));

namespace ufo
{
  typedef std::map<Expr, OpenCloseInterval> Box;
  typedef std::vector<Box> Boxes;
  typedef std::map<Expr, mpq_class> Aspect;
  typedef std::map<std::string, mpq_class> Model;
  typedef std::pair<std::string, mpq_class> Assignment;
  typedef std::pair<Expr, std::pair<double, double> > DoublePairPair;
  

  /**
   * A Boxes domain used to under-approximate a LRA formula
   */
  class BoxesUnder
  {
  public:
    ExprFactory &efac;
    Boxes boxes; // under-approximation computed so far
    Box convex_hull; // convex hull image of boxes
    Box &over_approx; // over-approximation image of formula
    std::set<Expr> &dims; // dimensions
    std::map<Expr, ExprPair> &alpha_beta; // alpha and beta associated with var
    std::map<Expr, std::pair<double, double> > growth; // decaying growth rate
    unsigned decay_counter;
    Expr phi; // original formula (before approximation)

    BoxesUnder (ExprFactory &_e,
                Box &_over,
                std::set<Expr> &_d,
                std::map<Expr, ExprPair> &_ab,
                Expr _p) : 
      efac (_e),
      over_approx (_over),
      dims (_d),
      alpha_beta (_ab),
      decay_counter (1),
      phi (_p)
      {
        // Initialize growth rate
        forall (Expr v, dims)
        {
          convex_hull[v] = OpenCloseInterval();
          growth[v] = make_pair(0.0, 0.0);
        }
      }

    ~BoxesUnder () {}
    
    /**
     * Add a single box
     */
    void addBox (const Box &b)
      {
        boxes.push_back (b);

        return;
      }

    unsigned size ()
      {
        return boxes.size();
      }

    /**
     * Check Unboundedness for each dim of a given box,
     * return a new box that is an expansion of the
     * original one
     */
    void checkUnbound ()
      {
        for (vector<Box>::iterator b = boxes.begin();
             b!= boxes.end(); ++b)
        {
          Box tmp = map<Expr,OpenCloseInterval>(*b);
          forall (Expr d, dims)
          {
            tmp[d] = OpenCloseInterval (b->find(d)->second.first, 
                                        mpq_class(1,0),
                                        true, false);
            if (!z3n_is_sat(mk<AND>(gammaBox (tmp),
                                    mk<NEG>(phi))))
            {
              (*b)[d] = tmp[d];
              errs () << *d << " expanded: " << (*b)[d].toString() << "\n";
            }
            else
              tmp[d] = (*b)[d];
          }
        }
      }

    /**
     * Add a box to the domain
     * Merge with existing boxes if possible.
     */
    void addBox (const Aspect &ratio, const Model &model,
                 bool strict, mpq_class &size)
      {
        //printModel (model);
        // Construct a box from the given model
        Box b;
        forall (Expr v, dims)
        { 
          Expr alpha = alpha_beta[v].first;
          //errs () << "alpha: " << *alpha << " v: " << *v << "\n";
          //assert (model.count (alpha) > 0);
          string alpha_name = lexical_cast<string>(alpha.get());
         
          //if (ratio.find(v)->second == mpq_class(10000)) // ub is unbounded
          //  b[v] = OpenCloseInterval (model.find(alpha_name)->second,
          //                            mpq_class (20000), true, true);
          //else
          b[v] = OpenCloseInterval (model.find(alpha_name)->second, 
                                    model.find(alpha_name)->second 
                                    + size * ratio.find(v)->second,
                                    true, strict);
        }

        // Combine with existing boxes
        for (vector<Box>::iterator eb = boxes.begin();
             eb != boxes.end();
             ++eb)
        {
          optional<Box> mg = Merge (*eb, b);
          // replace mergable box with the merged box.
          if (mg) 
          {
            *eb = *mg;
            updateGrowth (*mg);
            return;
          }
        }

        addBox (b);
        updateGrowth (b);
      }

    void recursiveMerge ()
      {
        for (vector<Box>::iterator b1 = boxes.begin();
             b1 != boxes.end(); ++b1)
        {
          for (vector<Box>::iterator b2 = b1+1;
               b2 != boxes.end(); ++b2)
          {
            optional<Box> mg = Merge (*b1, *b2);
            if (mg) 
            {
              *b1 = *mg;
              b2 = boxes.erase (b2);
              //recursiveMerge();
              b1 = boxes.begin();
              break;
            }
          }
        }
      }

    /**
     * Return a dim that is suspected to be unbounded
     */
    Expr suspectUnbound (const unsigned thresh)
      {
        forall (Expr d, dims)
        {
          if (growth.find(d)->second.second > thresh &&
              isPosInf(over_approx.find(d)->second.second) &&
              !isPosInf(convex_hull.find(d)->second.second))
            return d;
        }
        return NULL;
      }

    /**
     * Compute convex hull of boxes and
     * compare with the previous one to
     * decide the growth of each dimension.
     */
    void updateGrowth (const Box &b)
      {
        forall (Expr d, dims)
        {
          // Decay the growth rate
          if (decay_counter % DecayInterval == 0)
            growth[d] = make_pair (growth[d].first / DecayRate,
                                   growth[d].second / DecayRate);

          assert (convex_hull.count (d) > 0);
          
          OpenCloseInterval it = convex_hull[d];
          double l = it.isBot() ? 0.0 : it.first.get_d();
          double r = it.isBot() ? 0.0 : it.second.get_d();
          //errs () << "l: " << l << "\n";
          //errs () << "r: " << r << "\n";
          convex_hull[d] = convex_hull[d].join (b.find(d)->second);
          
          // accumulated growth
          growth[d] = 
            make_pair (l - convex_hull[d].first.get_d() + growth[d].first,
                       convex_hull[d].second.get_d() - r + growth[d].second);
        }
        
        decay_counter++;
      }

    /**
     * Test if two boxes can be merged into one.
     * Criterion: all dimensions equal except for one, 
     * and adjacent on that dimension.
     * Return the merged box if they are mergable.
     */
    optional<Box> Merge (const Box &b1, const Box &b2)
      {
        Expr merge_point = NULL;
        bool subsume = false; // b1 subsumes b2

        // Test subsumption
        Expr gamma1 = gammaBox(b1);
        Expr gamma2 = gammaBox(b2);
        if (!z3n_is_sat (mk<AND>(gamma1, mk<NEG>(gamma2))))
          return optional<Box>(Box(b2));
        if (!z3n_is_sat (mk<AND>(gamma2, mk<NEG>(gamma1))))
          return optional<Box>(Box(b1));

        forall (Expr v, dims)
        {
          if (b1.find (v)->second == b2.find (v)->second)
            continue;


          if (merge_point)
            return optional<Box>(); // more than one point found
          
          merge_point = v; // set merge point
          if (!(b1.find(v)->second.
                isAdjacent (b2.find(v)->second)))
            return optional<Box>(); // failed
        }
        
        Box res = Box (b1);
        res[merge_point] = b1.find(merge_point)->
          second.join (b2.find(merge_point)->second);

        return optional<Box>(res);
      }
      
    /**
     * Return an Aspect ratio that
     * requires all dimensions equal
     */
    Aspect equalAspect ()
      {
        Aspect res;
        forall (Expr d, dims)
          res[d] = mpq_class(1);
        return res;
      }

    /**
     * Return an Aspect ratio that conforms to the given box
     */
    Aspect getAspect (const Box &box)
      {
        Aspect res;
        forall (Expr d, dims)
        {
          OpenCloseInterval it = box.find(d)->second;
          if (isPosInf (it.second)) return equalAspect();
          assert (!isPosInf (it.second));
          assert (!isNegInf (it.first));
          res[d] = it.second - it.first;
        }
        return res;
      }

    /**
     * Return a constraint that fixes the size
     * of the box
     */
    Expr fixSize (const Model &m, Expr l)
      {
        mpq_class size = m.find (lexical_cast<string>(l.get()))->second;
        return mk<EQ>(l, mkTerm(size, efac));
      }

    /**
     * Print a model
     */
    void printModel (const Model &m)
      {
        forall (Assignment a, m)
          errs () << a.first << " = " << a.second << "\n";
      }

    /**
     * Print Boxes
     */ 
    std::string toString ()
      {
        std::string res = "===== Boxes =====\n";
        forall (Box b, boxes)
        {
          res += printBox (b);
          res += "\n-----------------\n";
        }
        
        return res;
      }

    /**
     * Return a compact encoding of boxes for experiment
     */
    std::string encodeBoxes ()
      {
        std::string res = "";
        forall (Box b, boxes)
        {
          forall (Expr v, dims)
          {
            res += b.find(v)->second.first.get_str();
            res += " ";
            res += b.find(v)->second.second.get_str();
            res += " ";
          }
          res += "\n";
        }
        return res;
      }

    /**
     * Print a single Box
     */
    std::string printBox (const Box &b)
      {
        std::string res = "";
        forall (Expr v, dims)
        {
          res += lexical_cast<string>(v.get()) + " : ";
          res += b.find(v)->second.toString() + " ";
        }
        return res;
      }

    /**
     * Print Growth Rate
     */
    std::string printGrowth ()
      {
        std::string res = "===== Growth =====\n";
        forall (Expr d, dims)
        {
          res += lexical_cast<string>(d.get()) + " : ";
          res += "[-] " 
            + lexical_cast<string>(growth[d].first) 
            + ", [+] "
            + lexical_cast<string>(growth[d].second)
            + "\n";
        }
        res += "------------------\n";
        return res;
      }

    /**
     * Gamma function of boxes domain
     */
    Expr gamma ()
      {
        ExprVector g;
        forall (Box b, boxes)
        {
          g.push_back(gammaBox (b));
        }
        return mknary<OR>(mk<FALSE>(efac), g.begin(), g.end());
      }

    /**
     * Gamma function for a box
     */
    Expr gammaBox (const Box &b)
      {
        ExprVector g;
        forall (Expr v, dims)
        {
          assert (b.count (v) > 0);
          g.push_back (b.find (v)->second.gamma(v));
        }
        
        return mknary<AND>(mk<TRUE>(efac), g.begin(), g.end());
      }

    /**
     * Return gamma of the convex hull of boxes
     */
    Expr gammaConvexBoxes ()
      {
        
        return gammaBox (convex_hull);
      }

    /**
     * Return the fastest growing box
     */
    Box fastestGrowBox ()
      {
        Expr max;
        double max_growth = 0.0;
        forall (DoublePairPair g, growth)
        {
          if (g.second.second > max_growth)
          {
            max = g.first;
            max_growth = g.second.second;
          }
        }
        
        Box res;
        mpq_class max_bound = mpq_class (0);
        forall (Box b, boxes)
        {
          if (isPosInf(b.find (max)->second.second))
              continue;
          if (b.find (max)->second.second > max_bound)
          {
            res = b;
            max_bound = b.find (max)->second.second;
          }
        }
        
        return res;
      }

    /**
     * Replace boxes with their convex hull
     * This function has to be used carefully.
     * Only if the convex hull is also contained
     * inside the target formula
     */
    void joinBoxes ()
      {
        boxes.clear();
        boxes.push_back (convex_hull);
      }

    /**
     * Return a constraint that blocking the
     * current boxes area
     */
    Expr block ()
      {
        ExprVector g;
        forall (Box b, boxes)
        {
          ExprVector d;
          forall (Expr v, dims)
          {
            assert (b.count (v) > 0);
            assert (alpha_beta.count (v) > 0);
            Expr alpha = alpha_beta[v].first;
            Expr beta = alpha_beta[v].second;
            OpenCloseInterval it = b.find(v)->second;
            
            Expr left = it.lclose ? 
              mk<LT>(beta, mkTerm(it.first, efac)) : 
              mk<LEQ>(beta, mkTerm(it.first, efac));
            Expr right = it.rclose ?
              mk<GT>(alpha, mkTerm(it.second, efac)) : 
              mk<GEQ>(alpha, mkTerm(it.second, efac));
            
            // unbounded
            if (isPosInf(it.second))
              right = mk<FALSE>(efac);
            
            d.push_back (mk<OR>(left, right));
          }
          g.push_back (mknary<OR>(mk<FALSE>(efac), d.begin(), d.end()));
        }
        
        return mknary<AND>(mk<TRUE>(efac), g.begin(), g.end());
      }
  };
}

#endif
