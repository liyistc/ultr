Welcome to uLTR, an under-approximation technique for decomposing Local Time Requirements (LTR) of component-based systems.

## License

The license is in LICENSE.TXT.

## Related Files

Our implementation is built on top of a model checking framework [UFO](https://bitbucket.org/arieg/ufo/wiki/Home)
which provides easy and efficient access to many utilities, e.g., satisfiability checking, Box/Boxes analysis.
The executable is currently not available since the symbolic optimization procedure we used, OptMathSAT,
is not publicly accessible. We are working on porting to another optimization procedure
[Symba](https://bitbucket.org/arieg/ufo/src/popl14).
The relevant source files for uLTR in the repository are:

	tools/ultr/ultr.cpp           // main function of uLTR algorithm
    include/under/BoxesUnder.hpp  // Boxes domain helper functions

## Compilation Instructions

1. Compile LLVM using cmake

        mkdir build ; cd build
        cmake -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_INSTALL_PREFIX:PATH=/ag/llvm/run/2.9 -DLLVM_TARGETS_TO_BUILD:STRING=X86 /ag/llvm/src/2.9/
        make
        make install

2. Configure UFO and uLTR using

        mkdir build ; cd build
        cmake <PATH_TO_ROOT> -DCMAKE_BUILD_TYPE=Debug -DLLVM_ROOT=/home/liyi/pkg/llvm/llvm-2.9/build-dbg/ -DLLVM_CONFIG_EXECUTABLE=/home/liyi/pkg/llvm/llvm-2.9/obj/bin/llvm-config -DZ3_ROOT=/home/liyi/pkg/z3/z3-unstable/ -DMATHSAT_ROOT=/home/liyi/pkg/mathsat/mathsat-5.2.6-linux-x86_64/bin/  -DLDD_ROOT=/home/liyi/pkg/ldd/ldd-r6138/ -DSOPLEX_ROOT=/home/liyi/pkg/soplex/soplex-1.7.1/ -DGLPK_ROOT=/home/liyi/pkg/glpk/obj/

Everything after `Z3_ROOT` is optional. Other available options can be
seen with `cmake -LH` or by using one of cmake GUI tools `ccmake` or
`cmake-gui`.

The binary of uLTR should be found under the folder `build/tools/ultr`, though it will not work without the executable of
OptMathSAT (we are working on this).

Good luck!
