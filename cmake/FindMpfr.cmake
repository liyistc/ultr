find_package (Gmp QUIET)

set (MPFR_SEARCH_PATH "" CACHE PATH "Search path for mpfr.")
mark_as_advanced (MPFR_SEARCH_PATH)

find_path (MPFR_INCLUDE_DIR NAMES mpfr.h PATHS ${MPFR_SEARCH_PATH}/include)
find_library (MPFR_LIB NAMES mpfr PATHS ${MPFR_SEARCH_PATH}/lib)

mark_as_advanced (MPFR_INCLUDE_DIR MPFR_LIB)

include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (Mpfr
  REQUIRED_VARS MPFR_INCLUDE_DIR MPFR_LIB GMP_FOUND)
  