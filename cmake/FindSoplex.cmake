# Find Soplex

set(SOPLEX_ROOT "" CACHE PATH "Root of Soplex compiled source tree.")

find_package(ZLIB REQUIRED)

find_path(SOPLEX_INCLUDE_DIR NAMES soplex.h PATHS ${SOPLEX_ROOT}/src)
find_library(SOPLEX_LIBRARY NAMES soplex PATHS ${SOPLEX_ROOT}/lib)


set(SOPLEX_LIBRARY ${SOPLEX_LIBRARY} ${ZLIB_LIBRARIES})

include (FindPackageHandleStandardArgs)
find_package_handle_standard_args(SOPLEX
  REQUIRED_VARS SOPLEX_LIBRARY SOPLEX_INCLUDE_DIR ZLIB_LIBRARIES)

mark_as_advanced(SOPLEX_LIBRARY SOPLEX_INCLUDE_DIR ZLIB_LIBRARIES)
