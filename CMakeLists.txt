project(Ufo)
cmake_minimum_required(VERSION 2.8)

if (CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR ) 
  message (FATAL_ERROR
    "In-source builds are not allowed. Please clean your source tree and try again.")  
endif()

enable_testing()
include (CTest)

# Add path for custom modules
set(CMAKE_MODULE_PATH
  ${CMAKE_MODULE_PATH}
  "${CMAKE_CURRENT_SOURCE_DIR}/cmake"
  )


set (CUSTOM_BOOST_ROOT "" CACHE PATH "Path to custom boost installation.")
if (CUSTOM_BOOST_ROOT)
  set (BOOST_ROOT ${CUSTOM_BOOST_ROOT})
  set (Boost_NO_SYSTEM_PATHS "ON")
endif()

set (Boost_USE_STATIC_LIBS ON)
find_package (Boost 1.46.1 REQUIRED COMPONENTS system unit_test_framework)
IF (Boost_FOUND)
  include_directories (${Boost_INCLUDE_DIRS})
endif ()

find_package (LLVM REQUIRED)

set(LLVM_REQUIRES_RTTI TRUE)
set(LLVM_REQUIRES_EH TRUE)

# FileCheck is not installed by default, warn the user to install it FileCheck
if( NOT EXISTS ${LLVM_ROOT}/bin/FileCheck
    OR NOT EXISTS ${LLVM_ROOT}/bin/not)
  message(WARNING "`FileCheck' and `not' are requred to run some tests, "
                  "but they are not installed! Please copy them from "
                  "${LLVM_OBJ_ROOT}/bin to ${LLVM_ROOT}/bin.")
endif()



include_directories(${LLVM_INCLUDE_DIR})
link_directories(${LLVM_LIBRARY_DIR})


# We incorporate the CMake features provided by LLVM:
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${LLVM_ROOT}/share/llvm/cmake")
include(LLVMConfig)
include(LLVM)
include(AddLLVM)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${LLVM_CPPFLAGS}")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${LLVM_LDFLAGS}")

# check for rt lib. Not needed on OSX.
find_library(RT_LIB NAMES rt)
if (NOT RT_LIB)
  set(RT_LIB "")
endif()
mark_as_advanced(RT_LIB)

find_package(Gmp REQUIRED)
if (GMP_FOUND)
  include_directories (${GMP_INCLUDE_DIR})
  include_directories (${GMPXX_INCLUDE_DIR})
else()
  set(GMP_LIB "")
  set(GMPXX_LIB "")
endif()

find_package(OpenMP)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")

# put static libraries first
set(CMAKE_FIND_LIBRARY_SUFFIXES ".a" ${CMAKE_FIND_LIBRARY_SUFFIXES})

find_package(MathSAT 5.2.6)
if (MATHSAT_FOUND)
  include_directories(${MATHSAT_INCLUDE_DIR})
else()
  set(MATHSAT_LIBRARY "")
endif()

find_package(Z3 4.3.2 REQUIRED)
include_directories(${Z3_INCLUDE_DIR})

find_package(Abc)
if (ABC_FOUND)
  include_directories(${ABC_INCLUDE_DIR})
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ABC_CXXFLAGS}")
else()
  set(ABC_LIBRARY "")
endif ()


find_package(Ldd)
if (LDD_FOUND)
  include_directories (${LDD_INCLUDE_DIR})
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${LDD_CXXFLAGS}")
else()
  set(LDD_LIBRARY "")
endif()

find_package(Apron)
if (APRON_FOUND)
  include_directories (${APRON_INCLUDE_DIR})
else()
  set (APRON_LIBRARY "")
endif()


find_package(Soplex)
if (SOPLEX_FOUND)
  include_directories (${SOPLEX_INCLUDE_DIR})
else()
  set (SOPLEX_LIBRARY "")
endif()

find_package(Glpk)
if (GLPK_FOUND)
  include_directories (${GLPK_INCLUDE_DIR})
else ()
  set (GLPK_LIBRARY "")
endif()

set(UFO_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(UFO_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR})


include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}/include
  ${CMAKE_CURRENT_BINARY_DIR}/include
  )

install(DIRECTORY include/
  DESTINATION include
  FILES_MATCHING
  PATTERN "*.hpp"
  PATTERN ".svn" EXCLUDE
  )

install(DIRECTORY ${UFO_BINARY_DIR}/include/
  DESTINATION include
  FILES_MATCHING
  PATTERN "*.hpp"
  PATTERN "CMakeFiles" EXCLUDE
  PATTERN ".svn" EXCLUDE
  )


add_subdirectory(lib)
add_subdirectory(tools)
add_subdirectory(test)
add_subdirectory(units)

option (ENABLE_REGRESSIONS "Enable regressions in test target" OFF)
if (ENABLE_REGRESSIONS)
  add_subdirectory(regression)
endif()


configure_file( ${UFO_SOURCE_DIR}/include/ufo/Config/config.h.cmake
                ${UFO_BINARY_DIR}/include/ufo/Config/config.h )

