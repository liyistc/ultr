#ifndef __ZPDR_REFINER__HPP__
#define __ZPDR_REFINER__HPP__

/** 
 * Refiner based on GPDR algorithm of Z3.
 * 
 * XXX Started as a copy-paste of DagItpRefiner.hpp. Need to merge the
 * two.
 */

#include <list>

#include "ufo/ufo.hpp"

#include "ufo/Cpg/Lbe.hpp"
#include "ufo/Arg.hpp"

#include "ufo/EdgeComp.hpp"

#include "Refiner.hpp"
#include "ufo/Smt/ExprMSat.hpp"
#include "ufo/Smt/UfoZ3.hpp"

#include "ufo/ArgBgl.hpp"
#include "ufo/ArgCond.hpp"

#include "ufo/Smt/MUS.hpp"

#include "ufo/property_map.hpp"

#include "boost/graph/graph_traits.hpp"
#include "boost/graph/filtered_graph.hpp"
#include "boost/graph/topological_sort.hpp"
#include "boost/typeof/typeof.hpp"
#include "boost/scoped_ptr.hpp"
#include "ufo/ufo_graph.hpp"
#include "ufo/ufo_iterators.hpp"

#include "UfoLinerPrinter.hpp"

#include "ufo/CptLiveValues.hpp"  
#include "ZPdrDagInterpolator.hpp"

#include "exprVisit.hpp"
namespace ufo
{
  class ZPdrRefiner : public Refiner
  {
  private:

    boost::scoped_ptr<CptLiveValues> m_live;
    
  public:
    typedef std::map<const BasicBlock*, Expr>  BbExprMap;

    ZPdrRefiner (ExprFactory &fac, 
		   AbstractStateDomain &dom,
		   LBE &cpG,
		   DominatorTree &dt,
		   ExprMSat &msat,
		   ARG &a, bool doSimp, NodeExprMap& nodeLabels) :
      Refiner (fac, dom, cpG, dt, msat, a, doSimp, nodeLabels), 
      m_live (0) {}
    
    void refine ()
    {
      assert (entryN != NULL);
      assert (exitN != NULL);
      
      // -- compute live values
      m_live.reset (new CptLiveValues (efac, *(arg.getFunction ()), 
				       entryN->getLoc (),
				       exitN->getLoc (),
				       DT, lbe));
      m_live->run ();
      

      /** Counterexamples are only computed once. There is no
       * refinement after that 
       */
      assert (argCex.empty ());
      assert (cfgCex.empty ());
      

      labels.clear ();      
      labels [entryN] = mk<TRUE> (efac);
      labels [exitN] = mk<FALSE> (efac);
      
      // errs () << "ZPdrRefiner: refine path to "
      // 	      << exitN->getLoc ()->getBB()->getNameStr() << " ...\n";
      
      // errs () << "Initial vertices\n";
      // foreach (Node *v, vertices (arg))
      // 	errs () << "NID(" << v->getId () << ")\n";
      // errs () << "END Initial vertices\n";
      
      // -- compute the set of nodes that are between entryN and exitN
      NodeSet reachable;
      BOOST_AUTO(reachablePM, make_set_property_map (reachable));
      {
	BOOST_AUTO(nfeArg, 
		   make_filtered_graph (arg, NotFalseEdgePredicate<ARG> (arg)));
	slice (nfeArg, entryN, exitN, reachablePM);
      }
      
      
      // -- restrict the graph to nodes between entryN and exitN (but
      // -- keep false edges)
      // BOOST_AUTO(rArg, 
      // 		 make_filtered_graph 
      // 		 (arg, make_vertex_edge_predicate 
      // 		  (arg, reachablePM)));
      BOOST_AUTO(rArg, 
		 make_filtered_graph 
		 (arg, make_vertex_edge_predicate (arg, reachablePM), 
		  make_vertex_predicate (arg, reachablePM)));
      
      
      // errs () << "entry " << entryN->getId () 
      // 	      << "exit " << exitN->getId () << "\n";
      
      // -- compute topological order of the ARG
      NodeList topo;
      topological_sort (rArg, std::front_inserter (topo));

      // errs () << "topo-sort\n";
      // foreach (Node *n, topo) errs () << "NID(" << n->getId () << ")\n";

      // -- compute the arg condition (with assumptions)
      ArgCond< BOOST_TYPEOF(rArg) > aCondComp (efac, lbe, DT, true);
      std::map<Node*,Environment> envMap;
      std::map<NodePair,Expr> argCond;
      std::map<NodePair,Environment> edgEnvMap;
      BOOST_AUTO (envMapPM, make_assoc_property_map (envMap));
      BOOST_AUTO (argCondPM, make_assoc_property_map (argCond));
      BOOST_AUTO (edgEnvPM, make_assoc_property_map (edgEnvMap));
      aCondComp.argCond (rArg, topo, envMapPM, argCondPM, edgEnvPM);

      NodeExprMap labelMap;

      {
	ExprVector vc (distance (topo));
	encodeVC (efac, rArg, topo, argCondPM, vc.begin ());


	if (consRefine)
	  {
	    // -- add lemmas from nodeLabels
	    typedef boost::tuple<Node*, Expr&> VE;
	    foreach (VE ve, 
		     make_pair
		     (mk_zip_it (++begin (topo), ++begin (vc)),
		      mk_zip_it (--end (topo), --end (vc))))
	      {
		Node *v = ve.get<0> ();
		Expr vTerm = mkTerm (v, efac);
	    
		// -- evaluate the label in the environment
		Expr nodeLabel = envMap [v].eval (nodeLabels [v]);
		// -- add assumptions
		addAssumptions addV;
		nodeLabel = dagVisit (addV, boolop::nnf (nodeLabel));
		// -- add assumption literal for the whole label
		nodeLabel = boolop::limp (mk<ASM> (vTerm), nodeLabel);
	    
		// -- store the label 
		labelMap[v] =  nodeLabel;

		// -- add the label to the VC
		Expr &e = ve.get<1> ();
		e = boolop::land (boolop::limp (vTerm, nodeLabel), e);

	      }
	  }	

	// -- compute MUS
	ExprSet usedAssumes;
	UfoZ3 z3Ctx (efac);
	ZSolver<UfoZ3> z3 (z3Ctx);

	Stats::resume ("refiner.mus");
	tribool res = mus_basic (z3, 
				 mknary<AND> (mk<TRUE> (efac), 
					      vc.begin (), vc.end ()),
				 std::inserter (usedAssumes, 
						usedAssumes.begin ()), 
				 3);
	Stats::stop ("refiner.mus");
	
	// -- SAT
	if (res || res == indeterminate)
	  {
	    labels.clear ();
	    // XXX Produce CEX here
	    return;
	  }

	Stats::resume ("refiner.kill.assumptions");
	// -- simplify ArgCond by keeping only used assumptions
	typedef std::map<NodePair,Expr>::value_type KV;
	foreach (KV kv, argCond) 
	  {
	    KillAssumptions ka (usedAssumes);
	    argCond [kv.first] = replaceSimplify (kv.second, mk_fn_map (ka));
	  }

	if (consRefine)
	  {
	    typedef NodeExprMap::value_type VT;
	    foreach (VT kv, labelMap)
	      {
		KillAssumptions ka (usedAssumes);
		labelMap [kv.first] = replaceSimplify (kv.second, 
						       mk_fn_map (ka));
	      }
	  }
	Stats::stop ("refiner.kill.assumptions");
      }
      
      
      
			 
      Stats::resume ("refiner.zpdr.total");
      Stats::resume ("refiner.zpdr");

      
      // // -- compute DAG Interpolant
      ZPdrDagInterpolator< BOOST_TYPEOF(rArg)> dagItpComp (efac, *m_live);
      
      BOOST_AUTO(labelsPM, make_assoc_property_map (labels));
      
      tribool res;
      if (consRefine)
      	{
      	  BOOST_AUTO (labelMapPM, make_assoc_property_map (labelMap));
      	  res = dagItpComp.dagItp (rArg, topo, envMapPM, 
      				   argCondPM, labelMapPM, labelsPM);
      	}
      else
      	{
      	  static_property_map<Expr> smap(mk<TRUE> (efac));
      	  res = dagItpComp.dagItp (rArg, topo, envMapPM, 
      				   argCondPM, smap, labelsPM);
      	}      
	
      Stats::stop ("refiner.zpdr");

      if (res)
	{
	  // typedef std::pair<Node*,Expr> KV;
	  // errs () << "New labels\n";
	  // foreach (KV kv, labels)
	  //   errs () << kv.first->getId () << " " << *kv.second << "\n";
	  
	  assert (labels.size () == topo.size ());
	  

	  Stats::resume ("refiner.nnf");	  
	  // -- put labels into NNF (required by other passes)
	  for (NodeExprMap::iterator it = labels.begin (), end = labels.end ();
	       it != end; ++it)
	    it->second = boolop::gather (boolop::nnf (it->second));
	  Stats::stop ("refiner.nnf");	  	  

	  if (consRefine)
	    {
	      typedef NodeExprMap::value_type KV;
	      foreach (KV kv, labels)
		{
		  if (isLeaf (kv.first, rArg) || 
		      isRoot (kv.first, rArg)) continue;
		  
		  NodeExprMap::const_iterator it = nodeLabels.find (kv.first);
		  assert (it != nodeLabels.end ());
		  
		  labels [kv.first] = boolop::land (kv.second, it->second);
		}
	    }  

	  assert (isOpX<TRUE> (labels [entryN]));
	  assert (isOpX<FALSE> (labels [exitN]));
	}
      else labels.clear ();

      Stats::stop ("refiner.zpdr.total");
    }

  };    
}




#endif
