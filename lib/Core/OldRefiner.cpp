#include "OldRefiner.hpp"
#include "exprVisit.hpp"

#include "ufo/Smt/MUS.hpp"
#include "ufo/Smt/ExprZ3.hpp"
#include "ufo/Smt/ForallSimplify.hpp"

namespace ufo
{
  /**  populates reach with nodes that can get to node n */
  void OldRefiner::computeCanReach (Node* n, NodeSet& out)
  {
    if (out.count (n) > 0) return;
      
    out.insert(n);
    foreach (Node* p, n->getParents ()) 
      // -- skip false edges
      if (!arg.isFalseEdge (p, n))
	//if (p->getVisit () == n->getVisit ()) 
	  computeCanReach (p, out);
  }

  void OldRefiner::computeReachFrom (Node *n, NodeSet &filter, NodeSet &out)
  {
    // -- skip if been here
    if (out.count (n) > 0) return;
    // -- skip if filtered out
    if (filter.count (n) <= 0) return;
    
    // -- new node, insert into out
    out.insert (n);
    
    // -- process children
    foreach (Node *kid, n->getChildren ())
      // -- avoid nodes reachable by false edges
      if (!arg.isFalseEdge (n, kid))
	//if (kid->getVisit () == n->getVisit ()) 
	  computeReachFrom (kid, filter, out);
  }
  
  void OldRefiner::computeReachable (Node *entry, Node *exit, NodeSet &out)
  {
    //computeCanReach (exit, out);
    // -- first compute all nodes that reach exit
    NodeSet reachExit;
    computeCanReach (exit, reachExit);
 
    // -- reduce by computing all nodes in reachExit that are
    // -- reachable from entry
    computeReachFrom (entry, reachExit, out);

    // assert (reachExit.size () == out.size () && 
    // 	    " must be true if falseEdges are not skipped");
  }
  



    /** Computes Edge Expression 
     ** XXX What does this mean?
     */
  void OldRefiner::computeEdgeExpr (Node* node, IntPairEnvMap &envMap,
				 std::map<IntPair, Expr> &exprMap,
				 std::map<int, Environment> &inScope,
				 std::set<Node*> &orderedNodes,
				 NodeExprMap& node2lbl,
				 bool addAssum,
				 std::map<Node*, Expr>& resolvers)
  {
    // -- We need children to get expr..
    assert (!node->getChildren ().empty ());


    resolvers[node] = mk<TRUE>(efac);

    // -- environments of the parents
    std::vector<Environment*> pEnvs;
      
    // -- merge parents environments
    foreach (Node* p, node->getParents ()){
      if (orderedNodes.count (p) == 0) continue;
      //if (arg.isFalseEdge (p, node)) continue;
      pEnvs.push_back 
	(&envMap[IntPair(p->getId (), node->getId ())]);
    }


    std::vector<Environment*>::iterator pEnvsIt = pEnvs.begin ();
    // -- create an environment based on the first parent,
    // -- or a new one if this is the entry node
    Environment mergedEnv;
    if (pEnvsIt == pEnvs.end ())
      mergedEnv = Environment (efac);
    else
      mergedEnv = Environment (*(*(pEnvsIt++)));
      
    // -- bindings that are refreshed by unify
    ExprSet refreshed;
    mergedEnv.unify (pEnvsIt, pEnvs.end (), refreshed);
      
    //errs() << "before clean for node: " << name (node) << "\n";
    //mergedEnv->printEnv ();
    // -- remove bad bindings from the merged environment
    cleanMerge (mergedEnv, node);

    // -- cleanup refreshed set
    ExprSet::iterator refIt = refreshed.begin ();
    while (refIt != refreshed.end ()){
      Expr key = *refIt;
      if (mergedEnv.isBound (key)) ++refIt;
      else refreshed.erase (refIt++);
    }
      

    //errs() << "\n\nafter clean\n";
    //mergedEnv->printEnv ();


    // -- Stores an environment with inscope variables
    inScope[node->getId ()] = mergedEnv;
      
    // -- update all incoming edges.
    foreach (Node* p, node->getParents ()){
      //errs() << " Updating incoming edge\n";
      if (orderedNodes.count (p) == 0) continue;
      if (arg.isFalseEdge (p, node)) continue;

      // -- expression of incoming edge
      Expr edge = exprMap[IntPair(p->getId (), node->getId ())];
	      
      // -- environment of an edge
      Environment& edgeEnv = envMap[IntPair(p->getId (), node->getId ())];
           
      ExprVector resolved;
	  
      // -- go over different variables between current env and env of 
	  
      // -- incoming edge and resolve  
      foreach (Expr key, refreshed){
	Expr temp = key;
	if (isOpX<VARIANT>(temp)) temp = variant::mainVariant(temp);
	if (isOpX<VARIANT>(temp)) temp = variant::mainVariant(temp);
          
	if (isOpX<BB>(temp)){
	  resolved.push_back 
	    (mk<IFF>(edgeEnv.lookup(key), mergedEnv.lookup (key)));
	  continue;
	}
	assert(isOpX<VALUE>(temp));
	  
	const Value* c = 
	  dynamic_cast<const VALUE&> (temp->op ()).get ();
	  
	Expr phi;
	if (isBoolType(c->getType()))
	  phi = mk<IFF>(edgeEnv.lookup(key), mergedEnv.lookup (key));
	else
	  {
	    //phi = mk<EQ>(edgeEnv.lookup(key), mergedEnv.lookup (key));
	    // -- stating x <= y && x >= y  seems to trick mathsat into 
	    // -- not propagating around the equality x=y. 
	    phi = 
	      mk<AND> (mk<LEQ> (edgeEnv.lookup(key), mergedEnv.lookup (key)),
	    	       mk<GEQ> (edgeEnv.lookup(key), mergedEnv.lookup (key)));
	    
	    // XXX This was an attempt to add assumptions so that XXX
	    // equalities do not flow to mathsat. But, this didn't
	    // work on the one example I've tried. Maybe there is a
	    // bug in this.

	    /* XXX Uncommenting this leads to very quick termination
	       with a claim that a CEX is found. Need to find out what
	       is going on. Maybe these assumptions are not collected
	       properly? Can assumptions be nested?*/
	    // Expr x = mk<LEQ> (edgeEnv.lookup(key),
	    // 		      mergedEnv.lookup (key)); 
	    // if (addAssum) x = mk<IMPL> (mk<ASM> (x), x);

	    // phi = x;

	    // x = mk<GEQ> (edgeEnv.lookup(key), mergedEnv.lookup (key));
	    // if (addAssum)  x = mk<IMPL> (mk<ASM> (x), x);

	    // phi = mk<AND> (phi, x);
	  }
	
	
	if (addAssum)
	  resolved.push_back (mk<IMPL> (mk<ASM> (phi), phi));
	else
	  resolved.push_back (phi);
      }

      if (resolved.size () == 1)
	edge = boolop::land (edge, resolved [0]);
      else if (resolved.size () > 1)
	edge = boolop::land 
	  (edge, mknary<AND> (resolved.begin (), resolved.end ()));
      
      exprMap[IntPair(p->getId (), node->getId ())] = edge;
    }
     
    Environment mergedEnvCopy(mergedEnv);


    //Expr nLabel = node->getLabel ();
    //if (!nLabel) nLabel = adom.gamma (node->getLoc (), node->getState ());
    SEdgeCondComp::updateEnvToFirstInst(mergedEnvCopy, *(node->getLoc()));
    node2lbl[node] = mergedEnvCopy.eval(nodeLabels[node]);
    
    // -- second compute all outgoing edges.
    foreach (Node* c, node->getChildren ()){
      if (orderedNodes.count (c) == 0) continue;
	      
      envMap[IntPair(node->getId (), c->getId ())] = 
	Environment (mergedEnv);
	  
      Environment &cEnv = envMap[IntPair(node->getId (), c->getId ())];
            
      SEdgePtr edge = *(node->getLoc ()->findSEdge (*c->getLoc ()));


      SEdgeCondComp x(efac, cEnv, edge, lbe, addAssum);
	
      exprMap[IntPair(node->getId (), c->getId ())] = mk<FALSE>(efac);
	
      // -- compute to update the environment. needed even the the
      // -- value is not used.
      x.compute();
        
      // -- skip false edges
      if (!arg.isFalseEdge (node, c))
	exprMap[IntPair(node->getId (), c->getId ())] = x.getCond();
    }
  }

  /** */
  Expr OldRefiner::computeNodeExpr (Node* node, 
				 std::map<IntPair, Expr>& exprMap,
				 std::set<Node*>& orderedNodes,
				 std::map<Node*,Expr>& node2lbl)
  {
          
    Expr nodeE = mkTerm (node, efac);
    ExprVector subRes;

    // -- variables for edges
    ExprVector evars;
    foreach (Node* c, node->getChildren ())
      {
	if (orderedNodes.count (c) == 0) continue;
	if (arg.isFalseEdge (node, c)) continue;

	Expr edge = exprMap[make_pair (node->getId (), c->getId ())];

	Expr cE = mkTerm(c, efac);
	edge = boolop::land (cE, edge);

	Expr bv = bind::boolVar (mk<TUPLE>(nodeE, cE));
	evars.push_back (bv);
	edge = boolop::land (edge, bv);

	subRes.push_back (edge);
      }
    
    Expr mex = mk<IMPL> (nodeE, exactlyOne (evars, efac));

    Expr subResExpr = 
      mknary<OR>(mk<FALSE>(efac), subRes.begin (), subRes.end ());


    if (node->getParents ().empty()) return subResExpr;

    Expr res = boolop::limp (nodeE, subResExpr);
    if (consRefine)
      {
	addAssumptions addVisit;
	Expr nodeLabel = dagVisit(addVisit, boolop::nnf (node2lbl[node]));
	Expr nlbl = mk<IMPL>(nodeE, mk<IMPL>(mk<ASM>(nodeE), nodeLabel));
	//Expr nlbl = mk<IMPL> (nodeE, nodeLabel);
	res = boolop::land (res, nlbl);
      }

    // errs () << "node expr for " << node->getNameStr () << "\n";
    // errs () << *boolop::pp(res) << "\n";
    
    return boolop::land(res, mex);
  }        

  void OldRefiner::cleanMerge (Environment& env, Node *node)
  {
    const BasicBlock* nodeBB = node->getLoc ()->getBB ();

    Environment::iterator it = env.begin ();      
    while (it != env.end ()){
      Expr key = it->first;
	      
      if (isOpX<BB> (key)) { env.erase (it++); continue; }
      else if (isOpX<VARIANT>(key)){
	Expr mv = variant::mainVariant (key);
	assert (isOpX<VALUE>(mv));
	      
	const Value* v = getTerm<const Value*> (mv);
	assert(v != NULL);
	      
	const Instruction* inst = dyn_cast<const Instruction>(v);
	assert(inst != NULL);
	      
	assert (isa<PHINode> (inst));
	// -- only care about PhiNodes that are defined in the node
	if (inst->getParent () != nodeBB){ 
	  env.erase (it++); 
	  continue;
	}
      }

      else if (isOpX<VALUE>(key)){
	const Value* v = getTerm<const Value*> (key);
	assert(v != NULL);
            
	const Instruction* inst = dyn_cast<const Instruction>(v);
	assert(inst != NULL);

	// -- anything that is defined in the basic block is
	// -- defined before used, so we don't need it from
	// -- other environments.
	if (inst->getParent () == nodeBB) 
	  {
	    env.erase (it++);
	    continue;
	  }
	// -- does not dominate the basic block, must be out of scope
	else if (!DT.dominates (inst->getParent (), nodeBB))
	  {
	    env.erase (it++);
	    continue;
	  }
	else{
	  // -- keep any value that is defined and used in different
	  // -- blocks except if the only different-block-use of the
	  // -- value is in a PHI assignment of nodeBB. 
	  bool keep = false;
		  
	  for (Value::const_use_iterator ut = inst->use_begin(), 
		 ue = inst->use_end (); ut != ue; ++ut)
	    {
	      const User* u = *ut;
	      const BasicBlock* ubb = 
		cast<Instruction>(u)->getParent ();
		      
	      if (ubb != inst->getParent())
		{
		  // -- found a use in a different block
		  // -- check if it is us
		  if (ubb == nodeBB)
		    {
		      // -- use in the node BB. check if it is a phi use
		      if (isa<PHINode> (u)) 
			{
			  //errs () << "skipping PHI use of " << *inst << "\n";
			  continue;
			}
		      
		    }
		  
		  keep = true; 
		  break;
		}
	    }		  
		  
	  if (!keep) 
	    { 
	      env.erase (it++);
	      continue;
	    }
	}
      }
      // -- didn't erase the iterator, do pre-increment
      ++it;
    }
  }

  
  namespace
  {
    /** Sets NODE expressions to FALSE. Used by OldRefiner::mkInScope */
    struct NodesToFalse 
    {
      Expr operator() (Expr e) const
      {
	if (isOpX<NODE> (e)) return mk<FALSE> (e->efac ());
	return Expr (0);
      }
    };

    /** A visitor to find all variables out-of-scope of a given
	environment. Used by OldRefiner::mkInScope */
    struct CollectOutScope 
    {
      /** the environment */
      const Environment &env;
      /* place to store the result */
      ExprSet res;

      CollectOutScope (const Environment &e) : env(e) {}

      VisitAction operator() (Expr exp)
      {	
	if (isOpX<VARIANT>(exp))
	  {
	    if (res.count (exp) > 0) return VisitAction::skipKids ();

	    Expr mv = variant::mainVariant(exp);
	    if (env.has_binding (mv, exp)) return VisitAction::skipKids ();
	    
	    if (isOpX<VARIANT> (mv)) 
	      { 
		mv = variant::mainVariant(mv);
		if (env.has_binding (mv, exp)) 
		  return VisitAction::skipKids ();
	      }

	    // -- no prime and unprime binding. out of scope
	    res.insert (exp);
	    return VisitAction::skipKids ();
	  }

	return VisitAction::doKids();
      }
    };

    struct StripVariants 
    {
      Expr operator () (Expr e) const
      {
	Expr res;
	// -- strip a variant
	if (isOpX<VARIANT> (e)) 
	  res = variant::mainVariant (e);
	// -- double-variant
	if (res && isOpX<VARIANT> (res))
	  res = variant::mainVariant (res);
	  
	return res;
      }
	
    };

  }  

  Expr OldRefiner::mkInScope (Expr e, const Environment &env, Node* n)
  {
    Stats::resume ("refiner.mkInScope");

    Stats::resume ("refiner.mkInScope.elim_and_simplify");

    Stats::resume ("refiner.mkInScope.elim_node");
    Expr e1 = replaceAllSimplify (e, mkTerm (n, efac), mk<TRUE> (efac));
    Stats::stop ("refiner.mkInScope.elim_node");

    Stats::resume ("refiner.mkInScope.elim_other");
    NodesToFalse ntf;
    Expr e2 = replaceSimplify (e1, mk_fn_map (ntf));
    Stats::stop ("refiner.mkInScope.elim_other");
    
    Stats::resume ("refiner.mkInScope.propSimp");
    Expr e3 = doSimplify ? boolop::simplify (e2) : e2;


    e3 = z3_lite_simplify (e2);
    errs () << "ITP SIZE : " << dagSize (e2)
	    << " AFTER SIMP: " << dagSize (e3)
	    << "\n";

    Stats::stop ("refiner.mkInScope.propSimp");

    Stats::stop ("refiner.mkInScope.elim_and_simplify");
          
    Expr e4 = e3;

    Stats::resume ("refiner.mkInScope.out_of_scope");
    CollectOutScope outScope = CollectOutScope (env);
    dagVisit (outScope, e4);
    Stats::stop ("refiner.mkInScope.out_of_scope");
    ExprSet &vars = outScope.res;

    Expr e5 = e4;
#ifndef ARIE_FORALL_DEBUG
    if (!vars.empty ())
      {
	ExprZ3 z3 (efac);
	Stats::resume ("refiner.mkInScope.z3.forall");
	//e5 = z3_forallElim (e5, vars);
	e5 = forallHeuristic (e5, vars);	
	Stats::stop ("refiner.mkInScope.z3.forall");
	
      }
#else
    if (!vars.empty ())
      {
	//project (e5, vars);
	e5 = forallHeuristic (e5, vars);
	errs () << "Eliminating: " << vars.size () << " vars for " 
		<< n->getNameStr () << "\n";
	foreach (ExprPair kv, env)
	  errs () << "INSCOPE: " << *kv.first << " -> " 
		  << *kv.second << "\n";
	
	e5 = boolop::simplify (boolop::nnf (e5));
	Stats::resume ("refiner.mkInScope.z3.forall");
	foreach (Expr var, vars)
	  {
	    errs () << "\tEVAR: " << *var << "\n";

	    ExprSet terms;
	    TermFinder tf (var);
	    filter (e5, tf, terms);
	    foreach (Expr term, terms)
	      errs () << "\tTERM: " << *term << "\n";

	    if (dagSize (e5) < 100)
	      (errs () << " e5 is \n"
	       << *nnf(e5) << "\n\n").flush ();

	    errs ().flush ();
	    ExprSet vv;
	    vv.insert (var);
	    errs () << "\tsz before elim " << dagSize (e5);
	    e5 = z3_forall_elim (e5, vv);
	    errs () << " sz after elim " << dagSize (e5);
	    e5 = z3_simplify (e5);
	    (errs () << " sz after simplify " << dagSize (e5) << "\n").flush ();

	  }
	Stats::stop ("refiner.mkInScope.z3.forall");
      }
    else
      {
	errs () << "NODE: " << n->getNameStr () << "\n";
	foreach (ExprPair kv, env)
	  errs () << "\tINSCOPE: " << *kv.first << " -> " 
		  << *kv.second << "\n";
      }
#endif
      
    Stats::resume ("refiner.mkInScope.variants");      
    StripVariants sv;
    Expr e6 = replace (e5, mk_fn_map (sv));
    Stats::stop ("refiner.mkInScope.variants");      
      
    Stats::stop ("refiner.mkInScope");
      
    return e6;
  }    
  

  void OldRefiner::refine ()
  {
    assert (entryN != NULL);
    assert (exitN != NULL);
      
    errs () << "Refiner: refine path to "
	    << exitN->getLoc ()->getBB()->getNameStr() << " ...\n";
    


    // -- compute nodes that can reach exitN
    NodeSet reachable;      
    computeReachable (entryN, exitN, reachable);
            
    // -- compute topological order of the explored graph
    NodeVector orderedNodes;      
    getNodeTopologicalOrdering(exitN, entryN, reachable,
			       std::back_inserter (orderedNodes));
    // -- remove exitN location from the list of nodes
    orderedNodes.pop_back();
      
    
    // -- edge to environment
    std::map<IntPair, Environment> envMap;
    // -- edge to edge expression
    std::map<IntPair, Expr> exprMap;
    // -- node to environment
    std::map<int,Environment> inScope;

    // -- node to lbl
    std::map<Node*, Expr> node2lbl;
    node2lbl[exitN] = mk<TRUE>(efac);
      
    std::map<Node*, Expr> resolvers;
    //double exprTimeA = getUsageTime ();      
    foreach (Node *curr, orderedNodes)
      computeEdgeExpr (curr, envMap, exprMap, inScope, reachable, 
		       node2lbl, false, resolvers);

    ExprVector pathExpr;
    foreach (Node* curr, orderedNodes)
      pathExpr.push_back (computeNodeExpr (curr, exprMap, reachable, node2lbl));
    

    errs() << "INTERPOLATING...\n";      
    
    // reset to interpolate
    interp.restart (true);
    interp.reset (true);
    
    Stats::resume ("refiner.interps");
    // -- state interpolants
    std::vector<Expr> interps;
    boost::tribool res =
      interp.interpolate (pathExpr.begin (), pathExpr.end (), 
			  std::back_inserter (interps));
    Stats::stop ("refiner.interps");
      
    errs() << "DONE INTERPOLATING: # interps =  "
	   << interps.size() << "\n";
                
    if (res != true) //no interpolants, then exitN found
      {
	errs () << "Refiner: found path to exit\n";
	return ;
      }

    // -- Have interpolants

    errs () << "Refiner: Mining labels from interpolants\n";

    unsigned int i = 0;
    for(NodeVector::iterator it = orderedNodes.begin(); 
	it != orderedNodes.end() ; ++it){
      // -- skip the entryN
      if (it == orderedNodes.begin()) continue;
	
      assert (i < interps.size());
	
      Node *curr = *it;

      errs () << "Refiner: mkInScope ...\n";
      Expr inscope = mkInScope(interps [i], inScope[curr->getId ()], curr);
      errs () << "Refiner: mkInScope: DONE\n";

      Expr cleanLabel;
      if (doSimplify){
        errs () << "Refiner: simplify ...\n";
        Stats::resume ("refiner.simplify");
        cleanLabel = z3_simplify (inscope);
	Stats::stop ("refiner.simplify");
	errs () << "Refiner: simplify: DONE\n";
      }
      else
        cleanLabel =  (inscope);
	
      Stats::resume ("refiner.nnf");
      // -- cleans label and puts it in NNF
      Expr finalLabel = exprToNNF (cleanLabel);
      Stats::stop ("refiner.nnf");
      
      // errs () << "Interp simp: " << i 
      // 	      << " for " << curr->getNameStr () 
      // 	      << "\n" 
      // 	      << *finalLabel << "\n\n"
      // 	      << " from \n"
      // 	      << *interps [i] << "\n";
	
      labels [curr] = finalLabel;

      i++;
    }

    // -- need to set all unlabeled nodes to false. 
    // -- by assumption, only exitN is unlabeled
    labels [exitN] = mk<FALSE>(efac);
    errs () << "Refiner: refine: DONE " << "\n";
    errs ().flush ();
  }
  
  void AsmRefiner::refine ()
  {
    assert (entryN != NULL);
    assert (exitN != NULL);
      
    errs () << "Refiner: refine path to "
	    << exitN->getLoc ()->getBB()->getNameStr() << " ...\n";
    

    Stats::resume ("refiner.pre");

    // -- compute nodes that can reach exitN
    NodeSet reachable;      
    computeReachable (entryN, exitN, reachable);

    assert (reachable.count (entryN) > 0 && 
	    "Entry can't reach exit, stop");
    assert (reachable.count (exitN) > 0 && 
	    "Exit not reachable from entry, stop");
            
    // -- compute topological order of the explored graph
    NodeVector orderedNodes;      
    getNodeTopologicalOrdering(exitN, entryN, reachable,
			       std::back_inserter (orderedNodes));

    assert (orderedNodes.back ()->getChildren ().empty () 
	    && " NOT AN ERROR NODE ");
    
    // -- remove exitN location from the list of nodes
    orderedNodes.pop_back();  
    
    // -- edge to environment
    std::map<IntPair, Environment> envMap;
    // -- edge to edge expression
    std::map<IntPair, Expr> exprMap;
    // -- node to environment
    std::map<int,Environment> inScope;
    
    std::vector<Expr> pathSkeleton;
    std::vector<Expr> assumptions;
      
    // -- node to lbl
    std::map<Node*, Expr> node2lbl;
    node2lbl[exitN] = mk<TRUE>(efac);
      
      
    std::map<Node*, Expr> resolvers;
    
    //double exprTimeA = getUsageTime ();      
    foreach (Node *curr, orderedNodes)
      computeEdgeExpr (curr, envMap, exprMap, inScope,
		       reachable, node2lbl, true, resolvers);
  
    

    ExprVector pathExpr_;
    ExprVector pathExpr;

    foreach (Node* curr, orderedNodes){
      Expr nodeExpr = computeNodeExpr (curr, exprMap, reachable, node2lbl);
      pathExpr_.push_back(nodeExpr);
    }
    
    foreach(Expr n, pathExpr_) pathExpr.push_back(n);

    foreach (Expr e, pathExpr){
      collectAssumptions collector(assumptions);
      dagVisit(collector, e);
    }

    Stats::stop ("refiner.pre");
    

    Stats::resume ("refiner.assumptions.total");
 
    /****** check with assumptions *****/
    Expr dagcond = boolop::land(pathExpr);

    ExprSet trueAssumptions;
    
    boost::tribool resAssumptions;
    {
      Stats::resume("refiner.assumptions");
      resAssumptions = 
	z3_is_sat_assuming (dagcond, assumptions, trueAssumptions);
      Stats::stop("refiner.assumptions");
 
      errs () << "Number of true assumptions: " << trueAssumptions.size()
	      << "\n";
    }

    ExprVector pathExprTemp (pathExpr.begin(), pathExpr.end());
    bool aggressiveSlicing = true;
    if (resAssumptions == false && aggressiveSlicing){
      
      int size = trueAssumptions.size();
      int sizep = -1;
      for (size_t i = 0; i < 3; ++i){
        if (sizep != -1)
	  if (size == sizep) break;
	
	pathExprTemp.clear();
        foreach (Expr n, pathExpr){
	  // replaceAssumptionsFalse r(trueAssumptions, 
	  // 			    mk<TRUE>(efac), mk<FALSE>(efac));
	  // Expr cleanExpr = dagVisit(r, n);
	  
	  // Simp simp(mk<TRUE>(efac), mk<FALSE>(efac), efac);
	  // Expr simpleCleanExpr = dagVisit(simp, cleanExpr);
	  // Expr simpleCleanExpr = boolop::simplify (cleanExpr);

	  Expr simpleCleanExpr = 
	    replaceSimplify (n, 
			     mk_fn_map(TurnAssumptionsFalse (trueAssumptions)));

	  pathExprTemp.push_back(simpleCleanExpr);
	}
        
        Expr dagcond = boolop::land(pathExprTemp);

	ExprVector assumptionsTemp(trueAssumptions.begin(), trueAssumptions.end());
	trueAssumptions.clear();
	
        Stats::resume("refiner.assumptions");
	resAssumptions = 
	  z3_is_sat_assuming (dagcond, assumptionsTemp, trueAssumptions);

        Stats::stop("refiner.assumptions");
	sizep = size;
	size = trueAssumptions.size();

	pathExpr.clear();
	foreach (Expr n, pathExprTemp) pathExpr.push_back(n);
        errs () << "Number of true assumptions: " << trueAssumptions.size()
		<< "\n";
      }
    }

    if (resAssumptions == true){
      errs () << "Error found (with Assumptions)\n";
      return;
    }

    ExprVector cleanPathExpr;
    
    if (resAssumptions == false){
      Stats::resume("asm_refiner.simplify.assumptions");
      errs () << "\n\n\n************************\n\n";
      errs () << "Number of assumptions: " << assumptions.size()
	      << "\n";
      int ucount= 0, ncount=0, tucount=0, tncount=0;
      for (size_t i = 0; i < assumptions.size(); ++i)
	if (isOpX<ULONG>(assumptions[i]->left()))
	  ucount++;
        else if (isOpX<NODE>(assumptions[i]->left())) 
	  ncount++;
      errs () << "\n\tNumber of inst assumptions: " << ucount;
      errs () << "\n\tNumber of node assumptions: " << ncount;
      ExprSet::iterator it;
      for (it = trueAssumptions.begin(); it != trueAssumptions.end(); ++it)
	if (isOpX<ULONG>((*it)->left()))
	  tucount++;
        else if (isOpX<NODE> ((*it)->left ())) tncount++;

      errs () << "\nNumber of TRUE assumptions: " << trueAssumptions.size();
      errs () << "\n\tNumber of true inst assumptions: " << tucount;
      errs () << "\n\tNumber of true node assumptions: " << tncount;
      errs () << "\n\n\n************************\n\n";

      foreach (Expr n, pathExprTemp){
	//std::cout << "\n\nbefore replace: " << n;

	replaceAssumptions r(trueAssumptions, mk<TRUE>(efac), mk<FALSE>(efac));
	Expr cleanExpr = dagVisit(r, n);
	
	//std::cout << "\n\nafter replace: " << cleanExpr;
        
	Simp simp(mk<TRUE>(efac), mk<FALSE>(efac), efac);
	Expr simpleCleanExpr = dagVisit(simp, cleanExpr);

	cleanPathExpr.push_back(cleanExpr);
      }

      Stats::stop("asm_refiner.simplify.assumptions");
    }

    Stats::stop ("refiner.assumptions.total");

    /***********************************/ 
    errs() << "INTERPOLATING...\n";      
    
    // reset to interpolate
    interp.restart (true);
    interp.reset (true);
    
    // -- state interpolants
    std::vector<Expr> interps;

    Stats::resume ("refiner.interps");
    boost::tribool res =
      interp.interpolate (cleanPathExpr.begin (), cleanPathExpr.end (), 
			  std::back_inserter (interps));
    Stats::stop ("refiner.interps");
    
    errs() << "DONE INTERPOLATING: # interps =  "
	   << interps.size() << "\n";
                
    if (res != true) //no interpolants, then exitN found
      {
	errs () << "Refiner: found path to exit\n";
	return ;
      }

    // -- Have interpolants

    errs () << "Refiner: Mining labels from interpolants\n";

    unsigned int i = 0;
    for(NodeVector::iterator it = orderedNodes.begin(); 
	it != orderedNodes.end() ; ++it){
      // -- skip the entryN
      if (it == orderedNodes.begin()) continue;
	
      assert (i < interps.size());
	
      Node *curr = *it;

      errs () << "Refiner: mkInScope ...\n";
      Expr inscope = mkInScope(interps [i], inScope[curr->getId ()], curr);
      errs () << "Refiner: mkInScope: DONE\n";

      Expr cleanLabel;
      if (doSimplify){
        errs () << "Refiner: simplify ...\n";
        Stats::resume ("asm_refiner.simplify");
        cleanLabel = z3_simplify (inscope);
	Stats::stop ("asm_refiner.simplify");
      }
      else
        cleanLabel =  (inscope);
 
      // -- cleans label and puts it in NNF
      Stats::resume ("refiner.nnf");
      //Expr finalLabel = exprToNNF (cleanLabel);
      // {
      // 	ExprZ3 z3 (efac);
      // 	cleanLabel = z3_lite_simplify (inscope);
      // 	errs () << "CLEAN LABEL: before " << dagSize (inscope) 
      // 		<< " after " << dagSize (cleanLabel) << "\n";
      // }
      
      Expr finalLabel = boolop::gather (boolop::nnf (cleanLabel));
      Stats::stop ("refiner.nnf");

      
      //errs () << "Interp simp: " << i << "\n" 
	//<< *finalLabel << "\n\n";
      //errs () << "Interp simp: " << *finalLabel << "\n\n";
      labels [curr] = finalLabel;
      
      if (consRefine)
	{
	  errs () << "Conjoining labels\n";
	  Expr nLabel = curr->getLabel ();
	  if (!nLabel) nLabel = adom.gamma (curr->getLoc (), curr->getState ());
	  
	  /* scope for Z3 */
	  // -- check whether finalLabel -> nLabel
	  // -- a -> b <-> ! (a & !b )
	  if (false /* DISABLED. NOT SURE IT HELPS */ && z3_is_sat
	      (boolop::land (finalLabel, boolop::lneg (nLabel))) == false)
	    {
	      //errs () << "HIT HIT HIT\n";
	      labels[curr] = finalLabel;
	    }
	  else
	    {
	      //errs () << "NOHIT NOHIT NOHIT\n";
	      labels[curr] = boolop::land(finalLabel, nLabel);
	    }
	}
      
	  
      i++;
    }
    
  
    // -- need to set all unlabeled nodes to false. 
    // -- by assumption, only exitN is unlabeled
    labels [exitN] = mk<FALSE>(efac);
    errs () << "Refiner: refine: DONE " << "\n";
    errs ().flush ();
  }  
}

