#include "ufo/InitializePasses.h"

#include "llvm/Analysis/Dominators.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Debug.h"
#include "llvm/Module.h"

#include <sys/time.h>
#include <sys/resource.h>

#include <list>

#include <boost/dynamic_bitset.hpp>
#include <boost/timer.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/bimap.hpp>
#include <boost/bimap/list_of.hpp>
#include <boost/bimap/set_of.hpp>
#include <boost/smart_ptr.hpp>


#include "ufo/ufo.hpp"


#include "ufo/Cpg/Topo.hpp"
#include "ufo/Cpg/Wto.hpp"
#include "ufo/Cpg/Lbe.hpp"

#include "ufo/Util/NameValues.hpp"

//#include "exprVisit.hpp"

#include "ufo/Smt/UfoZ3.hpp"
#include "ZPdrRefiner.hpp"
#include "DagItpRefiner.hpp"

#include "ufo/Smt/ExprMSat.hpp"
#include "ufo/Smt/ExprZ3.hpp"

#include "ufo/Arg.hpp"

#include "Refiner.hpp"
#include "OldRefiner.hpp"
#include "IncRefiner.hpp"

#include "ufo/Asd/PredicateAbstractDomain.hpp"
#include "ufo/Asd/Ldd/LddBoxAbstractStateDomain.hpp"
#include "ufo/Asd/Bnd/BoundAbstractStateDomain.hpp"

#include "CheckProof.hpp"

#include "ufo/Smt/SmartExprSet.hpp"

using namespace llvm;
using namespace ufo;

using namespace msat;
using namespace ufoz3;

namespace ufo
{


  double getUsageTime ()
  {
    struct rusage ru;
    getrusage (RUSAGE_SELF, &ru);
    return ru.ru_utime.tv_sec + (ru.ru_utime.tv_usec * 1.0 / 1000000);
  }


}
namespace ufo
{
  class ModelChecker
  {
  public:
    typedef std::pair<int,int> intPair;
    int numOfRef;
    int boolpost;
    int boolpostPred;
    int cartpost;
    int expandcalls;
    double interpTime;
    double coverTimeU;
    double exprTime;
    double postTime;
    double cleanTime;

    // -- formula manipulator

    // -- Large Block Encoding
    LBE& lbe;

    // -- Weak topo ordering
    WTOPass& wto;

    // -- Main function
    Function& mainF;

    // -- initial location
    CutPointPtr locInit;
    // -- error location
    CutPointPtr locError;


    DominatorTree &DT;

    ExprFactory &efac;    

    Expr trueE;
    Expr falseE;



    // SMT solver
    ExprMSat &ms;

    // -- abstract domains
    AbstractStateDomain &adom;

    // -- an AbsractReachabilityGraph
    ARG aArg;

    Refiner *refiner;

    // -- set of all cutpoints that can reach locError cut point
    std::set<CutPointPtr> reachSet;

    NodeExprMap nodeLabels;
    
    /** logically reduced set of expressions */
    typedef SmartExprSet<UfoCompSolver> ReducedExprSet;
    typedef std::map<CutPointPtr, ReducedExprSet*> InvarMap;
    InvarMap cp2inv;

  public:
    ModelChecker(ExprFactory &fac, ExprMSat &_ms, CutPointPtr mli, 
		 CutPointPtr mle, 
		 LBE& _lbe, WTOPass& _wto, Function& f, 
		 DominatorTree &dt, 
		 AbstractStateDomain &dom) :
      numOfRef(0),boolpost(0), boolpostPred(0), cartpost(0),
      expandcalls(0), interpTime(0.0), coverTimeU(0.0), 
      exprTime(0.0), postTime(0.0), cleanTime(0.0), 
      lbe(_lbe), wto(_wto),
      mainF(f),
      locInit(mli), 
      locError(mle), 
      DT(dt),
      efac (fac),
      trueE(mk<TRUE>(efac)),
      falseE(mk<FALSE>(efac)),
      ms(_ms), 
      adom (dom), 
      aArg (0, mainF),
      satCache (1024*4)
    {
      // refiner = new Refiner (&aArg, NULL, NULL, 
      // 			     efac, lbe, dom, ms, z3, ufocl::CONS_REFINE);

            switch (ufocl::INC_REFINE)
	{
	case REF0:
	  refiner = new OldRefiner (efac, adom, lbe, DT, ms, aArg, 
				    ufocl::UFO_SIMPLIFY, nodeLabels);
	  break;
	case REF1:
	  refiner = new IncRefiner (efac, adom, lbe, DT, ms, aArg, 
				    ufocl::UFO_SIMPLIFY, nodeLabels);
	  break;
	case REF2:
	  refiner = new IncRefiner2 (efac, adom, lbe, DT, ms, aArg, 
				     ufocl::UFO_SIMPLIFY, nodeLabels);
	  break;
	case REF3:
	  refiner = new IncRefiner2 (efac, adom, lbe, DT, ms, aArg, 
				     ufocl::UFO_SIMPLIFY, nodeLabels);
	  break;
	case REF4:
	  refiner = new AsmRefiner (efac, adom, lbe, DT, ms, aArg, 
				    ufocl::UFO_SIMPLIFY, nodeLabels);
	  break;
	case REF5:
	  refiner = new DagItpRefiner (efac, adom, lbe, DT, ms, aArg, 
				       ufocl::UFO_SIMPLIFY, nodeLabels);
	  break;
	case REF6:
	  refiner = new ZPdrRefiner (efac, adom, lbe, DT, ms, aArg, 
				     ufocl::UFO_SIMPLIFY, nodeLabels);
	  break;
	}
      
      refiner->setConsRefine (ufocl::CONS_REFINE);

      computeCanReach (locError, reachSet);
#ifdef DO_NOT_USE_REACH_SET
      // -- undoes the computeCanReach computation
      const CutPointVector &allCutPts = lbe.getFunctionCutPts (mainF);
      reachSet.insert (allCutPts.begin (), allCutPts.end ());
#endif
    }
    
  private:
    void computeCanReach (CutPointPtr cp, std::set<CutPointPtr> &reach)
    {
      if (reach.count (cp) > 0) return;
      
      reach.insert (cp);
      for (edgeIterator it = cp->predBegin (), end = cp->predEnd ();
	   it != end; ++it)
	computeCanReach ((*it)->getSrc (), reach);
    }
    
  public:

    ~ModelChecker () 
    { 
      errs () << "Destructing MC\n"; 
      delete refiner;
      // -- cleanup MathSat's cache
      ms.restart (false);
    }


    ExprCache<tribool> satCache;
    boost::tribool isSAT (Expr e)
    {
      ExprCache<tribool>::const_iterator it = satCache.find (e);
      if (it != satCache.end ()) return it->second;

      boost::timer t;
      boost::tribool res = z3n_is_sat (e);
      if (t.elapsed () < 1) return res;

      // -- insert in the cache
      satCache.insert (e, res);
      return res;
    }

    void printReport(int iter, boost::tribool res){
      errs() << "\n========REPORT========\n";
      if (res)
	errs() << "BRUNCH_STAT Result SAFE";
      else if (!res)
	errs() << "BRUNCH_STAT Result CEX";
      else 
	errs() << "BRUNCH_STAT Result UNKNOWN";

      errs() << "\nBRUNCH_STAT Iter " << iter; 
      errs() << "\nBRUNCH_STAT Refine " << numOfRef;
      errs() << "\nBRUNCH_STAT BoolPost " << boolpost;
      errs() << "\nBRUNCH_STAT BoolPostP " << boolpostPred;
      errs() << "\nBRUNCH_STAT CartPost " << cartpost << "\n";



      //errs() << "\nBRUNCH_STAT MSATSatCalls " << ms.satCalls;;
      //errs() << "\nBRUNCH_STAT Z3SatCalls " << z3.satCalls;;
      //errs() << "\nBRUNCH_STAT AllSatCalls " << ms.allsatCalls;;
      //errs() << "\nBRUNCH_STAT AllSatAsn " << ms.allsatAsn << "\n";
      errs() << "BRUNCH_STAT NodesNum " << aArg.size () << "\n";

      errs() << "\nBRUNCH_STAT InterpTime " << interpTime;
      errs() << "\nBRUNCH_STAT CoverTimeU " << coverTimeU;
      errs() << "\nBRUNCH_STAT ExprTime " << exprTime;
      errs() << "\nBRUNCH_STAT PostTime " << postTime;
      errs() << "\nBRUNCH_STAT CleanTime " << cleanTime;


      struct rusage ru;
      getrusage (RUSAGE_SELF, &ru);
      errs() << "\nBRUNCH_STAT UTime " <<  
	ru.ru_utime.tv_sec + (ru.ru_utime.tv_usec * 1.0 / 1000000);

      errs() << "\nBRUNCH_STAT STime " <<  
	ru.ru_stime.tv_sec + (ru.ru_stime.tv_usec * 1.0 / 1000000);
      errs() << "\n\n";
      errs().flush ();

      Stats::uset ("aArg.size", aArg.size ());
      Stats::PrintBrunch (errs ());
      errs ().flush ();
    }

    void processRefinementHints ()
    {
      typedef std::pair<Node*,Expr> NodeExprPair;
      foreach (NodeExprPair nl, refiner->getLabels ())
	adom.refinementHint (nl.first->getLoc (), nl.second);

    }

    
    

    /** maps a location to a list of active nodes for it.
        used by ufoExpand
    **/
    std::map<CutPointPtr, std::vector<Node*> > activeHeads;

    /** maps a location to a future node for it
        used by ufoExpand 
    **/
    std::map<CutPointPtr, Node* > futureNodes;

    // -- Plucks a node from the future and makes it current.
    // -- The node must exist!
    Node* pickNextNode (CutPointPtr cp)
    {
      errs () << "\nPICKING " <<  cp->getBB()->getName() << "\n";

      std::map<CutPointPtr,Node*>::iterator it = futureNodes.find (cp);
      if (it == futureNodes.end ()) return NULL;
      Node *res = it->second;
      futureNodes.erase (it);
      return res;
    }

    /** For a covered node n, remove all nodes from the future to
        which n is the only parent */
    void fixFuture (Node* n)
    {

      foreach (Node* kid, n->getChildren ())
	if (wto.isInCompOrSub(*n->getLoc (), *kid->getLoc ()))
	  // -- assert (kid->parents.size () == 1);
	  // -- if (kid->parents.size () == 1)
	  futureNodes.erase(kid->getLoc ());
    }

    // -- returns node for location loc if it exists, or create a new one
    Node* pickFutureNode (CutPointPtr loc)
    {
      Node* n;
      if (futureNodes.count (loc) == 0)
        {
          n = aArg.newNode (loc);
          errs () << "Creating new node: " << name(n) << "\n";
          futureNodes [loc] = n;
        }
      else
	n = futureNodes [loc];
      return n;      
    }

    void registerFutureNode (Node* n)
    {
      if (futureNodes.count (n->getLoc ()) == 0)
	futureNodes [n->getLoc ()] = n;

      Node *k = futureNodes [n->getLoc ()];
      // -- future is deterministic
      assert (k == n);
    }

    bool forcedCover(Node* n, Expr lbl)
    {
      foreach (Node* root, n->getParents ())
        {
          Environment env(efac);
          SEdgePtr edge = *(root->getLoc ()->findSEdge (*n->getLoc ()));
          updateFormula uf(env);

          const CutPointPtr src = edge->getSrc();
          for (BasicBlock::const_iterator it = src->getBB ()->begin(), 
		 end = src->getBB()->end (); it != end; ++it) 
	    {
	      // -- stop at first non-phi node
	      if (!isa<PHINode> (*it)) break;

	      // map phi to what phi' is pointing to
	      const Value *phi = it;
	      Expr u = env.lookup (variant::prime (mkTerm (phi, efac)));
	      env.bind (*phi, u);
	    }

          // -- prestate
          Expr pre = visit(uf, root->getLabel ());

          // -- then compute the edge 
          SEdgeCondComp x(efac, env, edge, lbe);
          x.compute();
          Expr econd = x.getCond();
          errs() << "\n\n\nfrom " << name(root) << " to " << name(n);
          //errs() << "\nforcedPRE: " << pre;

          //errs() << "\nforcedEDGE: " << econd;

          // -- poststate
          // -- first update phi nodes in env
          const CutPointPtr dst = edge->getDst();
          for (BasicBlock::const_iterator it = dst->getBB ()->begin(), 
		 end = dst->getBB()->end (); it != end; ++it) 
	    {
	      // -- stop at first non-phi node
	      if (!isa<PHINode> (*it)) break;

	      // map phi to what phi' is pointing to
	      const Value *phi = it;
	      Expr u = env.lookup (variant::prime (mkTerm (phi, efac)));
	      env.bind (*phi, u);
	    }


          Expr post = visit(uf, lbl);

          //errs() << "\nforcedPOST: " << post;
          Expr lhs = mk<AND>(pre,econd);
          boost::tribool sat = isSAT(mk<NEG>(mk<IMPL>(lhs,post)));
          if (sat == true || maybe(sat)) return false;
        }
      return true;
    }

    bool isCoveredAbs (Node* n, std::vector<Node*>& heads)
    {
      n->uncover();

      //errs () << "\nBoxes Covering\n";
      AbstractState rhs = adom.bot (n->getLoc ());
      std::vector<Node*> cover;
      AbstractStateVector v;

      foreach (Node *p, heads) 
	{ 
	  if (p->getState ())
	    {
	      v.push_back (p->getState ());
	      cover.push_back (p);
	    }
	}
	
      if (adom.isLeq (n->getLoc (), n->getState (), v))
        {
          errs () << "\ncovered\n";
          n->coverBy(cover.begin(), cover.end());
          return true;
        }

      return false;
    }


    bool isCoveredGlobal(Node* n){
      bool covered;
      Expr lbl;
      if (n->getLabel())
	lbl = n->getLabel();
      else 
	lbl = adom.gamma(n->getLoc(), n->getState());
      
      covered = !invAdd(n->getLoc(),lbl);

      n->uncover();
      std::vector<Node*> nodes;
      nodes.push_back(n);
      
      if (covered) n->coverBy(nodes.begin(), nodes.end());
      
      return covered;
    }
    
    // XXX needs optimization
    bool isCovered (Node* n, std::vector<Node*>& heads, bool force, 
		    bool isMarked = false)
    {
      // -- if node is marked, then check cover w.r.t to boxes
      if (isMarked && ufocl::COVER == ADOM) return isCoveredAbs (n, heads);

      if (ufocl::COVER == GLOBAL) return isCoveredGlobal(n);

      // XXX Check if it is still covered by what it was covered before...
      // XXX Need to be careful about nodes that where uncovered before
      // XXX and are covered now... 
      // XXX Guess such nodes are impossible under current 
      // XXX exploration strategy

      // -- First mark uncovered, then check if it is covered
      n->uncover ();

      // -- minumum number of initial unrollings
      if (heads.size () < ufocl::MIN_UNROLL) return false;

      // -- minum unrolling step
      if (((heads.size () - ufocl::MIN_UNROLL) % ufocl::UNROLL_STEP) != 0)
	return false;


      //errs() << "\n ==> Trying to cover  " << name(n) << "\n ";          
      //std::cout << n->getLabel() << " ===> " << flush;
      if (heads.empty()) return false;

      errs() << "\nBegin cover\n";
      Expr right = falseE;
      {
	std::vector<Node*> cover;
	foreach (Node *h, heads)
          {
            cover.push_back (h);
            assert (h->getId () < n->getId ());
	    if (h->getLabel ())
	      right = boolop::lor (right, h->getLabel ());
	    else if (h->getState ())
	      right = boolop::lor (right, 
				   adom.gamma (h->getLoc (), h->getState ()));
	  }
	// -- for now, assume covered
	n->coverBy (cover.begin (), cover.end ());
      }
      errs() << "\nEnd cover\n";

      //errs()  << " \n\n";
      Expr nval = n->getLabel ();
      if (!nval) nval = adom.gamma (n->getLoc (), n->getState ());
      Expr formula = mk<NEG>(mk<IMPL>(nval, right));
      Stats::resume ("ufo.expand.is_covered.sat");
      boost::tribool sat = isSAT(formula);
      Stats::stop ("ufo.expand.is_covered.sat");
      assert (!maybe(sat));

      if(!sat)
	{ 
	  errs () << "\ncovered\n";
	  return true; 
	}

      if (force && forcedCover(n,right))
        {
          // -- if uncovered, try to forcefully cover it
          n->setLabel (boolop::land(n->getLabel (), right));
          //errs() << "Forced covering " <<"\n";
          return true;
        }

      // -- covering did not work out, uncover and return
      n->uncover ();
      return false;
    }


    /** widen for box */
    AbstractState widen (std::vector<Node*> &heads, Node* n)
    {

      errs () << "\n==WIDEN GCNR08=\n";
      // --  is this a dead end?
      if (adom.isBot (n->getLoc (), n->getState ())) return n->getState ();
      
      if (heads.empty()) return n->getState ();


      AbstractStateVector vec;
      foreach (Node* h, heads) 
	if (h->getState ()) vec.push_back (h->getState ());

      if (vec.empty ()) return n->getState ();
      
      return adom.widenWith (n->getLoc (), vec, n->getState ());
    }

    /** returns true iff { label(n) } edge(n,c) {cLabel} is a valid
	Hoare tripple */
    bool isValidProofEdge (Node *n, Expr cLabel, Node *c)
    {
      // an edge is a valid proof edge iff  (label(n) && edge(n,c) -> label(c))
      Expr pre = n->getLabel ();
      if (!pre) pre = adom.gamma (n->getLoc (), n->getState ());

      Environment env (efac);
      SEdgePtr edge = *(n->getLoc ()->findSEdge (* (c->getLoc ())));
      Expr ant = SEdgeCondComp::computeEdgeCond (efac, env, pre, edge, lbe);

      return isSAT (mk<AND> (ant, mk<NEG> (env.eval (cLabel)))) == false;
    }
    

    /** Expand step of UFO algorithm */
    Node* ufoExpand(Node *root)
    {
      // -- clear global state
      activeHeads.clear();
      futureNodes.clear();
      // -- log this call for versioning nodes
      expandcalls++;

      // -- start w/ root
      Node* n = root;

      while (true)
        {
          errs() << "\nlooking at node " << name(n) << "\n";
          n->setVisit (expandcalls);
          // -- remember if node was marked
          bool wasMarked = n->isMarked();
          errs () << (wasMarked ? "marked\n" : "not marked\n");
          // -- if marked recompute label
          if (n->isMarked ())
	    {
	      CutPointPtr cp = n->getLoc ();
	      // -- if no children, add them. 
	      if (n->getChildren ().empty())
		{
		  foreach (SEdgePtr sedge, *cp)
		    {
		      // -- skip children that can never get to error
		      if (reachSet.count (sedge->getDst ()) <= 0) continue;
		      Node* child = pickFutureNode(sedge->getDst());
		      errs() << "\tAdding child " << name(child) << "\n";
		      n->addChild (*child);
		      child->mark ();
		    }
		}
	      errs() << "Done adding children\n";

	      // -- root starts at the top
	      if (n->getParents ().empty ())
	      { n->setState (adom.top (n->getLoc ()));
	      }
	      else // -- non-root is updated using post
		{
		  BoolVector dead (n->getParents ().size (), false);
		  
		  Stats::resume ("ufo.post");
		  LocLabelStatePairVector pre;
	      
		  // -- label of n is join of POST of parent labels
		  foreach (Node* p, n->getParents ())
		    {		      
		      // -- skip nodes that are not part of the current arg
		      if (p->getVisit () != n->getVisit ()) continue;
		     
		      LabelStatePair lsp;
		      
		      lsp.second = p->getState ();

		      // -- don't propagate from covered states
		      // XXX ugly hack
		      if (p->isCovered ()) lsp.second = adom.bot (p->getLoc ());
		      else if (!lsp.second) lsp.first = p->getLabel ();
		      
		      
		      pre.push_back (std::make_pair (p->getLoc (), lsp));
		    }
		  
		  
		  // -- abstract post
		  n->setState (adom.post (pre, n->getLoc (), dead));
		  Stats::stop ("ufo.post");

		  
		  LOG("ufo_verbose",
		      dbgs () << "Label after post on " << name(n) << "\n"
		      << *adom.gamma (n->getLoc (), n->getState ())
		      << "\n\n";);
		  
		  
		  // check the label
		  if (ufocl::UFO_VALIDATE == POST)
		    {
		      errs () << "CHECKING POST\n";
		      CheckProof cp (efac, adom, lbe, aArg);
		      foreach (Node *p, n->getParents ())
			{
			  if (p->isCovered ()) continue;
			  if (p->getVisit () != n->getVisit ()) continue;
			  assert ((bool)cp.checkEdge (p, n) == true);
			}
		      
		      errs () << "DONE WITH POST\n";
		    }
		  
		  
		  if (ufocl::FALSE_EDGES)
		    {
		      size_t count = 0;
		      foreach (Node *p, n->getParents ())
		  	{
			  // -- skip parents that are not in the current arg
			  // -- and don't count them
			  if (p->getVisit () == n->getVisit () && 
			      dead [count++])
			    {
			      aArg.setFalseEdge (p, n);
			      errs () << "FALSE_EDGE " 
				      << name(p) << " -> " << name (n)
				      << "\n";
			    }
		  	  else
		  	    aArg.clearFalseEdge (p, n);
		  	}
		    }
		}
	      
	      

	      // -- widen
	      if (wto.isWtoHead (*n->getLoc ()))
		// -- Widen every WIDEN_STEPs
		if (activeHeads[n->getLoc()].size() > 0 &&
		    activeHeads[n->getLoc()].size() % ufocl::WIDEN_STEP == 0)
		  {
		    n->setState (widen (activeHeads [n->getLoc ()], n));

		    LOG ("ufo_verbose",
			 dbgs () << "Label after widen on " << name(n) << "\n"
			 << *adom.gamma (n->getLoc (), n->getState ())
			 << "\n\n";);

		  }
	      

	      //inv.add(n->getLoc(), adom.gamma(n->getLoc(), n->getState()));
	      // //XXX COMBINE LABELS
	      // if (ufocl::COMBINE)
	      // 	// AG: at this point, n->getLabel () is the label
	      // 	// AG: computed by predicate abstraction
	      // 	n->setLabel (boolop::land (n->getLabel (), 
	      // 				   adom.gamma (n->getLoc (), 
	      // 					       n->getState ())));
	     
	      // -- if node is false and child c does has a label through interpolants, then don't mark c
	      foreach (Node* child, n->getChildren ())
		{
		  // -- if node labeled false, don't register child (someone else will)
		  // and don't mark it.
		  if (!(adom.isBot(n->getLoc(), n->getState()))){
		    registerFutureNode (child);
		    child->mark ();
		  }
		}


	      errs () << "Done computing post\n";
	    
	      // -- done w/ n, unmark
	      n->unmark ();
	    }
          else /* unmarked */
	    {
	      // -- register children w/ the future
	      foreach (Node *child, n->getChildren ())
		registerFutureNode (child);
	      

	      // -- update label if one exists
	      if (refiner->hasLabel (n))
		{
              
		  errs () << "\nlabeling node: " << name(n) << "\n"; 
		  
		  LOG("ufo_verbose",
		      dbgs () << "node " << name(n) << " gets " 
		      << *(refiner->getLabel (n)) << "\n";);


		  if (ufocl::UFO_CONJOIN_LABELS || ufocl::UFO_CONJOIN_OLD_LABEL)
		    {
		      // XXX cleanup this block of code
		      Expr oldLabel;
		      if (ufocl::UFO_CONJOIN_OLD_LABEL) 
			oldLabel = n->getLabel ();

		      if (!oldLabel && n->getState ())
		        oldLabel = adom.gamma (n->getLoc (), n->getState ());
		      
		      bool flag = oldLabel;
		      if (flag)
			{
			  foreach (Node *p, n->getParents ())
			    {
			      // -- skip parents that are not in the
			      // -- currently active portion of the ARG
			      if (p->getVisit () != n->getVisit ()) continue;
			      
			      // -- check that an edge is valid for
			      // -- the old label
			      flag &= isValidProofEdge (p, oldLabel, n);
			      if (!flag) break;
			    }
			}

		      // -- only use the old label if it is valid for
		      // -- all incomming edges
		      if (flag)
			n->setLabel (boolop::land(refiner->getLabel (n), 
						  oldLabel));
		      else
			{
			  errs () << "KILLED OLD LABEL\n";
			  n->setLabel (refiner->getLabel (n));
			}			
		    }
		  else
		    n->setLabel (refiner->getLabel (n));


		  // -- mark children that will not get a new label
		  foreach (Node *child, n->getChildren ())
		    {
		      if (child->isMarked ()) continue;
		      if (!refiner->hasLabel (child)) child->mark ();
		      // -- mark a child that was reachable through a
		      // -- FALSE_EDGE before, but the edge is not a
		      // -- valid proof edge under the new labeling
		      else if (aArg.isFalseEdge (n, child) && 
			       !isValidProofEdge (n, 
						  refiner->getLabel (child), 
						  child)) 
			{
			  errs () << "resurecting dead edge "
				  << name(n) << " -> " 
				  << name(child) << "\n";
			  child->mark ();
			}
		    }		  
		  
		}
	      
	      
	      if (ufocl::UFO_VALIDATE == POST)
		{
		  errs () << "CHECKING LABELS\n";
		  CheckProof cp (efac, adom, lbe, aArg);
		  foreach (Node *p, n->getParents ())
		    {
		      if (p->getVisit () != n->getVisit ()) continue;
		      if (p->isCovered ()) continue;
		      
		      if (refiner->hasLabel (p) && refiner->hasLabel (n))
			{
			  errs () << "checking refiner labels on " 
				  << p->getId () << " -> " 
				  << n->getId () << "\n";
			  assert 
			    ((bool)cp.checkLabels (refiner->getLabel (p), p, 
					     refiner->getLabel (n), n) == true);
			}
		      
		      assert ((bool)cp.checkEdge (p, n) == true);
		    }
		  
		  errs () << "DONE WITH CHECKING LABELS\n";
		} 
	    }


          // -- if error, break
          if (n->getLoc () == locError){ 
	    Expr lbl;
	    if (n->getLabel())
	      lbl = n->getLabel();
	    else 
	      lbl = adom.gamma(n->getLoc(), n->getState());
	    
	    invAdd(n->getLoc(),lbl);

	    break;
	  }

          /** pick next node to process */

          if (wto.isWtoHead (*n->getLoc ()))
	    {
	      Stats::resume ("ufo.expand.is_covered");
	      
	      
	      bool x = isCovered (n, activeHeads[n->getLoc ()],
				  false, wasMarked);
	      Stats::stop ("ufo.expand.is_covered");
	      if (x)
		{
		  // -- remove children of n from the future since 
		  // -- n is covered and will not be expanded more
		  fixFuture (n);

		  // -- heads no longer active
		  activeHeads.erase (n->getLoc ());
		  // -- n is covered, it is not in the future anymore
		  futureNodes.erase (n->getLoc ());

		  //errs () << "here " << name (n) << "\n";
		  // -- pick the head of the next loop to process skip
		  // -- all loops whose head cannot reach error
		  // -- guaranteed to terminate since error location
		  // -- is always last, and is always in reachSet
		  CutPointPtr loc = n->getLoc ();
		  do
		    loc = wto.nextWtoHead (*loc);
		  while (reachSet.count (loc) <= 0);
		 
		  errs () << "Picking next node from future in NEXT component " 
			  << loc->getNameStr() << "\n"; 
		  
		  do{
		    n = pickNextNode (loc);
		    
		    if (n == NULL){ 
		      activeHeads.erase (loc);
		      
		      do
			loc = wto.nextWtoHead (*loc);
		      while (reachSet.count (loc) <= 0);
		    }
  
		  }while (n == NULL);
		  
		  continue;
		}

	      // -- else not-covered, remember this is an active loop head
	      activeHeads[n->getLoc()].push_back (n);
	    }

          /** pick a next node in the current WTO component */
	  {
	    CutPointPtr loc = n->getLoc ();
	    CutPointPtr loc_ = n->getLoc ();
	    do{
	      //errs () << "\nhere";
	      //errs () << loc->getBB()->getName();
	      loc_ = wto.wtoNextSame (*loc);
	      if (loc_ == loc && reachSet.count(loc) <= 0) 
		loc = wto.nextWtoHead(*loc);
	      else loc = loc_;
	    }while (reachSet.count (loc) <= 0);
            errs () << "Picking next node from future in SAME component " 
		    << loc->getNameStr() << "\n"; 
	    n = pickNextNode (loc);
	    assert (n != NULL);
	  }
	  
        }
      return n;
    }

    // -- printing/testing functions
    std::string name(Node* n)
    {
      if (n == NULL) return "NULL";
      std::string s = n->getLoc ()->getBB ()->getName ();
      return boost::lexical_cast<string>(n->getId ()) + ":" + s;
    }

    /**
       Returns true if path argument can be extended to a path to dst
       going through nodes. If successful, path contains the extended
       path.
    */
    bool findPath (std::set<const Node*> &nodes, 
		   Node* dst, 
		   std::vector<Node*>& path)
    {

      Node* curr = path.back();

      // -- found destination
      if (curr == dst)
        {
          // -- check that there is a feasible exection
          errs() << "\nFEASIBILITY CHECK OF A PATH:\n";

          // -- compute path condition for a path
          Environment env(efac);
          Expr pathCond = trueE;
          for (size_t i = 0; i < path.size() - 1; ++i)
	    {
	      Node* s = path [i];
	      Node* d = path [i+1];

	      errs () << "Edge between " << name(s) 
		      << " - " << name(d) <<":\n";
	      SEdgePtr edge = *s->getLoc ()->findSEdge (*d->getLoc ());
	      SEdgeCondComp x(efac, env, edge, lbe);
	      x.compute();
	      //std::cout << x.getCond() << flush;
	      pathCond = boolop::land(pathCond, x.getCond());
	    }
          if (isSAT (pathCond)) 
	    {
	      errs () << "\nCEX\n";
	      return true;
	    }
          else
	    {
	      errs () << "\nNo CEX\n";
	      return false;
	    }

        }

      // -- current node is not he destination
      // -- look at children
      foreach (Node *c, curr->getChildren ())
        {
          // -- skip children that are not in our set of nodes
          if (nodes.count(c) == 0 && c != dst) continue;
          // -- add child to the path and recurse
          path.push_back (c);
          if (findPath (nodes, dst, path)) return true;
          path.pop_back ();
        }
      return false;
    }

    void checkCex(Node* root, Node* err)
    {
      std::set<const Node*> nodes;
      foreach (Node *n, aArg.getNodes ())
	if (isOpX<TRUE>(ms.getModelValue (mkTerm (n, efac))))
	  nodes.insert (n);

      std::vector<Node*> path;
      path.push_back(root);

      // -- assert that counterexample is found
      assert (findPath (nodes, err, path));

      printPath (path);
    }

    /**
       Prints a path
       // XXX Change to take a boost range as an argument
       */
    void printPath (std::vector<Node*>& path)
    {
      foreach (Node* n, path)
        {
          const DebugLoc *dloc = n->getLoc ()->getDebugLoc ();
          if (dloc == NULL) 
            errs () << name (n) << "\n";
          else 
	    errs () << "<file>:" << 
	      dloc->getLine () << ":" << dloc->getCol () << "\n";
        }
    }


    void printNodeTree (Node *n, std::set<Node*> &visited)
    {
      if (visited.count (n) > 0) return ;

      foreach (Node *child, n->getChildren ())
	errs() << "\t\"" << name(n) << "\" -> " 
	       << "\"" << name(child) << "\";\n";
      visited.insert (n);

      foreach (Node* child, n->getChildren ())
	printNodeTree (child, visited);
    }

    // -- sets nodeLabels map to invariant at each node
    void setNodeLabels(){
      nodeLabels.clear();
      if (ufocl::UFO_LBL==INV){
	
	foreach (Node* n, aArg.getNodes()){
	  errs () << "Getting node " << name(n);
	  if (n->getParents().empty())
	    nodeLabels[n] = mk<TRUE>(efac);
	  else if (n->getChildren().empty())
	    nodeLabels[n] = mk<TRUE>(efac);
	  else
	    nodeLabels[n] = invGet(n->getLoc());
	}

      }else if (ufocl::UFO_LBL==NODES){
        foreach (Node* n, aArg.getNodes()){
	  errs () << name(n) << "\n";
	  if (n->getParents().empty())
	    nodeLabels[n] = mk<TRUE>(efac);
	  else{
	    nodeLabels[n] = n->getLabel();
	    if (!nodeLabels[n] && n->getState())
	      nodeLabels[n] = adom.gamma(n->getLoc(), n->getState());
	  }
        }
      }
    }

    /** the following functions maintain an invariant in cp2inv
      by interfacing with the SmartExprSet datatype
      */
    bool invAdd(CutPointPtr cp, Expr e)
    {
      if (cp2inv.count (cp) <= 0) cp2inv[cp] = new ReducedExprSet (efac);
      return cp2inv[cp]->add(e);
    }

    Expr invGet(CutPointPtr cp)
    {
      InvarMap::const_iterator it = cp2inv.find (cp);
      assert (it != cp2inv.end ());
      return (it->second)->get ();
    }
    
    void invInit()
    {
      foreach (InvarMap::value_type &v, cp2inv) v.second->reset ();
    }

  public:

    // -- returns true if error found, false otherwise
    bool ufo ()
    {
      Stats::resume ("ufo.total");
      Node* root = pickFutureNode (locInit);
      root->setState (adom.top (root->getLoc ()));
      root->mark ();
      errs () << " Picked root node and about to start UFO\n";

      // -- iteration counter
      unsigned iter = 0;

      while (true) {
	//inv.reset();
	//cp2inv.clear();
        invInit();

	Stats::count ("ufo.main_loop");
	// XXX restart mathsat, just in case
	ms.restart (false);
	errs() << "\n\n========================ITER " << iter
	       << " ========================\n\n";
	errs() << "Largest node: " << aArg.size () << "\n";
	printReport(iter++, boost::indeterminate);

	// -- OD-only mode
	if (ufocl::UFO_OD_ONLY) {
	  // -- mark root, this will cause POST to be recomputed on
	  // -- the whole tree
	  root->mark ();
	  refiner->reset();
	}

	Stats::resume ("ufo.expand");
	// -- expand ARG from root, using newLabels when available
	Node* err = ufoExpand(root);
	Stats::stop ("ufo.expand");

	errs () << "ERROR NODE :" << name (err) << "\n";

	if (ufocl::UFO_DEBUG){
	  std::set<Node*> v;
	  printNodeTree (root, v);
	}

	if (ufocl::UFO_VALIDATE >= ITER)
	{
	  errs () << "VALIDATING PROOF: ";
	  CheckProof cp (efac, adom, lbe, aArg);
	  boost::tribool val = cp.validate ();
	  errs () << val << "\n";
	  if (val == false)
	    {
	      errs () << "failed node: " << name (cp.getFailedNode ())
		      << " dst " << name (cp.getFailedDst ()) << "\n";
	      assert (0);
	    }
	  
	  
	  errs () << "END VALIDATING PROOF\n";
	  errs ().flush ();
	}
	
	if (err->getState ())
	  {
	    if (adom.isBot (err->getLoc (), err->getState ()))
	      {
		Stats::stop ("ufo.total");
		printReport(iter,true);
		errs () << "UNREACHABLE: " 
			<< "(by " << adom.name () << ")"
			<< "\n";
		if (ufocl::UFO_VALIDATE >= LAST) 
		  {
		    CheckProof cp (efac, adom, lbe, aArg);
		    // -- compute inv and then check it
		    boost::tribool invar = true;
		    //cp.checkInvariant(inv.cp2inv); 
		    if (invar == false){
		      errs () << "Invariant check failed\n";
                      assert (0);
		    }
		    
		    boost::tribool val = cp.validate ();
		    errs () << val << "\n";
		    if (val == false)
		      {
			errs () << "failed node: " << name (cp.getFailedNode ())
				<< " dst " << name (cp.getFailedDst ()) << "\n";
			assert (0);
		      }
		  }

		return false;
	      }
	  }
	// -- Label of error node is unsat, program is SAFE
	else if (err->getLabel ())
	  {
	    if (!isSAT(err->getLabel ()))
	      { 
		Stats::stop ("ufo.total");
		printReport(iter,true); 
		errs () << " UNREACHABLE: (by interpolants)\n";
		if (ufocl::UFO_VALIDATE >= LAST) 
		  {
		    CheckProof cp (efac, adom, lbe, aArg);
		    // -- compute inv and then check it
		    boost::tribool invar = true;//XXX cp.checkInvariant(inv.cp2inv); 
		    if (invar == false){
		      errs () << "Invariant check failed\n";
                      assert (0);
		    }
		    
		    boost::tribool val = cp.validate ();
		    errs () << val << "\n";
		    if (val == false)
		      {
			errs () << "failed node: " << name (cp.getFailedNode ())
				<< " dst " << name (cp.getFailedDst ()) << "\n";
			assert (0);
		      }
		  }
		return false; 
	      }
	  }

	// -- compute invariant if they'll be used as labels
	//XXX if (ufocl::UFO_LBL==INV)
	  //inv.simplifyInv();
	// -- set nodelabels
	setNodeLabels();

	// -- REFINE all paths from root to err and produce new labels
	refiner->reset ();
	refiner->setEntry (root);
	refiner->setExit (err);
	
	Stats::resume ("refiner.total");
	refiner->refine ();
	Stats::stop ("refiner.total");

	// -- UNSAFE, no labels
	if (refiner->getLabels ().empty ())
	  { 
	    Stats::stop ("ufo.total");
	    printReport(iter, false); 
	    errs () << " REACHABLE: checking cex\n";
	    if (ufocl::UFO_VALIDATE >= LAST)  checkCex(root, err);
	    return true;
	  }
	  
	processRefinementHints ();
      }
    }
  };

  struct Lawi : public ModulePass 
  {

    static char ID;
    Lawi() : ModulePass(ID) {errs () <<" Creating LAWI \n";}    

    static bool exit;
    virtual bool runOnModule(Module &M) 
    { 
      if (exit) return false;
      else exit = true;
      // if (!ufocl::COMBINE && (ufocl::POST==BOX_LIGHT_CPRED || ufocl::POST==BOX_LIGHT_BPRED || ufocl::POST==BOXCPRED || ufocl::POST==BOXBPRED))
      // {  assert (0 && "Predicate abstraction with BOX requires --ufo-combine=true"); }
      /*
         Function& F = *(M.getFunction("main")); 
         if (F.getName().compare("main") != 0) return false;
         LBE &lbe = getAnalysis<LBE>();
         WTOPass &wto = getAnalysis<WTOPass>();
         CutPointPtr li = lbe.bb2cp [&F.getEntryBlock ()];
         CutPointPtr l2 = (li->succ[0])->getDst();
         BoxesDom domain;
         domain.init(F);
         BoxesVal top = domain.top();
      //assert (l2->succ[1] != NULL);
      SEdgeAbsPost x(top, (l2->succ[0]), domain);
      BoxesVal post = x.computePost();
      errs () << "\nPost1: ";
      domain.printVal(post);

      SEdgeAbsPost x2(post, (l2->succ[0]), domain);
      BoxesVal post2 = x2.computePost();
      errs () << "\nPost2: ";
      domain.printVal(post2);
      //domain.printVars();

      BoxesVal w = domain.widen (post, post2);
      errs () << "\nWidening: ";
      domain.printVal(w);

      return false;*/

      Function& F = *(M.getFunction("main")); 
      if (F.getName().compare("main") != 0) return false;
      LBE &lbe = getAnalysis<LBE>();
      WTOPass &wto = getAnalysis<WTOPass>();
      CutPointPtr li = lbe.bb2cp [&F.getEntryBlock ()];

      // -- error location is the exit location
      CutPointPtr le = lbe.cutPts[&F][1];


      if (le == NULL) {
        errs() << "No error location defined\n" 
	       << "TERMINATING\n"; 
        return false;
      }

      // -- check if error cp unreachable in CFG
      if (le->predBegin() == le->predEnd()) {
        errs() << "\n\n\n\nMain does not return\n";
        errs() << "program correct: ERROR unreachable\n\n\n";
        return false;
      }

      errs() << "instantiating mc";

      DominatorTree &DT = getAnalysis<DominatorTree> (F);

      ExprFactory efac;
      ExprMSat ms(efac);
      scoped_ptr<LddBoxAbstractStateDomain> lddBox (0);
      scoped_ptr<LddBoxesAbstractStateDomain> lddBoxes (0);
      //LddBoxesAbstractStateDomain lddBoxes (F, lbe, DT, efac, ufocl::UFO_DVO);

      scoped_ptr<PredicateAbstractDomain> nullPred (0);
      //PredicateAbstractDomain nullPred (lbe, efac, ms, new FalseBoolExprFn ());

      scoped_ptr<CartesianPADomain> cPred (0);
      //CartesianPADomain cPred = CartesianPADomain (lbe, efac, ms);
      scoped_ptr<BoolPADomain> bPred (0);
      //BoolPADomain bPred = BoolPADomain (lbe, efac, ms);

      scoped_ptr<ProductAbstractStateDomain> boxBPred (0);
      //ProductAbstractStateDomain boxBPred (bPred, lddBox);
      scoped_ptr<ProductAbstractStateDomain> boxCPred (0);
      //ProductAbstractStateDomain boxCPred (cPred, lddBox);

      scoped_ptr<ProductAbstractStateDomain> boxesCPred (0);
      scoped_ptr<ProductAbstractStateDomain> boxesBPred (0);
      

      // ProductAbstractStateDomain boxesBPred (bPred, lddBoxes);
      // ProductAbstractStateDomain boxesCPred (cPred, lddBoxes);

      scoped_ptr<BoundAbstractStateDomain> boundDom (0);
      //BoundAbstractStateDomain boundDom (lbe, efac);

      AbstractStateDomain *dom = NULL;
      switch (ufocl::UFO_POST)
	{
	case BOUND:
	  boundDom.reset (new BoundAbstractStateDomain (lbe, efac));
	  dom = boundDom.get ();
	  break;
	case BPRED:
	  bPred.reset (new BoolPADomain (lbe, efac, ms));
	  dom = bPred.get ();
	  break;
	case CPRED:
	  cPred.reset (new CartesianPADomain (lbe, efac, ms));
	  dom = cPred.get ();
	  break;
	case BOX:
	  lddBox.reset (new LddBoxAbstractStateDomain (F, lbe, DT, efac));
	  dom = lddBox.get ();
	  break;
	case BOXBPRED:
	case BOX_LIGHT_BPRED:
	  lddBox.reset (new LddBoxAbstractStateDomain (F, lbe, DT, efac));
	  bPred.reset (new BoolPADomain (lbe, efac, ms));
	  boxBPred.reset (new ProductAbstractStateDomain (*bPred, *lddBox));
	  dom = boxBPred.get ();
	  break;
	case BOX_LIGHT_CPRED:
	case BOXCPRED:
	  lddBox.reset (new LddBoxAbstractStateDomain (F, lbe, DT, efac));
	  cPred.reset (new CartesianPADomain (lbe, efac, ms));
	  boxCPred.reset (new ProductAbstractStateDomain (*cPred, *lddBox));
	  dom = boxCPred.get ();
	  break;
	case BOXES:
	  lddBoxes.reset (new LddBoxesAbstractStateDomain 
			  (F, lbe, DT, efac, ufocl::UFO_DVO));
	  dom = lddBoxes.get ();
	  break;
	case BOXESCPRED:
	  lddBoxes.reset (new LddBoxesAbstractStateDomain 
			  (F, lbe, DT, efac, ufocl::UFO_DVO));
	  cPred.reset (new CartesianPADomain (lbe, efac, ms));
	  boxesCPred.reset (new ProductAbstractStateDomain (*cPred, *lddBoxes));
	  dom = boxesCPred.get ();
	  break;
	case BOXESBPRED:
	  lddBoxes.reset (new LddBoxesAbstractStateDomain 
			  (F, lbe, DT, efac, ufocl::UFO_DVO));
	  bPred.reset (new BoolPADomain (lbe, efac, ms));
	  boxesBPred.reset (new ProductAbstractStateDomain (*bPred, *lddBoxes));
	  dom = boxesBPred.get ();
	  break;
	case NONE:
	  nullPred.reset (new PredicateAbstractDomain 
			  (lbe, efac, ms, new FalseBoolExprFn ()));
	  dom = nullPred.get ();
	  break;
	}
      

      if (ufocl::UFO_POST == BOX_LIGHT_BPRED)
	bPred->setRefinementFilter (new IntervalsRefFilter ());
      else if (ufocl::UFO_POST == BOX_LIGHT_CPRED)
	cPred->setRefinementFilter (new IntervalsRefFilter ());

      assert (dom != NULL);
      scoped_ptr<ModelChecker> mc(new ModelChecker 
				  (efac, ms, li, le, lbe, wto, F, DT, *dom));
      

      errs() << "\ninstantiated model checker - now model checking\n";

      bool res = mc->ufo ();

      // -- validate Ufo result against expected output
      validateUfoResult (res);

      if (res) errs() << "ERROR reachable\n";
      else errs() << "\nprogram correct: ERROR unreachable\n";

      return false;
    }

    virtual void getAnalysisUsage(AnalysisUsage &AU) const{
      AU.addRequired<DominatorTree> ();
      AU.addRequired<LBE> ();
      AU.addRequired<Topo> ();
      AU.addRequired<WTOPass> ();
      AU.addRequired<NameValues> ();
      AU.setPreservesAll ();
    }
  };

  char Lawi::ID = 0;
  bool Lawi::exit = false;
  // static RegisterPass<Lawi> X("lawi", "LAWI model checker", true, true);
}

INITIALIZE_PASS_BEGIN(Lawi, "lawi", "LAWI model checker", false, false)
INITIALIZE_PASS_DEPENDENCY(DominatorTree)
INITIALIZE_PASS_DEPENDENCY(LBE)
INITIALIZE_PASS_DEPENDENCY(Topo)
INITIALIZE_PASS_DEPENDENCY(WTOPass)
INITIALIZE_PASS_DEPENDENCY(NameValues)
INITIALIZE_PASS_END(Lawi, "lawi", "LAWI model checker", false, false)



