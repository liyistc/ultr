#include "ufo/Smt/Abc.hpp"
#include "base/abc/abc.h"
#include "ufo/Util/UfoDebug.hpp"
#include "boost/array.hpp"

#include "llvm/Support/raw_ostream.h"

using namespace ufo;
using namespace expr;
using namespace abc;
using namespace abc_detail;

namespace abc
{
  extern Abc_Frame_t* Abc_FrameAllocate ();
  extern void Cmd_Init (Abc_Frame_t *);
  extern int Cmd_CommandExecute (Abc_Frame_t*, const char*);
  extern void Io_Init (Abc_Frame_t*);
  extern void Abc_Init (Abc_Frame_t*);
  extern void Abc_End (Abc_Frame_t*);
  extern void Io_End (Abc_Frame_t*);
  extern void Cmd_End (Abc_Frame_t*);
  extern void Abc_FrameDeallocate (Abc_Frame_t*);
  extern void Abc_Start ();
  extern void Abc_Stop ();
  extern Abc_Frame_t *Abc_FrameGetGlobalFrame ();
}

static Abc_Aig_t *aigMgr (Abc_Ntk_t *ntk)
{ return static_cast<Abc_Aig_t*> (ntk->pManFunc); }

Abc::Abc (ExprFactory &efac) : m_efac (efac) 
{ 
  Abc_Start ();
  m_frm = Abc_FrameGetGlobalFrame ();
  
  // XXX using local frame does not work since many high-level abc
  // commands implicitly assume existence of global frame. The proper
  // solution is to use the low-level API directly. 

  // m_frm = Abc_FrameAllocate ();
  // Cmd_Init (m_frm);
  // Cmd_CommandExecute (m_frm, "set checkread");
  // Io_Init (m_frm);
  // Abc_Init (m_frm);
}

Abc::~Abc () 
{ 
  Abc_Stop ();
  // Abc_End (m_frm);
  // Io_End (m_frm);
  // Cmd_End (m_frm);
  // Abc_FrameDeallocate (m_frm);
}



Abc_Obj_t* Abc::toAbc (Expr e, Abc_Ntk_t * ntk, ExprObjMap &seen)
{
  if (isOpX<TRUE> (e)) return Abc_AigConst1 (ntk);
  if (isOpX<FALSE> (e)) return Abc_ObjNot (Abc_AigConst1 (ntk));
      
  if (isOpX<NEG> (e)) return Abc_ObjNot (toAbc (e->left (), ntk, seen));

  ExprObjMap::const_iterator it = seen.find (e);
  if (it != seen.end ()) return it->second;

  Abc_Obj_t *res = NULL;

  if (isOp<BoolOp> (e))
    {

      assert (e->arity () == 2);
      Abc_Obj_t *left = toAbc (e->left (), ntk, seen);
      Abc_Obj_t *right = toAbc (e->right (), ntk, seen);
	  

      if (isOpX<AND> (e)) res = Abc_AigAnd (aigMgr (ntk), left, right);
      else if (isOpX<OR> (e)) res = Abc_AigOr (aigMgr (ntk), left, right);
      else if (isOpX<XOR> (e)) res = Abc_AigXor (aigMgr (ntk), left, right);
      else if (isOpX<IFF> (e)) 
	res = Abc_ObjNot (Abc_AigXor (aigMgr (ntk), left, right));
      else assert (0);

    }
  else
    {
      assert (0 && "Unexpected PI. PIs must be pre-created.");
      res = Abc_NtkCreatePi (ntk);
      m_PiNames.push_back (e);
    }

  assert (res != NULL);
  seen [e] = res;
  return res;
}


Expr Abc::toExpr (Abc_Obj_t* obj, Abc_Ntk_t* ntk, ObjExprMap &seen)
{
  bool isCompl = Abc_ObjIsComplement (obj);
  obj = Abc_ObjRegular (obj);
      
  int fanin = Abc_ObjFaninNum (obj);
  assert (fanin <= 2);
      
      
  if (fanin == 0)
    {
      if (Abc_AigNodeIsConst (obj))
	return isCompl ? mk<FALSE> (m_efac) : mk<TRUE> (m_efac);
	  
      assert (Abc_ObjIsPi (obj));
      ObjExprMap::const_iterator it = seen.find (obj);
      assert (it != seen.end ());
      return isCompl ? boolop::lneg (it->second) : it->second;
    }
      
  if (fanin == 1)
    {
      Expr r = toExpr (Abc_ObjFanin0 (obj), ntk, seen);
      if (Abc_ObjFaninC0 (obj)) r = boolop::lneg (r);
      return r;
    }
      
  assert (fanin == 2);
      
  Expr res = NULL;

  ObjExprMap::const_iterator it = seen.find (obj);
  if (it != seen.end ()) res = it->second;
      
  if (res == NULL)
    {
      Expr left = toExpr (Abc_ObjFanin0 (obj), ntk, seen);
      Expr right = toExpr (Abc_ObjFanin1 (obj), ntk, seen);
	  
      if (Abc_ObjFaninC0 (obj)) left = boolop::lneg (left);
      if (Abc_ObjFaninC1 (obj)) right = boolop::lneg (right);
	  
      assert (Abc_AigNodeIsAnd (obj));
      res = mk<AND> (left, right);
      seen [obj] = res;
    }
      
  return isCompl ? boolop::lneg (res) : res; 
}

void Abc::cmd (const char *s)
{
  Abc_Frame_t *abc = frm ();
  LOG("abc", 
      Cmd_CommandExecute (abc, "print_stats"););
  Cmd_CommandExecute (abc, s);
}

Expr ufo::abc_exec (Expr e, const char* cmd)
{
  Abc abc (e->efac ());
  boost::array<Expr,1> in = {{boolop::norm (e)}};
  boost::array<Expr,1> out;
    
  abc.load (in);
  abc.cmd (cmd);
  abc.store (out.begin ());
  Expr res = boolop::gather (boolop::nnf (out [0]));
  LOG("abc", 
      ::llvm::dbgs () << "abc_cmd: " << boolop::circSize (e) 
      << " -> " << boolop::circSize (res) << "\n";);
  return res;

}

Expr ufo::abc_resyn (Expr e)
{
  return abc_exec (e, "balance; rewrite -l; rewrite -lz; "
		   "balance; rewrite -lz; balance");
}

Expr ufo::abc_resyn2 (Expr e)
{
  return abc_exec (e, "balance; drw -l; refactor -l; balance; "
		   "drw -l; drw -lz; balance; refactor -lz; "
		   "drw -lz; balance; &get ; &fraig ; &put ; dc2");
}

Expr ufo::abc_simplify (Expr e)
{
  return abc_exec (e, "dc2 ; dch ; dc2 ; "
		   "&get ; &fraig ; &put & dch & dc2");
}

void abc::NtkSetName (Abc_Ntk_t *ntk, const char *name)
{
  Abc_NtkSetName (ntk, strdup (name));
}

int abc::NtkPoNum (Abc_Ntk_t *ntk)
{
  return Abc_NtkPoNum (ntk);
}

Abc_Obj_t* abc::NtkPo (Abc_Ntk_t *ntk, int i)
{
  return Abc_NtkPo (ntk, i);
}

int  abc::NtkPiNum (Abc_Ntk_t *ntk)
{
  return Abc_NtkPiNum (ntk);
}

Abc_Obj_t* abc::NtkPi (Abc_Ntk_t *ntk, int i)
{
  return Abc_NtkPi (ntk, i);
}

Abc_Ntk_t *abc::newAigNtk ()
{
  return Abc_NtkAlloc (ABC_NTK_STRASH, ABC_FUNC_AIG, 1);
}


Abc_Obj_t *abc::NtkCreatePo (Abc_Ntk_t* ntk)
{
  return Abc_NtkCreatePo (ntk);
}

Abc_Obj_t *abc::NtkCreatePi (Abc_Ntk_t *ntk)
{
  return Abc_NtkCreatePi (ntk);
}


