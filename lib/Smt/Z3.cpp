#include "ufo/Smt/Z3.hpp"

namespace ufoz3
{
  void error_handler (Z3_context ctx, Z3_error_code e)
  {
    errs () << "Z3 Error: "
	    << Z3_get_error_msg_ex (ctx, e)
	    << "\n";
    errs ().flush ();
    exit (1);
  }
}

