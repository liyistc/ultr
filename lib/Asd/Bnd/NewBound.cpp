#include "ufo/Asd/Bnd/NewBound.hpp"

namespace ufo
{
  
  NewBound::~NewBound () {}

  /**
   * scanFace
   * Relate vars appear in the same constraint
   * param[in] Expr phi
   */
  void NewBound::scanFace (Expr phi)
  {
    ExprSet tmpF;
    // Filter inequalities
    filter(phi, ConsFilter(), std::inserter(tmpF, tmpF.begin()));
      
    ExprSet tmpFeq; //Remove duplicates
    forall (Expr f, tmpF)
      tmpFeq.insert (mk<EQ> (f->left(), f->right()));

    forall (Expr f, tmpFeq)
    {
      ExprSet vars = collectVars (f);
      forall (Expr var, vars)
      {
        // Relate vars in the same f with f itself
        relate.relate (var, f);
      }
    }
      
    // Copy back to F
    copy (tmpFeq.begin(), tmpFeq.end(), back_inserter (F));
  }

  /**
   * scanTemplate
   * Relate vars appear in the same template
   */
  void NewBound::scanTemplate ()
  {
    forall (Expr t, varbase)
    {
      ExprSet vars = collectVars (t);
      forall (Expr var, vars)
      {
        // Relate variables within the same template
        relate.relate (var, t);
      }
    }
  }

  /**
   * boundAll. Compute bounds for templates. Store results in U.
   * param[in] formula phi.
   */
  void NewBound::boundAll (Expr phi)
  {
    // Close Phi
    phi = closeFormula (phi);
    // Asserting Phi
    z3.assertExpr (phi);
    // Scan F
    scanTemplate ();
    scanFace (phi);
    
    while (z3.solve())
    {
      ContextQueue rqueue;

      // Global Push
      AbstractState p = findModel (false);
      BitSet pBit = getEQ();
      rqueue.push (Context(p, pBit, 2, false));

      do
      { 
        AbstractState oldU = btcm.copy (U);
        ExprVector updatedT;
        Context ctx = rqueue.front();
        rqueue.pop();

        U = btcm.joinAware (U, ctx.point, updatedT);
        // btcm.print (U);
       
        z3.assertExpr (mk<NEG> (gamma()));

        // If the point is produced by unbound check,
        // then apply type 2 check.
        unboundCheck (ctx,
                      oldU, updatedT, rqueue,
                      ctx.unb);
          
        addEQ(ctx.bit);  
      } while (!rqueue.empty());
        
      // Block current TCM hull
      z3.assertExpr (mk<NEG> (gamma()));
    }
  }

  /**
   * assertStrongerEQ. Make an assertion that asks for a model in
   * a stronger equivalence class.
   * param[in] ExprVector& rf: Available faces
   * param[in] ExprVector& pc: Faces in the current equivalence class
   */
  void NewBound::assertStrongerEQ (ExprVector& rF, ExprVector& pC)
  {
    // rF / ||p|| (set minus)
    ExprVector Fmp;

    set_difference (rF.begin(), rF.end(), pC.begin(), pC.end(),
                    back_inserter(Fmp));

    // Assert "AND ||p|| and (OR F \setminus ||p||)"
    Expr unb = boolop::land (
      mknary<AND> (mk<TRUE>(efac), pC.begin(), pC.end()),
      mknary<OR> (mk<FALSE>(efac), Fmp.begin(), Fmp.end()));

    z3.assertExpr (unb);
  }

  /**
   * relatedF. Find the related faces for term t.
   * param[in] Expr t
   * return ExprVector: related faces
   */
  ExprVector NewBound::relatedF (Expr t)
  {
    ExprVector res;
    forall (Expr f, F)
      if (relate.isRelated (f, t))
        res.push_back (f);

    return res;
  }
    
  /**
   * unboundCheck
   * param[in] Context &ctx: the context of point p1.
   * param[in] AbstractState &Uold: old copy of the under-approximation (TCM hull).
   * param[in] ExprVector &delta: list of dimensions that are updated by p.
   * param[out] ContextQueue &queue: queue for storing contexts need to be
   * further checked.
   * param[in] bool type2: true if this is a type 2 check.
   * effect: check unboundedness of the 
   */
  void NewBound::unboundCheck (NewBound::Context &ctx,
                               AbstractState &Uold, ExprVector &delta,
                               ContextQueue &queue, bool type2)
  {
    // Type 1 check requires a previously visited point
    // which is in a stronger or equal equivalence class.
    // Abort the check if such point is not found.
    if (!type2 && !existStrongerEQ(ctx.bit))
      return;
    
    forall (Expr t, delta) // Check all templates that is updated by p
    {
      // Get t(p), represented as an Interval
      Interval itv = btcm.getBound (t, ctx.point);
      mpq_class tp;
      if (itv.isTop()) // No model for t
      {
        // Join U with top
        btcm.joinBound (t, U, Interval::top());
        continue;
      }

      // p has to be a single point
      assert (itv.first == itv.second);
      tp = itv.first;
        
      Expr tp_expr = mkTerm (tp, efac); // Expr of t(p)
      ExprVector pClass = bitvec2eq (ctx.bit); // [p]
      bool positive = !leq (tp, btcm.getBound (t, Uold).second); // t(p)>Uold.max
      bool negative = !leq (btcm.getBound (t, Uold).first, tp); // t(p)<Uold.min
      assert (!positive || !negative);

      if (type2) Stats::count ("newb.gpush"); else Stats::count ("newb.gpush");

      if (type2) // Type 2 unbound check
      {
        tp_expr = type2Check (positive, t, pClass, tp_expr, itv);
        if (tp_expr == NULL)
        {
          btcm.joinBound (t, U, itv);
          return;
        }
      }

      // Push towards a direction (positive/negative) and attempt to find
      // a point which is in a stronger equivalence class.
      optional<NewBound::Context> p3 =
        directionalPush (positive, t, pClass, tp_expr, ctx.degree);

      // Push to the queue if such point p3 is found.
      if (p3) queue.push (*p3);
    }
  }

  /**
   * type2Check.
   * Try find p2 in [p1] such that t(p2)>t(p1).
   * param[in] bool pos: direction (true if +, false if -)
   * param[in] Expr t: template
   * param[in] ExprVector &pClass: set of equalities p1 satisfies
   * param[in] Expr tp_expr: t(p1)
   * param[in] Interval itv: [t(p1), t(p1)]
   * return Expr: p2 if found; otherwise NULL
   */
  Expr NewBound::type2Check (bool pos, Expr t,
                             ExprVector &pClass, Expr tp_expr, Interval itv)
  {
    z3.push();
    if (pos)
      z3.assertExpr(
        boolop::land(mk<GT>(t, tp_expr),
                     mknary<AND>(mk<TRUE>(efac), pClass.begin(), pClass.end())));
    else
      z3.assertExpr(
        boolop::land(mk<LT>(t, tp_expr),
                     mknary<AND>(mk<TRUE>(efac), pClass.begin(), pClass.end())));
      
    bool sat = z3.solve();    
    Expr res = z3.getModel().eval (t);

    z3.pop();

    if (!sat) // No p2 such that t(p2) > t(p1). Cannot do unbound check.
      return NULL;

    assert (isOpX<MPQ> (res));
    Stats::count ("newbound.model");
    return res;
  }

  /**
   * directionalPush.
   * Given p2, try find a p3 such that t(p3)>=t(p2) AND [p3]>[p2].
   * param[in] bool pos: direction (true if +, false if -)
   * param[in] Expr t: template
   * param[in] ExprVector &pClass: set of equalities p2 satisfies
   * param[in] Expr tp: t(p2)
   * param[in] unsigned dgr: >1 do not join p3 with U
   * return optional<Context>: p3 if found; otherwise empty
   */
  optional<NewBound::Context> NewBound::directionalPush
  (bool pos, Expr t, ExprVector &pClass, Expr tp, unsigned dgr)
  {
    z3.push();
    // Get faces related to t
    ExprVector rF = relatedF (t);
    assertStrongerEQ (rF, pClass);
      
    if (pos)
      z3.assertExpr (mk<GEQ> (t, tp));
    else
      z3.assertExpr (mk<LEQ> (t, tp));
    
    if (z3.solve()) // Found a p3 such that t(p3) >= t(p2)
    {
      if (dgr > 1)
      {
        AbstractState p3 = findModel (false);
        z3.pop();
        // return p3
        return optional<NewBound::Context>
          (Context (p3, getEQ(), dgr, true));
      }
      // Just join p3 with U
      else findModel (true);
    }
    // Cannot find such p3
    else if (pos)
      // Join +oo with U
      btcm.joinBound (t, U, Interval (posInf(), posInf()));
    else
      // Join -oo with U
      btcm.joinBound (t, U, Interval (negInf(), negInf()));

    z3.pop();
    // Return empty Context.
    return optional<NewBound::Context>();
  }

  /**
   * Get Equivalence Class (represented as bit vectors) of the current model.
   * Return a Bit Vector representing the equivalence class that the
   * current model is in.
   * return BitVector&: a bit vector representing the set of satisfied equalities
   * by the current model.
   */
  NewBound::BitSet NewBound::getEQ ()
  {
    BitSet res (F.size());
    // generate bit vector
    unsigned i = 0;
    forall (Expr f, F)
    {
      Expr m = z3.getModel().eval (f);
      if (isOpX<TRUE>(m))
        res[i] = true;
      else if (isOpX<FALSE>(m))
        res[i] = false;
      else // partial model
      {
        res[i] = false;
      }
      ++i; 
    }
    // return the inserted element
    return res;
  }

  /**
   * Get equalities from bit vectors
   * param[in] BitVector& b: a bit vector representing the satisfied equalities
   * return ExprVector: a set of satisfied equalities
   */
  ExprVector NewBound::bitvec2eq (BitSet& b)
  {
    ExprVector res;
    unsigned i = 0;
    forall (Expr f, F)
    {
      if (b[i])
        res.push_back (f);
      ++i;
    }
     
    return res;
  }

  /**
   * Return true if there is stronger EQ stored in EQbase
   */
  bool NewBound::existStrongerEQ (BitSet &_p)
  {
    forall (BitSet b, EQbase)
    {
      if (_p.is_subset_of (b))
      {
        Stats::count ("newbound.strong");
        return true;
      }
    }
    return false;
  }

  /**
   * findModel.
   * Compute the alpha of the model in the current context.
   * param[in] bool joinWithU: join the model found with U if true.
   * return AbstractState: alpha_TCM (model)
   */
  AbstractState NewBound::findModel (bool joinWithU)
  {
    ExprMap bmodel, rmodel;
    forall (Expr _v, boolVars)
      bmodel[_v] = z3.getModel().eval (_v);
    forall (Expr _v, realVars)
      rmodel[_v] = z3.getModel().eval (_v);

    Stats::count ("newbound.model");
    AbstractState p = btcm.point2state (bmodel, rmodel);

    if (joinWithU)
    {
      U = btcm.join (CutPointPtr(), U, p);
      addEQ (getEQ());
    }
    
    return p;
  }

  /**
   * print bit vector
   */
  void NewBound::printBit (NewBound::BitSet& b)
  {
    errs () << "Bit: ";
    unsigned i;
    for (i=0; i<b.size(); ++i)
      errs () << b[i];
    errs () << "\n";
  }
}
