set (LLVM_REQUIRES_EH ON)
add_llvm_library(UFOUtil
        AlwaysInlineMark.cpp
        ErrorExit.cpp
        ExitReturn.cpp
        NameValues.cpp
        NondetInit.cpp
        ReturnGoto.cpp
        ShadowMem.cpp
        Stats.cpp
        UfoDebug.cpp
        SetLimitsOpts.cpp
        ufocl.cpp
        EdgeComp.cpp
        )
