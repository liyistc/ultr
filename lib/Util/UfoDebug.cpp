#include "ufo/Util/UfoDebug.hpp"
#include "ufo/Util/UfoAssert.hpp"
#include "llvm/Support/CommandLine.h"
#include <string>
#include <set>

#ifndef NUFOLOG
using namespace ufo;
using namespace llvm;

#ifndef NUFOLOG

bool ufo::UfoLogFlag = false;
std::set<std::string> ufo::UfoLog;

void ufo::UfoEnableTrace (std::string x) 
{
  if (x.empty ()) return;
  UfoLogFlag = true;
  UfoLog.insert (x); 
}
  
namespace
{
  struct UfoLogOpt 
  {
    void operator= (const std::string &tag) const
    {
      UfoLogFlag |= tag.empty ();
      UfoEnableTrace (tag);
    }    
  };  
}

static UfoLogOpt UfoLogOptLoc;
static cl::opt<UfoLogOpt, true, cl::parser<std::string> >
UfoLogOption ("ufo-log", cl::desc("Enable a specific log level"),
	      cl::Hidden, cl::value_desc("log level"),
	      cl::location (UfoLogOptLoc), cl::ValueRequired,
	      cl::ZeroOrMore);

static cl::opt<bool> 
ExpectedRes ("ufo-res", cl::desc ("Expected result"),
             cl::Hidden, cl::ValueRequired);

#endif

void ufo::validateUfoResult (bool res)
{
  // -- only validate if the option was explicitly given on command line
  if (ExpectedRes.getPosition () > 0 && ExpectedRes != res)
    {
      errs () << "Expected: " << ExpectedRes << " but got " << res << "\n";
      ufo::assertion_failed ("Unexpected result", __FILE__, __LINE__);
    }
}


#else
#endif

