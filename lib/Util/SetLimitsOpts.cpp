#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"
#include <sys/time.h>
#include <sys/resource.h>
#include <climits>

#include "ufo/Util/SetLimitsOpts.hpp"

unsigned ufo::ufoCpuLimit = UINT_MAX;
unsigned ufo::ufoMemLimit = UINT_MAX;
using namespace llvm;

namespace
{
  struct UfoCpuOpt
  {
    void operator= (const unsigned v) const
    {
      ufo::ufoCpuLimit = v;
      struct rlimit rlim = {v, v};
      setrlimit (RLIMIT_CPU, &rlim);
      errs () << "UFO: cpu limit set to " << v << "s\n";
    }
  };    

  struct UfoMemOpt
  {
    void operator= (const unsigned v) const
    {
      unsigned mb = v * 1024 * 1024;
      ufo::ufoMemLimit = mb;
      struct rlimit rlim = { mb, mb };
      setrlimit (RLIMIT_AS, &rlim);
      errs () << "UFO: memory limit set to " << v << "MB\n";
    }
  };
}



static UfoCpuOpt UfoCpuOptLoc;  
static cl::opt<UfoCpuOpt, true, cl::parser<unsigned> >
UfoCpuOption ("ufo-cpu", 
              cl::desc("Limit cpu usage"),
              cl::value_desc ("seconds"),
              cl::location (UfoCpuOptLoc), 
              cl::ValueRequired, 
              cl::Optional);

static UfoMemOpt UfoMemOptLoc;  
static cl::opt<UfoMemOpt, true, cl::parser<unsigned> >
UfoMemOption ("ufo-mem", 
              cl::desc("Limit memory"),
              cl::value_desc ("MB"),
              cl::location (UfoMemOptLoc), 
              cl::ValueRequired, 
              cl::Optional);

