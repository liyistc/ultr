#include "ufo/InitializePasses.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Instructions.h"
#include "llvm/Module.h"
#include "llvm/Function.h"
#include "llvm/BasicBlock.h"
#include "llvm/PassManager.h"
#include <llvm/LLVMContext.h>
#include "llvm/Support/IRBuilder.h"


using namespace llvm;

namespace 
{
  /** replaces ERROR label w/ exit(5) */
  struct ErrorExit : public ModulePass
  {
    static char ID;
   
    ErrorExit () : ModulePass (ID) {}
    

    virtual bool runOnModule (Module &M) 
    {
      LLVMContext &Context = M.getContext ();
      Constant* exitFn = 
	M.getOrInsertFunction ("exit", 
			       Type::getInt32Ty (Context),
			       Type::getInt32Ty (Context), 
			       NULL);
      
      for (Module::iterator FI = M.begin(), E = M.end(); FI != E; ++FI)
	{
	  for (Function::iterator b = FI->begin(), be = FI->end(); b != be; ++b)
	    {
	      BasicBlock *bb = &*b;
	      // -- skip not ERROR labeled blocks 
	      if (bb->getName ().compare ("ERROR") != 0) continue;
	      
	      IRBuilder<> B(FI->getContext ());
	      B.SetInsertPoint (bb->begin ());
	      B.CreateCall (exitFn, B.getInt32 (5));
	    }
	}
      return true;
    }
    
    virtual void getAnalysisUsage (AnalysisUsage &AU) const
    {
      AU.setPreservesAll ();
    }
    
  };
char ErrorExit::ID = 0;
}

//static RegisterPass<ErrorExit> X("error-exit", 
//				     "adds exit() if ERROR label is reached");
INITIALIZE_PASS(ErrorExit, "error-exit", 
		"adds exit() if ERROR label is reached", false, false)
