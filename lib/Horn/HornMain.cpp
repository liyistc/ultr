#include "ufo/InitializePasses.h"

#include "llvm/Support/CommandLine.h"
#include "llvm/Module.h"
#include "llvm/Support/raw_ostream.h"


#include "ufo/Cpg/Topo.hpp"
#include "ufo/Cpg/Wto.hpp"
#include "ufo/Util/NameValues.hpp"

#include "ufo/Smt/UfoZ3.hpp"
#include "ufo/CptLiveValues.hpp"


using namespace llvm;
using namespace ufo;


static cl::opt<std::string>
MuzOutput("muz-output",
           cl::desc("File to store horn system"),
           cl::value_desc("FILE"), cl::init(""));
static cl::opt<bool>
MuzSolve ("muz-solve", cl::desc ("Run the solver"),
       cl::init (true));

static cl::opt<bool>
MuzAnswer ("muz-answer", cl::desc ("Print muZ answer"),
	   cl::init (false));

static cl::opt<bool>
MuzInvars ("muz-invars", cl::desc ("Print muZ invariants"),
	   cl::init (false));

static cl::opt<bool>
MuzPp ("muz-pp", cl::desc ("Use muZ pre-processing"),
	   cl::init (false));

static cl::opt<bool>
MuzPrettyOutput ("muz-pretty", cl::desc ("Use pretty FP output"),
		 cl::init (false));

static cl::opt<bool>
MuzValidate ("muz-validate", cl::desc ("Validate UNSAT results"),
	     cl::init (false));
static cl::opt<std::string>
MuzCert("muz-cert",
	  cl::desc("File to store UNSAT certificate"),
	  cl::value_desc("FILE"), cl::init(""));



namespace ufo
{
  namespace horn
  {
    /** a Horn rule */
    class Rule {
    private:
      // rule is from srcCp to dstCp
      CutPointPtr srcCp, dstCp; 
      // arguments for relations
      ExprVector srcArgs, dstArgs; 
      // edge condition
      Expr econd; 
      
      /// environments at source and destination
      Environment srcEnv, dstEnv;
    public:

      Rule () {}
    
      void setSrc (CutPointPtr cp) { srcCp = cp; }
      void setDst (CutPointPtr cp) { dstCp = cp; }
      void setEcond (Expr e) { econd = e; }

      void addSrcArg (Expr e) { srcArgs.push_back (e); }
      void addDstArg (Expr e) { dstArgs.push_back (e); }
      
      const CutPointPtr getSrcCp () const { return srcCp; }
      const CutPointPtr getDstCp () const { return dstCp; }
      const ExprVector& getSrcArgs () const { return srcArgs; }
      const ExprVector& getDstArgs () const { return dstArgs; }
      const Expr getEcond () const { return econd; }

      unsigned getNumSrcArgs() const { return srcArgs.size(); }
      unsigned getNumDstArgs() const { return dstArgs.size(); }

      void setSrcEnv (Environment &v) { srcEnv = v; }
      Environment &getSrcEnv () { return srcEnv; }

      void setDstEnv (Environment &v) { dstEnv = v; }
      Environment &getDstEnv () { return dstEnv; }
    };
  }
  

  class HornProducer
  {
  public:

    ExprFactory m_efac;

    // -- Large Block Encoding
    LBE& lbe;

    // -- Weak topo ordering
    WTOPass& wto;

    // -- Main function
    Function& mainF;

    // -- initial location
    CutPointPtr locInit;
    // -- error location
    CutPointPtr locError;


    DominatorTree &DT;

    // -- live value analysis
    CptLiveValues cpLive;
    
    // head and body for each rule
    std::vector<horn::Rule> m_rules;

    typedef std::map<CutPointPtr,Expr> LocExprMap;
    // -- map from cut-points to the decl of corresponding relation
    LocExprMap m_cpDecl;


    // z3 context for communicating Expr to z3::ast
    UfoZ3 m_z3;
    ZFixedPoint<UfoZ3> m_fp;
    Expr m_query;

  public:
    HornProducer (CutPointPtr mli, CutPointPtr mle, 
		  LBE& _lbe, WTOPass& _wto, 
		  Function& f, DominatorTree &dt) :
      lbe(_lbe), wto(_wto),
      mainF(f),
      locInit(mli), 
      locError(mle), 
      DT(dt),
      cpLive (m_efac, mainF, locInit, locError, dt, lbe),
      m_z3 (m_efac), m_fp (m_z3) 
    {
      ZParams<UfoZ3> params (m_z3);
      params.set (":engine", "pdr");
      params.set (":use-farkas", true);
      params.set (":generate-proof-trace", false);

      if (!MuzPp)
	{
	  params.set (":slice", false);
	  params.set (":inline-linear", false);
	  params.set (":inline-eager", false);
	}

      m_fp.set (params);      
    }
    
    
  public:

    // -- generates horn clauses for the program and returns false
    bool run ()
    {
      LOG ("muz",
	   dbgs () << "Running Horn Producer\n";);
      
      // run live value analysis
      cpLive.run ();
      // -- populate m_rules
      createRules ();

      // -- populate m_fp
      createFixedPoint ();
      solveFixedPoint ();
      
      // output the horn clauses
      outputHorn ();
      
      return false;
    }

    std::string getCpRelName (CutPointPtr cp) 
    {
      return "CP!" + std::string (cp->getBB()->getName());
    }


    /// specialized function to arrange cut-points in topological
    /// order.
    CutPointVector &topological_sort (CutPointVector &cps)
    {
      // second element of cps is the exit - move it to the end
      CutPointVector::iterator cp_it = cps.begin() + 1;
      assert (cp_it != cps.end ());
      
      CutPointPtr cp = *cp_it;
      cps.erase(cp_it);
      cps.push_back(cp);
      return cps;
    }
    

    void createRules ()
    {
      // get a copy of the cutpoints in topological order
      CutPointVector cps (lbe.getFunctionCutPts (mainF));
      topological_sort (cps);


      /** iterate over cut points in topological order */
      forall (CutPointPtr cp, cps)
	{
	  LOG("muz_verbose",
	      dbgs () << "Cutpoint's basic block : " 
	      << (cp->getBB ()->getName ()) << "\n";);

	  for (edgeIterator it = cp->predBegin (), end = cp->predEnd (); 
	       it != end; ++it)
	    {
	      horn::Rule r;
	      
	      SEdgePtr edg = *it;
              CutPointPtr src = edg->getSrc();
              CutPointPtr dst = cp;

              r.setSrc (src);
              r.setDst (dst);
	      
	      Environment env (m_efac);
	      
	      // eval all src args
	      forall (Expr var, cpLive.live (src)) r.addSrcArg (env.eval (var));
	      r.setSrcEnv (env);
	      
              // compute edge between src and dst
              // The initial environment is not updated to first instruction
              // it contains no prime variables
              SEdgeCondComp condComp (m_efac, env, edg, lbe, false, false);
              condComp.compute ();
	      Expr econd = condComp.getCond ();
              // -- the final environment is updated to map unprimed
              // -- variables that have changed to primed ones.
              SEdgeCondComp::updateEnvToFirstInst (env, *edg->getDst ());

              r.setEcond (econd);

	      // eval all dst args
	      forall (Expr var, cpLive.live (dst)) r.addDstArg (env.eval (var));
	      r.setDstEnv (env);

              // store the rule
              m_rules.push_back (r);
	    }
	}


      // -- dump debug summary
      LOG("muz_verbose",
	  forall (horn::Rule &r, m_rules) 
	  {
	    dbgs() << "Rule" << "\n"
		   << "\tSrc : " 
		   << (r.getSrcCp())->getBB()->getName() << "\n"
		   << "\tSrc args : " << " ";
	    forall (Expr arg, r.getSrcArgs()) dbgs() << *arg << " ";
	    dbgs() << "\n";
	      
	    dbgs() << "\tDst : " << 
	      (r.getDstCp())->getBB()->getName() << "\n";
	    
	    dbgs() << "\tDst args : " << " ";
	    forall (Expr arg, r.getDstArgs()) dbgs() << *arg << " ";
	    dbgs() << "\n";
	      
	    dbgs() << "\tEcond : " << *(boolop::pp(r.getEcond())) << "\n\n"
		   << "End Rule\n";
	  });
    }
    
    void createFixedPoint ()
    {
      // everything other than cutpoint relations
      ExprVector allExps;

      forall (horn::Rule &r, m_rules) 
	{
	  forall (Expr e, r.getSrcArgs()) 
	    allExps.push_back (e);
        
	  forall (Expr e, r.getDstArgs()) 
	    allExps.push_back (e);
        
	  allExps.push_back (r.getEcond());
	}

      // -- find all variables
      ExprVector vars;
      filter (mknary<AND> (allExps), ufo::IsVar (), std::back_inserter (vars));
      
      LOG("muz_verbose", dbgs () << "VARS\n";
	  forall (Expr v, vars)
	  { dbgs () << "\t" << *v << "\n"; }
	  dbgs () << "END VARS\n";);

      forall (CutPointPtr cp, lbe.getFunctionCutPts (mainF)) 
	{
	  Expr name = mkTerm (getCpRelName (cp), m_efac);

	  ExprVector types;
	  forall (Expr var, cpLive.live (cp)) 
	    types.push_back (ufo::typeOf (var));
	  // -- the range
	  types.push_back ( mk<BOOL_TY> (m_efac));
	  Expr decl = bind::fdecl (name, types);
	  m_cpDecl [cp] = decl;

	  // -- register relation with fixedpoint 
	  m_fp.registerRelation (decl);
	}				      
      
      m_fp.addRule (vars, bind::fapp (m_cpDecl [locInit]));
      forall (horn::Rule &r, m_rules) 
	{
	  Expr dst = bind::fapp (m_cpDecl [r.getDstCp ()], r.getDstArgs ());
	  Expr src = bind::fapp (m_cpDecl [r.getSrcCp ()], r.getSrcArgs ());

	  m_fp.addRule (vars, boolop::limp (boolop::land (src, r.getEcond ()), 
					    dst));
	}
      
      m_query = bind::fapp (m_cpDecl [locError]);
    }
    
    void solveFixedPoint ()
    {
      if (!MuzSolve) return;
      
      tribool res = m_fp.query (m_query);
      if (res == true) outs () << "UNSAFE\n";
      else if (res == false) outs () << "SAFE\n";
      else outs () << "UNKNOWN\n";
      
      if (MuzAnswer) outs () << m_fp.getAnswer () << "\n";
      
      if (!res && MuzInvars)
	{
	  outs () << "Invariants:\n";
	  forall (CutPointPtr cp, lbe.getFunctionCutPts (mainF))
	    {
	      Expr decl = m_cpDecl [cp];
	      Expr app = bind::fapp (decl, cpLive.live (cp));
	      outs () << *bind::fname (decl) << ": " 
		      << *m_fp.getCoverDelta (app)
		      << "\n";
	    }
	}
      
      if (!res && (MuzValidate || MuzCert.size () > 0))
	validate ();
    }

    void validate () { validate_new (); }
    

    void validate_new ()
    {

      /// A broken attempt at getting the validator to produce a single formula. 
      ZSolver<UfoZ3> smt (m_z3);

      typedef std::map<CutPointPtr,Expr> LocExprMap;
      
      LocExprMap srcApps, dstApps;

      // -- the edges and the source tags
      forall (horn::Rule &r, m_rules)
        {
          CutPointPtr src = r.getSrcCp ();
          CutPointPtr dst = r.getDstCp ();
          
          Expr edgTag = edgeTag (src, dst);
          smt.assertExpr (mk<IMPL> (edgTag, r.getEcond ()));
          smt.assertExpr (mk<IMPL> (edgTag, srcTag (src)));


          // -- prepare src and dst applications for future lemma extraction
          if (srcApps.count (src) <= 0)
            srcApps.insert 
              (LocExprMap::value_type (src,
                                       bind::fapp (m_cpDecl [src], 
                                                   r.getSrcArgs ())));
          if (dstApps.count (dst) <= 0)
            dstApps.insert
              (LocExprMap::value_type (dst, 
                                       bind::fapp (m_cpDecl [dst],
                                                   r.getDstArgs ())));
        }
      
      CutPointVector cps (lbe.getFunctionCutPts (mainF));
      topological_sort (cps);

      // -- destination tags
      forall (CutPointPtr cp, cps)
        {
          if (cp->predSize () == 0) continue;
          
          ExprVector edges;
          edges.reserve (cp->predSize ());
          for (edgeIterator it = cp->predBegin (), end = cp->predEnd ();
               it != end; ++it)
            {
              SEdgePtr edg = *it;
              edges.push_back (edgeTag (edg->getSrc (), edg->getDst ()));
            }
          smt.assertExpr (mk<IMPL> (dstTag (cp), 
                                    mknary<OR> (mk<TRUE> (m_efac), edges)));
        }

      ExprVector preAssumptions;

      // -- source lemmas
      forall (CutPointPtr cp, cps)
        {
          if (srcApps.count (cp) <= 0) continue;
          

          string assumpPrefix ("pre!" + cp->getBB ()->getNameStr () + "!");
          

          // -- get the lemmas
          ExprVector lemmas;
          lemmas.push_back (m_fp.getCoverDelta (srcApps [cp]));
          
          // -- break down a conjunction
          if (isOpX<AND> (lemmas.back ()))
            {
              Expr l = lemmas.back ();
              lemmas.assign (l->args_begin (), l->args_end ());
            }
          
          // -- assert each lemma individually
          Expr tag = srcTag (cp);
          
          unsigned idx = 0;
          forall (Expr &lemma, lemmas) 
            { 
              std::string asmpName = assumpPrefix + lexical_cast<string> (idx++);
              Expr asmp (bind::boolConst (mkTerm (asmpName, m_efac)));
              preAssumptions.push_back (asmp);
              smt.assertExpr (mk<IMPL> (asmp, mk<IMPL> (tag, lemma)));
            }
          
        }

      ExprVector postAssumptions;
      ExprVector dstLemmas;
      
      // -- destination lemmas
      forall (CutPointPtr cp, cps)
        {
          if (dstApps.count (cp) <= 0) continue;

          string assumpPrefix ("post!" + cp->getBB ()->getNameStr () + "!");

          // -- get the lemmas
          ExprVector lemmas;
          lemmas.push_back (m_fp.getCoverDelta (dstApps [cp]));
          
          // -- break down a conjunction
          if (isOpX<AND> (lemmas.back ()))
            {
              Expr l = lemmas.back ();
              lemmas.assign (l->args_begin (), l->args_end ());
            }

          Expr tag = dstTag (cp);
          unsigned idx = 0;
          forall (Expr &lemma, lemmas)
            {
              std::string asmpName = assumpPrefix + lexical_cast<string> (idx++);
              Expr asmp (bind::boolConst (mkTerm (asmpName, m_efac)));
              postAssumptions.push_back (asmp);
              dstLemmas.push_back (mk<AND> (mk<NEG> (asmp), 
                                            tag, boolop::lneg (lemma)));
            }
          
        }
      
      smt.assertExpr (mknary<OR> (mk<TRUE> (m_efac), dstLemmas));

      if (MuzValidate)
        {
          outs () << "VALIDATING: ";
          tribool res = smt.solveAssuming (preAssumptions);
          if (!res) outs () << "OK\n";
          else 
            {
              outs () << "FAIL\n";
              validate_old ();

              ExprVector assertions;
              smt.assertions (std::back_inserter (assertions));
              ZModel<UfoZ3> model = smt.getModel ();
          
              forall (Expr a, assertions)
                {
                  // -- skip all implications that are vacuously satisfied
                  if (isOpX<IMPL> (a) && isOpX<FALSE> (model.eval (a->left ())))
                    continue;

                  errs () << *a << "\n";
                }
              exit (1);
            }
        }

      if (MuzCert.size () > 0)
        {
          outs () << "Storing certificate to " << MuzCert << "\n";

          std::string errorInfo;
          raw_fd_ostream out (MuzCert.c_str (), errorInfo);
          if (errorInfo.empty ())
            {
              smt.toSmtLibAssuming (out, preAssumptions);

              out << ";(post-assumptions:";
              forall (const Expr &a, postAssumptions) out << " " << *a;
              out << ")\n";
              
              out.flush ();
              out.close ();
            }
          else
            errs () << "Cannot open '" << MuzCert << "' due to: " 
                    << errorInfo << "\n";
        }
    }
    
    Expr srcTag (CutPointPtr src)
    { return bind::boolConst (variant::variant (0, m_cpDecl [src])); }

    Expr dstTag (CutPointPtr dst)
    { return bind::boolConst (variant::variant (1, m_cpDecl [dst])); }

    Expr edgeTag (CutPointPtr src, CutPointPtr dst)
    { return bind::boolConst (mk<TUPLE> (m_cpDecl [src], m_cpDecl [dst])); }
    
    
    
    void validate_old ()
    {
      tribool res = false;
      
      outs () << "VALIDATING: ";
      // validate each rule independently
      forall (horn::Rule &r, m_rules)
	{
	  ZSolver<UfoZ3> smt (m_z3);
	  Expr srcDecl = m_cpDecl [r.getSrcCp ()];
	  Expr srcApp = bind::fapp (srcDecl, r.getSrcArgs ());
	  Expr srcLemma = m_fp.getCoverDelta (srcApp);

	  Expr dstDecl = m_cpDecl [r.getDstCp ()];
	  Expr dstApp = bind::fapp (dstDecl, r.getDstArgs ());
	  Expr dstLemma = m_fp.getCoverDelta (dstApp);
	  
	  smt.assertExpr (srcLemma);
	  smt.assertExpr (r.getEcond ());
	  smt.assertExpr (mk<NEG> (dstLemma));
	  res = smt.solve () || res;
	  if (!res) 
	    outs () << ".";
	  else 
	    outs () << "x";
	  outs ().flush ();
	}

      if (!res) outs () << " OK\n";
      else
	{
	  outs () << " FAIL\n";
	  exit (1);
	}
    }
    
    
    void outputHorn () 
    {
      // -- no file name
      if (MuzOutput.size () == 0) return;
      
      outs () << "Storing fixedpoint in: " << MuzOutput << "\n";
      
      std::string errorInfo;
      raw_fd_ostream out (MuzOutput.c_str (), errorInfo);
      if (errorInfo.empty ())
	{
	  if (MuzPrettyOutput)
	    {
	      out << m_fp << "\n";
	      out << "(query " << *m_query << ")\n";
	    }
	  else
	    out << m_fp.toString (m_query) << "\n";
	  out.flush ();
	}
      else
	errs () << "Cannot open file: `" << MuzOutput << "' due to: " 
		<< errorInfo << "\n";
      
    }
  };




  struct Horn : public ModulePass 
  {
    static char ID;
    Horn () : ModulePass(ID) { errs () << " Creating Horn Pass \n";}    

    static bool exit;
    virtual bool runOnModule(Module &M) 
    { 
      if (exit) return false;
      else exit = true;

      Function& F = *(M.getFunction("main")); 
      if (F.getName().compare("main") != 0) return false;

      LBE &lbe = getAnalysis<LBE>();
      WTOPass &wto = getAnalysis<WTOPass>();
      CutPointPtr li = lbe.bb2cp [&F.getEntryBlock ()];

      // -- error location is the exit location
      CutPointPtr le = lbe.cutPts[&F][1];


      if (le == NULL) 
	{
	  errs() << "No error location defined\n" 
		 << "TERMINATING\n"; 
	  return false;
	}

      // -- check if error cp unreachable in CFG
      if (le->predBegin() == le->predEnd()) 
	{
	  errs() << "\n\n\n\nMain does not return\n";
	  errs() << "program correct: ERROR unreachable\n\n\n";
	  return false;
	}

      errs () << "Instantiating Horn\n";

      DominatorTree &DT = getAnalysis<DominatorTree> (F);

      HornProducer hp (li, le, lbe, wto, F, DT);

      hp.run ();

      return false;
    }

    virtual void getAnalysisUsage(AnalysisUsage &AU) const
    {
      AU.addRequired<DominatorTree> ();
      AU.addRequired<LBE> ();
      AU.addRequired<Topo> ();
      AU.addRequired<WTOPass> ();
      AU.addRequired<NameValues> ();
      AU.setPreservesAll ();
    }
  };

  char Horn::ID = 0;
  bool Horn::exit = false;
}

INITIALIZE_PASS_BEGIN(Horn, "muz", "Z3 muZ engine", false, false)
INITIALIZE_PASS_DEPENDENCY(DominatorTree)
INITIALIZE_PASS_DEPENDENCY(LBE)
INITIALIZE_PASS_DEPENDENCY(Topo)
INITIALIZE_PASS_DEPENDENCY(WTOPass)
INITIALIZE_PASS_DEPENDENCY(NameValues)
INITIALIZE_PASS_END(Horn, "muz", "Z3 muZ engine", false, false)



