#include "llvm/Support/Signals.h"
#include "llvm/Support/PrettyStackTrace.h"
#include "llvm/Support/SystemUtils.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Support/raw_ostream.h"

#include "ufo/Smt/UfoZ3.hpp"

#include "ufo/Asd/OpenCloseInterval.hpp"
#include "ufo/Asd/Bnd/Bound.hpp"
#include "under/BoxesUnder.hpp"

#include "llvm/Support/Debug.h"
#include <fstream>

using namespace llvm;
using namespace ufo;

// input ltr model file
static cl::opt<std::string> 
BenchmarkFilename ("b",
                   cl::desc("Benchmark filename"),
                   cl::value_desc("filename"));

// verbose level
static cl::opt<unsigned>
Verbose ("verbose",
         cl::desc("Display mathsat results"),
         cl::init(0));

// maximum number of iterations
static cl::opt<unsigned>
Iteration ("iter",
           cl::desc("Number of iterations (boxes)"),
           cl::init(30));

// speed of ratio adaptation
static cl::opt<unsigned>
AdapteRate ("adapt",
            cl::desc("Number of iterations of sampling before adapte aspect ratio"),
            cl::init(9));

// speed of unboundedness check
static cl::opt<unsigned>
UnboundThreshold ("unbound",
                  cl::desc("The threshold of growth rate for blocking unbound dir"),
                  cl::init(10));

namespace
{
  struct UnderApp
  {
    typedef std::pair<Expr, ExprPair> ExprExprPairPair;

    ExprFactory efac;
    UfoZ3 ctx; // Z3 context
    Expr fmla; // input LTR formula: phi
    ExprSet vars; // Vars (phi)
    Expr lambda;  // lambda variable expression
    Expr zero; // zero expression
    std::map<Expr, ExprPair> abox; // map var to (alpha, beta)

    UnderApp () : ctx(efac),
                  lambda (freshVar("lambda")),
                  zero (mkTerm (mpq_class(0), efac))
      {}
    
    /**
     * Read input file and parse to Expr
     */
    void readInput ()
      {
        if (Verbose > 0)
          errs () << "Loading smt file: " << BenchmarkFilename << "\n";
        std::string benchStr = loadFile (BenchmarkFilename);
        if (Verbose > 0)
          errs () << "Parsing...\n";
        fmla = z3_from_smtlib (ctx, benchStr);
        vars = Bound::collectVars (fmla);
        if (Verbose >0)
          errs () << "Size of |v|: " << vars.size() << "\n";
      }

    /**
     * Run ultr algorithm
     */
    void run ()
      {
        Stats::resume ("total");
        readInput(); // read input file
        ExprVector g, h, s;
        
        forall (Expr v, vars)
        {
          Expr alpha = freshAlpha();
          Expr beta = freshBeta();
          abox[v] = std::make_pair(alpha, beta);
          // interval domain
          g.push_back (mk<AND>(mk<LEQ>(alpha,v), mk<LEQ>(v, beta)));
          // for timing variables implicitly greater than 0
          h.push_back (mk<GEQ>(v, zero));
          s.push_back (alpha);
        }
        
        // sum term
        Expr sum = mknary<PLUS> (zero, s.begin(), s.end());
        // All timing variables should be greater than or equal to zero
        Expr fmla_non_negative = mk<AND>(mknary<AND>
                                         (mk<TRUE>(efac), h.begin(), h.end()),
                                         fmla);
        // Detect unbounded directions
        // Compute over-approximation of \phi
        Box over = overApprox (fmla_non_negative, vars);
        BoxesUnder boxes (efac, over, vars, abox, fmla_non_negative); 
        // open-close-box-domain
        if (Verbose > 1) 
          errs () << "Over: " << boxes.printBox (over) << "\n";

        // theta
        Expr theta = mk<IMPL>(mknary<AND>(mk<TRUE>(efac), g.begin(), g.end()),
                              fmla_non_negative);
        
        if (Verbose >= 10) errs () << "theta: " << *theta << "\n";
        Expr theta_prim = z3_forall_elim (ctx, theta, vars);        
        Aspect ratio = boxes.equalAspect (); // current aspect ratio for sampling
        
        Model m;
        mpq_class size, last_size; // size of the maximal box
        bool strict;
        
        //main loop
        int loop_count = 0;
        Expr ub = NULL; // sampling unbounded direction
        bool initial_sample = true;
        while (loop_count++ < Iteration)
        {
          Stats::count ("main.loop");
          if (Verbose > 0) 
            errs () << *(boxes.block()) << "\n";
          
          Expr aspect = aspectToExpr (ratio, abox, lambda);
          Expr tmp = mk<AND>(mk<AND>(aspect,
                                     mk<GT>(lambda, zero)), 
                             theta_prim); // formula to be optimized
          
          if (optimize (mk<AND>(tmp, boxes.block()), 
                        lambda, m, size, strict, true))
          {
            if (initial_sample) 
            {
              last_size = size;
              initial_sample = false;
            }
            if (size < last_size/20)
            {
              errs () << "size too small\n";
              ratio = boxes.equalAspect();
              continue;
            }
            
            Expr tmp_2 = mk<AND>(tmp, boxes.fixSize (m, lambda));
          
            mpq_class tmp_size;
            if (optimize (mk<AND>(tmp_2, boxes.block()),
                          sum, m, tmp_size, strict, false))
            {
              errs () << "SIZE: " << size << "\n";
              boxes.addBox (ratio, m, strict, size);
            }
            
            errs() << boxes.printGrowth () << "\n";
            
            Stats::resume ("adapt");
            // Adjust aspect ratio
            if (loop_count % AdapteRate == 0)
            {
              if (Verbose > 1) errs () << "before check convex\n";
              //check if the convex hull of boxes is subsumed
              if (!z3n_is_sat (mk<AND>(boxes.gammaConvexBoxes(),
                                       mk<NEG>(fmla_non_negative))))
              {
                if (Verbose > 1) errs () << "check convex hull\n";
                ratio = boxes.getAspect (boxes.convex_hull);
                // replace boxes with convex hull
                boxes.joinBoxes ();
              }
              else if (!z3n_is_sat (mk<AND>(
                                      boxes.gammaBox (boxes.fastestGrowBox()),
                                      mk<NEG>(fmla_non_negative))))
              {
                if (Verbose > 1) errs () << "check box shape\n";
                // adjust according to the fastest growing box
                ratio = boxes.getAspect (boxes.fastestGrowBox());
              }
              if (Verbose > 0)
              {
                errs () << "Aspect updated!\n";
                printAspect (ratio);
              }
            }
            
            // Block unbounded dir
            errs () << "check unbound\n";
            ub = boxes.suspectUnbound (UnboundThreshold);
            if (ub)
            {
              if (Verbose > 0)
                errs () << "Unbound detected! " << *ub << "\n";
              boxes.checkUnbound();
              boxes.recursiveMerge();
              if (boxes.size() >= 10)
              {
                errs () << "Get enough boxes, now terminate.\n";
                break;
              }
            }
            Stats::stop ("adapt");

            if (Verbose > 1) errs() << boxes.toString () << "\n";
          }
        }
        Stats::stop ("total");

        if (Verbose > 0) errs () << "Before recursive merge\n";
        boxes.recursiveMerge ();
        errs() << boxes.toString () << "\n";
        // validate
        if (!z3n_is_sat (mk<AND>(boxes.gamma(),
                                 mk<NEG>(fmla_non_negative))))
        {
          errs () << "Validation Success!\n";
          toFile ("/tmp/boxes." + lexical_cast<string>(boxes.size()) + ".txt", 
                  boxes.encodeBoxes());
        }
        else errs () << "Validation Failed!\n";
      }

    // ------------- helper functions ------------------------
    /**
     * Return over-approximation of fmla in Box
     */
    Box overApprox (Expr fmla, ExprSet &dims)
      {
        Model m;
        mpq_class sl, sr;
        bool str_l, str_r;
        Box res;

        forall (Expr d, dims)
        {
          optimize (fmla, d, m, sl, str_l, false);
          optimize (fmla, d, m, sr, str_r, true);
          
          res[d] = OpenCloseInterval (sl, sr, !str_l, !str_r);
        } 

        return res;
      }
    
    /**
     * Convert an aspect ratio to Expr constraint
     */
    Expr aspectToExpr (const Aspect &ratio, 
                       const std::map<Expr, ExprPair> &abox, 
                       Expr lambda)
      {
        ExprVector g;
        forall (ExprExprPairPair v, abox)
        {
          g.push_back (mk<EQ>(mk<MINUS>(v.second.second, v.second.first),
                              mk<MULT>(
                                mkTerm(ratio.find (v.first)->second, efac),
                                lambda)));
        }
        
        return mknary<AND> (mk<TRUE>(efac), g.begin(), g.end());
      }

    /**
     * Print Aspect ratio
     */
    void printAspect (const Aspect &ratio)
      {
        string res = "";
        typedef std::pair<Expr, mpq_class> RatioPair;
        forall (RatioPair p, ratio)
        {
          errs () << *(p.first) << " = " << p.second << " : ";
        }
        errs () << "\n";
      }
     
    /**
     * Execute a command string
     */
    std::string exec (std::string cmd) 
      {
        FILE* pipe = popen (cmd.c_str(), "r");
        if (!pipe) return "ERROR";
        char buffer[128];
        std::string result = "";
        while (!feof(pipe)) {
          if (fgets(buffer, 128, pipe) != NULL)
            result += buffer;
        }
        pclose (pipe);
        return result;
      }

    /**
     * optimize v in phi using optMathSAT
     * output formula phi to a smt2 file at hardcoded location
     * the path for optMathSAT is also hardcoded, need to be fixed
     */
    bool optimize (Expr phi, Expr v, Model &model, mpq_class &max, bool &strict,
                   bool maximize = false)
      {
        Expr cost = freshVar ("cost");
        Expr solve = mk<AND> (mk<EQ>(cost, 
                                     maximize? mk<UN_MINUS>(v) : v), 
                              phi);
        
        string smtlib_file = ctx.toSmtLibDecls (solve) + 
          "\n" + "(assert\n" +
          ctx.toSmtLib (solve) + ")";
        
        Stats::resume ("to.file");
        toFile ("/tmp/theta_prime.smt2", smtlib_file);
        Stats::stop ("to.file");
       
        Stats::resume ("mathsat");
        string res = 
          exec ("/home/liyi/Desktop/optimathsat5/optimathsat -c cost -m < /tmp/theta_prime.smt2");
        Stats::stop ("mathsat");

        if (Verbose > 5)
          errs () << "RES: " << res << "\n";
        
        mpq_class min;
        bool sat = false;
        strict = false;

        vector<string> flds = split (res, '\n', 0);

        forall (string line, flds)
        {
          if (line[0] == '#')
          {
            if (line.compare ("# No solution.") == 0)
              return false;
            if (line.compare ("# Exact strict minimum found!") == 0)
              strict = true;
            if (line.find ("Minimum:") != string::npos)
            {
              string val = line.substr (line.find_last_of (" ") + 1,
                                        string::npos);
              if (val.compare ("-INF") == 0)
                min = mpq_class (-1, 0);

              else min = mpq_class (val);
            }
          }
          else
          {
            if (line.compare ("sat") == 0)
            {
              sat = true;
              continue;
            }
            
            string res = line.substr (3, line.length() -4); // remove outter brackets
            string varname = res.substr (0, res.find_first_of (" "));
            string value = res.substr (res.find_first_of (" ") + 1, 
                                       string::npos);

            model[varname] = getValFromString (value);
          }
        }
        
        //assert (sat && "OptiMathSAT returns unsat!");
        max = 0 - min;
        
        return sat;
      }

    /**
     * Convert string get from optimathsat output
     * to mpq_class value that we can work with
     */
    mpq_class getValFromString (const string val)
      {
        if (val[0] != '(')
          return mpq_class (val);
        else
        {
          string quot; // val is a quotient
          bool neg = false;
          if (val[1] == '-')
          { // negative value
            neg = true;
            if (val[3] != '(')
              return 0 - mpq_class (val.substr (3, val.length() - 4));
                                    
            quot = val.substr (6, val.length() - 8);
          }
          else
            quot = val.substr (3, val.length() - 4);
          
          string sign = neg ? "-" : "";
          return mpq_class (sign +
                            quot.substr(0, quot.find_first_of (" ")) + 
                            "/" + 
                            quot.substr(quot.find_first_of (" "), string::npos));
        }
      }

    
    /**
     * split: receives a char delimiter; returns a vector of strings
     * by default ignores repeated delimiters, unless argument rep == 1.
     */
    vector<string> split(string work, char delim, int rep) {
      vector<string> flds;
      string buf = "";
      int i = 0;
      while (i < work.length()) {
        if (work[i] != delim)
          buf += work[i];
        else if (rep == 1) {
          flds.push_back(buf);
          buf = "";
        } else if (buf.length() > 0) {
          flds.push_back(buf);
          buf = "";
        }
        i++;
      }
      if (!buf.empty())
        flds.push_back(buf);
      return flds;
    }

    /**
     * Get a fresh variable
     */
    Expr freshVar (std::string name)
      {
        return bind::realConst(mkTerm (name, efac));
      }

    /**
     * Create a real variable \alpha: a_i
     */
    Expr freshAlpha ()
      {
        static int count = 0;
        std::string var = "a" + lexical_cast<string>(count++);
        return bind::realConst (mkTerm (var, efac));
      }

    /**
     * Create a real variable \beta: b_i
     */
    Expr freshBeta ()
      {
        static int count = 0;
        std::string var = "b" + lexical_cast<string>(count++);
        return bind::realConst (mkTerm (var, efac));
      }
    
    /**
     * Load file to string given path
     */
    std::string loadFile (std::string fname)
      {
        // -- open file
        std::ifstream f(fname.c_str ());
    
        if (f)
        {
          // -- compute size
          f.seekg(0, std::ios::end);
          size_t size = f.tellg();
          f.seekg(0);
        
          std::string buffer (size, ' ');
          
          f.read (&buffer[0], size);
          return buffer;
        }
        else 
        { errs () << "File Not Found!\n"; exit(0);}
      }

    /**
     * Write string to file
     */
    void toFile (std::string fname, std::string out)
      {
        //std::ofstream f(fname.c_str ());
        string errorStr;
        raw_fd_ostream f (fname.c_str(), errorStr);

        if (errorStr.empty())
        {
          f << out;
          f.flush ();
          f.close ();
        }
        else
        {
          errs () << "Unable to open file\n";
          exit(0);
        }
        
        return;
      }

    /**
     * Print results
     */
    void printRes (Bound& b, ExprSet &T)
      {
        errs () << "\n";
        forall (Expr t, T)
        {
          errs () << "RESULT: " << *t << " : ["
                  << b[t].first << ","
                  << b[t].second << "]\n";
        }
      }
  };
}

int main (int argc, char** argv)
{
  sys::PrintStackTraceOnErrorSignal ();
  llvm::PrettyStackTraceProgram X (argc, argv);
  EnableDebugBuffering = true;
  llvm_shutdown_obj Y;
  
  cl::ParseCommandLineOptions (argc, argv, 
                               "Symba : "
                               "Symbolic Optimization Benchmark Runner\n");
  
  if (!BenchmarkFilename.empty ())
  {
    UnderApp under;
    under.run ();
    
    Stats::PrintBrunch (errs ());
  }
}
