#include "llvm/Support/raw_ostream.h"

#include "ufo/Smt/UfoZ3.hpp"

using namespace ufo;

/** A simple program that exercises Expr and Z3 interfaces */

int main (int argc, char ** argv)
{
  if (argc != 2)
    {
      llvm::outs () << "usage: smt2smt SMT\n";
      return 1;
    }

  char* fname = argv [1];

  ExprFactory efac;
  UfoZ3 z3 (efac);
  outs () << "Parsing " << fname << "...\n";
  Expr exp = z3_from_smtlib_file (z3, fname);

  // -- note that to_smtlib will use different z3 object.
  // -- this is on purpose to exercise more of the framework
  outs () << "Result:\n" << z3n_to_smtlib (exp) << "\n";

  return 0;
}
