set (SVCOMP13_OBJ_ROOT "SVBCOMP13_OBJ_ROOT-NOTFOUND" 
  CACHE PATH "Location of SVCOMP13 object files")

if (NOT SVCOMP13_OBJ_ROOT)
  message (FATAL_ERROR "SVCOMP13_OBJ_ROOT has to be set")
endif ()
## CFI regressions
include (Cfi.cmake)